<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT doc_ref,doc_remarks,doc_date,doc_status,gr_name,
(SELECT loc_name FROM location l WHERE l.loc_id = d.doc_source) as source, d.doc_desti as desti,
SUM(dp.dp_qty*pv.pv_price) as tot
FROM document d,group_div gr, document_product dp, product_version pv
WHERE (d.doc_gr_id = gr.gr_id)
AND (dp.dp_doc_id = d.doc_id)
AND (dp.dp_prod_id = pv.pv_id)
AND (d.doc_id = ?)
GROUP BY d.doc_id
ORDER BY doc_status desc, doc_date desc";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    if($fetch['doc_status']=='shipped')
      $status_string = "verified";
    else
      $status_string = $fetch['doc_status'];
    
    $output[] = array ($fetch['doc_ref'],$fetch['doc_date'],$status_string,
      $fetch['gr_name'],$fetch['source'],$fetch['desti'],$fetch['tot'],$fetch['doc_remarks']);          
  }         
$conn = null;             

echo json_encode($output);
?>    