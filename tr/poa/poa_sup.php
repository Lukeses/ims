<?php
require('../../mn/include/log_tr.php')
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>P.O.A. | Supplier </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- toastr-->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />    
    <!-- Daterange picker -->
    <link href="../../resources/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/slive.js" type="text/javascript"></script>   
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>
    <!--bootsrap validator-->
    <script src="../../resources/bootstrap/js/bootstrapValidator.js"></script>
    <script src="../../resources/plugins/datatables/formatted-num.js" type="text/javascript"></script> 
    <script src="../../resources/plugins/datatables/formatted-numbers.js" type="text/javascript"></script>   
    <!-- Daterang Picker -->
    <script src="../../resources/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>  

  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini ">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside_sup.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">




        <!-- Content Header (Page header) -->
        <section class="content-header">
        <a href="javascript:select_groupdiv()" id="btn_select_groupdiv" role="button" class="btn bg-green pull-right" data-toggle='tooltip' title="Re-Select Group " data-placement='left'><i class="ion-android-checkbox-outline"></i> Re-Select Group</a>   

          <h1>
            P.O. Analysis
            <small>By Supplier</small>
          </h1>
          <?php echo'<text class="lead" id="gd_selected_name">'.$session_group_name.'</text>';  ?>                    
          <?php echo'<input type="hidden" id="gd_selected" value="'.$session_group.'">';  ?>            

        </section>

        <!-- Main content -->
        <section class="content">

          <?php include('../../mn/include/loading.html') ?>
          <?php include('../../mn/include/group_div.html') ?>

          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Information</h4>
            1. Browse and Select Supplier Below. <br>
            2. Either View P.O. Records by PRODUCTS or DOCUMENT. Click the Button<br>
            3. Re-Select Group is changeable throughout the pages
          </div>

          <div class="row">                     <!-- TABLES -->
          <div class="col-lg-12 col-sm-12 col-xs-12">
              <div class="box box-solid">
              <div class="box-header">
                <h3 class="box-title"><i class="ion-android-person"></i> SELECT SUPPLIER: </h3>
              </div><!-- /.box-header -->              
                <div class="box-body">    
                  <table id="sup_table" class="table table-striped table-hover">                  
                    <thead>
                      <tr>
                        <th>Supplier</th>                       
                        <th>Description</th>
                        <th style="width:80px">Telephone</th> 
                        <th style="width:80px">Phone</th>                         
                        <th style="width:10px"></th>
                        <th style="width:10px"></th>

                      </tr>
                      <tbody></tbody>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>  <!-- /.row -->

        </section><!-- /.content -->


      </div><!-- /.content-wrapper -->

      <?php include('../../mn/include/footer.php'); ?>
    </div><!-- ./wrapper -->


    <!--MAIN CONTROLLER -->
    <script src="controller_sup.js"></script>
    <script>
      $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-bottom-left",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "500",
          "hideDuration": "1000",
          "timeOut": "6000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "shoIMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }//.toastr config  
   
      //Initialize Select2 Elements
      $("#gd_name").select2();

      });      


      function select_groupdiv(){
        populate_gr_dropdown();    
        $('#gd_name').select2().select2('val','none');              
        $('#gd_nameDiv').removeClass('has-error');
        $('#gd_modal').modal('show');    
      }  

      $('#btn_gd_submit').click(function(){
        if($('#gd_name').val()=='none'){
          $('#gd_nameDiv').addClass('has-error');
        }
        else{
          $('#btn_select_groupdiv').removeClass('bg-red');
          $('#btn_select_groupdiv').addClass('bg-green');         
          $('#gd_nameDiv').removeClass('has-error');
          $('#gd_selected').val($('#gd_name').val());
          $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );            
          $('#gd_submit_name').val( $( "#gd_name option:selected" ).text()  );   
          $('#gd_modal').modal('hide');                   
          //load_gr_table($('#gd_selected').val());
          change_session_group();
        }
      })

      function change_session_group(){
        var gr_name = $( "#gd_name option:selected" ).text();  
        var gr_id = $('#gd_name').val();
        var dataString = 'gr_id='+gr_id+'&gr_name='+gr_name;
        //ajax now
        $.ajax ({
          type: "POST",
          url: "../../mn/include/change_session_group.php",
          data: dataString,
          success: function(s){         
          }  
        }); 
        //ajax end      
      }

      function populate_gr_dropdown(){ 
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/populate_gd_dropdown.php",
          dataType: 'json',      
          cache: false,
          success: function(s)
          {
            $('#gd_name').empty();
            $('#gd_name').append('<option value="none">--Select Group--</option>');
            for(var i = 0; i < s.length; i++) { 
              $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
            }       
          }  
        }); 
        //ajax end  
      } 
    </script>
  </body>
</html>
