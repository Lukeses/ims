<?php
require('../../mn/include/log_tr.php')
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> P.O. by Category</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- toastr-->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />    
    <!-- Daterange picker -->
    <link href="../../resources/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="../../resources/plugins/TableTools/css/dataTables.tableTools.css" rel="stylesheet" type="text/css" />


    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/slive.js" type="text/javascript"></script>   
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>
    <!--bootsrap validator-->
    <script src="../../resources/bootstrap/js/bootstrapValidator.js"></script>
    <!-- Daterang Picker -->
    <script src="../../resources/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script> <script src="../../resources/plugins/datatables/formatted-num.js" type="text/javascript"></script> 
    <script src="../../resources/plugins/datatables/formatted-numbers.js" type="text/javascript"></script>   
    <script src="../../resources/plugins/TableTools/js/dataTables.tableTools.js" type="text/javascript"></script>   

  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini ">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside_cat.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <a href="javascript:select_groupdiv()" id="btn_select_groupdiv" role="button" class="btn bg-green pull-right" data-toggle='tooltip' title="Re-Select Group " data-placement='left'><i class="ion-android-checkbox-outline"></i> Re-Select Group</a>  
          <h1>
            P.O. Analysis
            <small> By Category 
            </small>            
          </h1>          
          <?php echo'<text class="lead" id="gd_selected_name">'.$session_group_name.'</text>';  ?>                   
          <?php echo'<input type="hidden" id="gd_selected" value="'.$session_group.'">';  ?>

        </section>

        <!-- Main content -->
        <section class="content">

          <?php include('../../mn/include/loading.html') ?>
          <?php include('../../mn/include/group_div.html') ?>


          <div class="row" style="margin-top:10px">                     <!-- TABLES -->
            <div class="col-lg-12 col-sm-12 col-xs-12">
              <div class="box box-default">
                <div class="box-footer">
                  <div class="row">

                    <!--
                    <div class="col-sm-5 col-xs-12">
                 
                      <label>SELECT DATE RANGE: </label>

                      <div class="input-group" id="dateInputDiv">
                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input id="dateInput" type="text" class="form-control" placeholder="SELECT DATE" >
                        <div class="input-group-btn">
                          <button id="btnSearchDate" class="btn btn-default" ><i class="ion-android-search"></i> Search Date </button>
                          <button id="btnViewAll" class="btn btn-default" > View All </button>>
                        </div>
                      </div>
                    </div><!-- /.col -->
                    <div class="col-sm-4 col-xs-12">
                      <label>Select Category</label> <!-- Prod_Name -->
                      <select type="text" class="form-control " id="pr_cat" placeholder="Search">
                        <option value="none" >--Search--</option>                     
                      </select>
                    </div><!-- /.col -->

                    <div class="col-sm-4 col-xs-12">
                      <label>Filter By Supplier</label> <!-- Prod_Name -->
                      <select type="text" class="form-control " id="pr_sup">
                        <option value="all" >ALL</option>                     
                      </select>
                    </div><!-- /.col -->

                    <div class="col-sm-3 col-xs-12">
                      <div class="description-block border-left">
                        <span class="description-percentage"><i class="fa fa-calculator"></i></span>
                        <h5 class="description-header" id="lbl_grand_total"></h5>
                        <span class="description-text">GRAND TOTAL</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->

                  </div><!-- /.row -->
                </div><!-- /.box-footer -->  
                <div class="box-body">
                  <table id="pr_table" class="table table-condensed table-striped table-hover">
                    <thead>
                      <tr>
                        <th style="width:180px">Name</th>                    
                        <th>Category</th>
                        <th>Supplier</th>
												<th>Vessel</th>
                        <th>UOM</th>
                        <th>Owner</th> 
                        <th style="width:100px">Unit Price</th>                        
                        <th style="width:100px">QTY</th>                       
                        <th style="width:110px">Amount</th>
                      </tr>
                      <tbody></tbody>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>  <!-- /.row -->

        </section><!-- /.content -->


      </div><!-- /.content-wrapper -->

      <?php include('../../mn/include/footer.php'); ?>
    </div><!-- ./wrapper -->

    <script>

      $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
     
        //Initialize Select2 Elements
        $("#gd_name").select2();
        $('#pr_cat').select2();    
        $('#pr_sup').select2();  

        populate_supplier_dropdown();
        populate_category_dropdown();

        $('#dateInput').daterangepicker(
                {
                  ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                  },
                  startDate: moment().subtract('days', 29),
                  endDate: moment()
                },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );  
      });      


      function select_groupdiv(){
        populate_gr_dropdown();    
        $('#gd_name').select2().select2('val','none');              
        $('#gd_nameDiv').removeClass('has-error');
        $('#gd_modal').modal('show');    
      }  

      $('#btn_gd_submit').click(function(){
        if($('#gd_name').val()=='none'){
          $('#gd_nameDiv').addClass('has-error');
        }
        else{
          $('#btn_select_groupdiv').removeClass('bg-red');
          $('#btn_select_groupdiv').addClass('bg-green');         
          $('#gd_nameDiv').removeClass('has-error');
          $('#gd_selected').val($('#gd_name').val());
          $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );            
          $('#gd_submit_name').val( $( "#gd_name option:selected" ).text()  );   
          $('#gd_modal').modal('hide');                   
          populate_main_table('all',null,null);          
          change_session_group();
        }
      })

      function change_session_group(){
        var gr_name = $( "#gd_name option:selected" ).text();  
        var gr_id = $('#gd_name').val();
        var dataString = 'gr_id='+gr_id+'&gr_name='+gr_name;
        //ajax now
        $.ajax ({
          type: "POST",
          url: "../../mn/include/change_session_group.php",
          data: dataString,
          success: function(s){         
          }  
        }); 
        //ajax end      
      }

      function populate_gr_dropdown(){ 
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/populate_gd_dropdown.php",
          dataType: 'json',      
          cache: false,
          success: function(s)
          {
            $('#gd_name').empty();
            $('#gd_name').append('<option value="none">--Select Group--</option>');
            for(var i = 0; i < s.length; i++) { 
              $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
            }       
          }  
        }); 
        //ajax end  
      } 

      var pr_table = $('#pr_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
        "aaSorting": [],
        sDom: 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "../../resources/plugins/TableTools/swf/copy_csv_xls_pdf.swf",
                 "aButtons": [
                  {
                      "sExtends": "xls",
                      "mColumns": [0,1,2,3,4,5,6,7]
                  },
                  {
                      "sExtends": "pdf",
                      "mColumns": [0,1,2,3,4,5,6,7]
                  }, 
                  {
                      "sExtends": "copy",
                      "mColumns": [0,1,2,3,4,5,6,7]
                  },                                   

                  ]
                }          
      });  //Initialize the datatable location

      $('#btnSearchDate').click(function(){
        var str = $('#dateInput').val();
        if(str.length == 23 && str!='')
        {    
          var startDate = str.slice(0,10);
          var endDate = str.slice(13);  
        $('#dateInputDiv').removeClass('has-error'); 
        $('#btnSearchDate').removeClass('btn-danger');           
        $('#pr_table').dataTable().fnClearTable();    
        //load('specific',startDate,endDate);
        }

        else if(str.length != 23 || str=='')
        {
        $('#dateInputDiv').addClass('has-error');
        $('#btnSearchDate').addClass('btn-danger');    
        $('#pr_table').dataTable().fnClearTable();    
        }   
      })

      $('#btnViewAll').click(function(){
        $('#dateInput').val('');
        $('#dateInputDiv').removeClass('has-error'); 
        $('#btnSearchDate').removeClass('btn-danger');    
        //load('all',null,null);
      })




      function populate_main_table(type,startDate,endDate){
        $('#lbl_grand_total').text('');
        var gd_name = $('#gd_selected').val();      
        var sup_id = $('#pr_sup').val();  
        var cat_id = $('#pr_cat').val();

        var dataString = 'gd_name='+gd_name+'&sup_id='+sup_id+'&cat_id='+cat_id+'&type='+type+'&startDate='+startDate+'&endDate='+endDate;
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/populate_pr_by_cat.php",
          data: dataString,
          dataType: 'json',      
          cache: false,
          success: function(s){   
              pr_table.fnClearTable();
              var gtotal = 0.00;
              for(var i = 0; i < s.length; i++) {     
                gtotal += parseFloat(s[i][8],10);       
                pr_table.fnAddData
                ([
                  s[i][2],s[i][3],s[i][1],s[i][9],s[i][4],s[i][5],comma(s[i][6]),comma(s[i][7]),comma(s[i][8])
                ],false); 
                pr_table.fnDraw();

              }  //for     
              $('#lbl_grand_total').text('₱ '+comma(gtotal.toFixed(2)));
          }  //success
        }); 
        //ajax end
      }


      function comma(val){
          while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
          }
          return val;
      }

      function populate_category_dropdown(){ 
        //ajax now
        $.ajax ({
          type: "POST",
          url: "../../mn/pr/serverside/populate_category_dropdown.php",
          dataType: 'json',      
          cache: false,
          success: function(s)
          {
            $('#pr_cat').empty();
            $('#pr_cat').html('<option selected="selected" value="none">--Search Category--</option>');
            for(var i = 0; i < s.length; i++) { 
              $('#pr_cat').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
            }       
          }  
        }); 
        //ajax end  
        $('#pr_cat').select2().select2('val','none');                      
      } // 
      function populate_supplier_dropdown(){ 
        //ajax now
        $.ajax ({
          type: "POST",
          url: "../../mn/pr/serverside/populate_supplier_dropdown.php",
          dataType: 'json',      
          cache: false,
          success: function(s)
          {
            $('#pr_sup').empty();
            $('#pr_sup').html('<option selected="selected" value="all">ALL</option>');
            for(var i = 0; i < s.length; i++) { 
              $('#pr_sup').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
            }       
          }  
        }); 
        //ajax end  
        $('#pr_sup').select2().select2('val','all');                      
      } // 

      $('#pr_cat').change(function(){
        if($(this).val()!='none')
          populate_main_table('all',null,null);
      })

      $('#pr_sup').change(function(){
        if($('#pr_cat').val()!='none')
          populate_main_table('all',null,null);
      })
    </script>
  
  </body>
</html>
