<?php
    include('../../../mn/include/connect.php');

    $gd_name = $_POST['gd_name'];
    $sup_id = $_POST['sup_id'];
    $cat_id = $_POST['cat_id'];
    $type = $_POST['type'];
    $startDate = $_POST['startDate'];
    $endDate = $_POST['endDate'];
    $start = date('Y-m-d', strtotime(str_replace('-', '/', $startDate)));
    $end = date('Y-m-d', strtotime(str_replace('-', '/', $endDate))); 


    if($sup_id=='all'){
      $sql = "SELECT prod_id, prod_name, cat_name,prod_var, co_name,prod_sku, pp.pp_qty as qty, pv.pv_price as price, (pp.pp_qty*pv.pv_price) as total, sup_name, po_desc,po_remarks
       FROM product p, category cat, supplier s, company c, purchase_product pp, product_version pv,purchase_order po
       WHERE (c.co_gr_id=?)
       AND (p.prod_cat_id = cat.cat_id)
       AND (p.prod_sup_id = s.sup_id)
       AND (p.prod_id = pv.pv_prod_id)
       AND (po.po_co_id = c.co_id)
       AND (po.po_id = pp.pp_po_id)
       AND (pp.pp_prod_id = pv.pv_id)
       AND (cat.cat_id = ?)   
       AND (po.po_status != 'deleted')
       GROUP BY pv_id
       ORDER BY prod_name ASC ";
    }
    else if($sup_id!='all'){
      $sql = "SELECT prod_id, prod_name, cat_name,prod_var, co_name,prod_sku, pp.pp_qty as qty, pv.pv_price as price, (pp.pp_qty*pv.pv_price) as total, sup_name,po_desc,po_remarks
       FROM product p, category cat, supplier s, company c, purchase_product pp, product_version pv,purchase_order po
       WHERE (c.co_gr_id=?)
       AND (p.prod_cat_id = cat.cat_id)
       AND (p.prod_sup_id = s.sup_id)
       AND (p.prod_id = pv.pv_prod_id)
       AND (po.po_co_id = c.co_id)
       AND (po.po_id = pp.pp_po_id)
       AND (pp.pp_prod_id = pv.pv_id)
       AND (cat.cat_id = ?)   
       AND (s.sup_id = ?)
       AND (po.po_status != 'deleted')
       GROUP BY pv_id
       ORDER BY prod_name ASC ";
    }      



    




  $q = $conn->prepare($sql);

  if($sup_id == 'all')
    $q -> execute(array($gd_name,$cat_id));
  else if($sup_id!='all')
    $q -> execute(array($gd_name,$cat_id,$sup_id));


  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['prod_id'],$fetch['sup_name'],$fetch['prod_name'],
      $fetch['cat_name'],$fetch['prod_var'],$fetch['co_name'],
      $fetch['price'],$fetch['qty'],$fetch['total'],($fetch['po_desc']." ".$fetch['po_remarks'])  );				 	
  }         
$conn = null;             

echo json_encode($output);
?>    
