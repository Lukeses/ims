<?php
    include('../../../mn/include/connect.php');

    $gd_name = $_POST['gd_name'];
    $sup_id = $_POST['sup_id'];
    $vessel = $_POST['vessel'];
    $type = $_POST['type'];
    $by = $_POST['by'];
    $startDate = $_POST['startDate'];
    $endDate = $_POST['endDate'];
    $start = date('Y-m-d', strtotime(str_replace('-', '/', $startDate)));
    $end = date('Y-m-d', strtotime(str_replace('-', '/', $endDate))); 


    if($by == 'supplier'){
      if($type=='all'){
        $sql = "SELECT prod_id, prod_name, cat_name,prod_var, co_name,prod_sku, pp.pp_qty as qty, pv.pv_price as price, (pp.pp_qty*pv.pv_price) as total,po_desc,po_remarks
         FROM product p, category cat, supplier s, company c, purchase_product pp, product_version pv,purchase_order po
         WHERE (c.co_gr_id=?)
         AND (p.prod_cat_id = cat.cat_id)
         AND (p.prod_sup_id = s.sup_id)
         AND (p.prod_id = pv.pv_prod_id)
         AND (po.po_co_id = c.co_id)
         AND (po.po_id = pp.pp_po_id)
         AND (pp.pp_prod_id = pv.pv_id)
         AND (s.sup_id = ?)       
         AND (po.po_status != 'deleted')
         GROUP BY pv_id
         ORDER BY prod_name ASC ";
      }
      else if($type=='specific'){
        $sql = "SELECT prod_id, prod_name, cat_name,prod_var, co_name,prod_sku, pp.pp_qty as qty, pv.pv_price as price, (pp.pp_qty*pv.pv_price) as total,po_desc,po_remarks
         FROM product p, category cat, supplier s, company c, purchase_product pp, product_version pv,purchase_order po
         WHERE (c.co_gr_id=?)
         AND (p.prod_cat_id = cat.cat_id)
         AND (p.prod_sup_id = s.sup_id)
         AND (p.prod_id = pv.pv_prod_id)
         AND (po.po_co_id = c.co_id)
         AND (po.po_id = pp.pp_po_id)
         AND (pp.pp_prod_id = pv.pv_id)
         AND (s.sup_id = ?)       
         AND (po.po_status != 'deleted')   
         AND (po.po_date BETWEEN ? AND ?)    
         GROUP BY pv_id
         ORDER BY prod_name ASC ";
      }      
    }

    else if($by == 'vessel'){
      if($type=='all'){
        $sql = "SELECT prod_id, prod_name, cat_name,prod_var, co_name,prod_sku, pp.pp_qty as qty, pv.pv_price as price, (pp.pp_qty*pv.pv_price) as total,po_desc,po_remarks
         FROM product p, category cat, supplier s, company c, purchase_product pp, product_version pv,purchase_order po
         WHERE (c.co_gr_id=?)
         AND (p.prod_cat_id = cat.cat_id)
         AND (p.prod_sup_id = s.sup_id)
         AND (p.prod_id = pv.pv_prod_id)
         AND (po.po_co_id = c.co_id)
         AND (po.po_id = pp.pp_po_id)
         AND (pp.pp_prod_id = pv.pv_id)
         AND (po.po_status != 'deleted')
         AND (po.po_desc = ?)
         GROUP BY pv_id
         ORDER BY prod_name ASC ";
      }
      else if($type=='specific'){
        $sql = "SELECT prod_id, prod_name, cat_name,prod_var, co_name,prod_sku, pp.pp_qty as qty, pv.pv_price as price, (pp.pp_qty*pv.pv_price) as total,po_desc,po_remarks
         FROM product p, category cat, supplier s, company c, purchase_product pp, product_version pv,purchase_order po
         WHERE (c.co_gr_id=?)
         AND (p.prod_cat_id = cat.cat_id)
         AND (p.prod_sup_id = s.sup_id)
         AND (p.prod_id = pv.pv_prod_id)
         AND (po.po_co_id = c.co_id)
         AND (po.po_id = pp.pp_po_id)
         AND (pp.pp_prod_id = pv.pv_id)
         AND (po.po_status != 'deleted')
         AND (po.po_desc = ?)
         AND (po.po_date BETWEEN ? AND ?)             
         GROUP BY pv_id
         ORDER BY prod_name ASC ";
      }      
    }


    




  $q = $conn->prepare($sql);

  if($by == 'supplier'){
    if($type=='all'){
      $q -> execute(array($gd_name,$sup_id));
    }
    else if($type=='specific'){
      $q -> execute(array($gd_name,$sup_id,$start,$end));
    }    
  }
  else if($by == 'vessel'){
    if($type=='all'){
      $q -> execute(array($gd_name,$vessel));
    }
    else if($type=='specific'){
      $q -> execute(array($gd_name,$vessel,$start,$end));
    }    
  }


  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['prod_id'],$fetch['prod_sku'],$fetch['prod_name'],
      $fetch['cat_name'],$fetch['prod_var'],$fetch['co_name'],
      $fetch['price'],$fetch['qty'],$fetch['total'],  ($fetch['po_desc']." ".$fetch['po_remarks'])   );				 	
  }         
$conn = null;             

echo json_encode($output);
?>    
