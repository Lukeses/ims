<?php
    include('../../../mn/include/connect.php');


  $gd_name = $_POST['gd_name'];
  $sup_id = $_POST['sup_id'];
  $vessel = $_POST['vessel'];  
  $type = $_POST['type'];
  $by = $_POST['by'];  
  $startDate = $_POST['startDate'];
  $endDate = $_POST['endDate'];
  $start = date('Y-m-d', strtotime(str_replace('-', '/', $startDate)));
  $end = date('Y-m-d', strtotime(str_replace('-', '/', $endDate))); 



  if($by == 'supplier'){
    if($type=='all'){
        $sql = "SELECT po_id,po_ref,po_pr_no,po_date,po_desc,po_remarks,po_status,co.co_name,gr.gr_name,
      SUM(pp.pp_qty*pv.pv_price) as tot
      FROM purchase_order po, company co, group_div gr,purchase_product pp, product_version pv, product p
      WHERE (po.po_co_id = co.co_id)
      AND (co.co_gr_id = gr.gr_id)
      AND (po.po_id = pp.pp_po_id)
      AND (pv.pv_id = pp.pp_prod_id)
      AND (p.prod_id = pv.pv_prod_id)
      AND (gr.gr_id = ?)
      AND (po.po_status != 'deleted')
      AND (p.prod_sup_id = ?)
      GROUP BY po_id
      ORDER BY po_id DESC";
    }
    else if($type=='specific'){
        $sql = "SELECT po_id,po_ref,po_pr_no,po_date,po_desc,po_remarks,po_status,co.co_name,gr.gr_name,
      SUM(pp.pp_qty*pv.pv_price) as tot
      FROM purchase_order po, company co, group_div gr,purchase_product pp, product_version pv, product p
      WHERE (po.po_co_id = co.co_id)
      AND (co.co_gr_id = gr.gr_id)
      AND (po.po_id = pp.pp_po_id)
      AND (pv.pv_id = pp.pp_prod_id)
      AND (p.prod_id = pv.pv_prod_id)
      AND (gr.gr_id = ?)
      AND (po.po_status != 'deleted')
      AND (p.prod_sup_id = ?)
      AND (po.po_date BETWEEN ? AND ?)
      GROUP BY po_id
      ORDER BY po_id DESC";
    }
  }
  else if($by == 'vessel'){
    if($type=='all'){
        $sql = "SELECT po_id,po_ref,po_pr_no,po_date,po_desc,po_remarks,po_status,co.co_name,gr.gr_name,
        SUM(pp.pp_qty*pv.pv_price) as tot
        FROM purchase_order po, company co, group_div gr,purchase_product pp, product_version pv
        WHERE (po.po_co_id = co.co_id)
        AND (co.co_gr_id = gr.gr_id)
        AND (po.po_id = pp.pp_po_id)
        AND (pv.pv_id = pp.pp_prod_id)
        AND (gr.gr_id = ?)
        AND (po.po_desc = ?)
        AND (po.po_status != 'deleted')        
        GROUP BY po_id
        ORDER BY po_id DESC";
    }
    else if($type=='specific'){
        $sql = "SELECT po_id,po_ref,po_pr_no,po_date,po_desc,po_remarks,po_status,co.co_name,gr.gr_name,
        SUM(pp.pp_qty*pv.pv_price) as tot
        FROM purchase_order po, company co, group_div gr,purchase_product pp, product_version pv
        WHERE (po.po_co_id = co.co_id)
        AND (co.co_gr_id = gr.gr_id)
        AND (po.po_id = pp.pp_po_id)
        AND (pv.pv_id = pp.pp_prod_id)
        AND (gr.gr_id = ?)
        AND (po.po_desc = ?)
        AND (po.po_date BETWEEN ? AND ?)
        AND (po.po_status != 'deleted')        
        GROUP BY po_id
        ORDER BY po_id DESC";
    }
  }

  


  $q = $conn->prepare($sql);


  if($by == 'supplier'){
    if($type=='all'){
      $q -> execute(array($gd_name,$sup_id));
    }
    else if($type=='specific'){
      $q -> execute(array($gd_name,$sup_id,$start,$end));
    }    
  }
  else if($by == 'vessel'){
    if($type=='all'){
      $q -> execute(array($gd_name,$vessel));
    }
    else if($type=='specific'){
      $q -> execute(array($gd_name,$vessel,$start,$end));
    }    
  }

  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['po_id'],$fetch['po_ref'],$fetch['po_pr_no'],
      $fetch['po_date'],ucwords($fetch['po_status']),
      $fetch['co_name'],  ($fetch['po_desc']." ".$fetch['po_remarks']) ,$fetch['tot']);          
  }         
$conn = null;             

echo json_encode($output);
?>    
