



function checkbox_fetch_po(){
  if( $('#po_checkbox').prop('checked') ){
    $('#div_doc_fetch').css('display','block');
    $('#doc_fetch').val('');
    $('#chk_doc_fetch').val('');
    $('#doc_fetch').prop('disabled',false);
    clear_list();
    $('#lbl_grandtotal').text('0.00');
    $("#pr_table").find("tr:gt(0)").remove();     
  }
  else{
    $('#doc_fetch').val('');
    $('#div_doc_fetch').css('display','none');   
    $('#btn_doc_fetch').prop('disabled',true);     
     clear_list();            
    $("#pr_table").find("tr:gt(0)").remove(); 
    $('#chk_doc_fetch').val('');   
    $('#lbl_grandtotal').text('0.00');     
  }
}

function checking_doc_fetch(get_val)
{
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_fetch.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#btn_doc_fetch').prop('disabled',false);        
        $('#chk_doc_fetch').val(s[1]);
        $('#doc_fetch').val(get);
      }
      else{
        $('#btn_doc_fetch').prop('disabled',true);                
        $('#chk_doc_fetch').val('no-match');        
      }
    }
  }) 
} // checking_doc_desti

function checking_doc_desti(get_val)
{
  
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_desti.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_desti').val(s[1]);
        $('#doc_desti').val(get);
      }
      else
        $('#chk_doc_desti').val('no-match');        
    }
  }) 
} // checking_doc_desti

function populate_warehouse_dropdown(){ 
  //ajax now
  $.ajax ({
    type: "POST",
    url: "serverside/populate_warehouse_dropdown.php",
    dataType: 'json',      
    cache: false,
    success: function(s)
    {
      $('#doc_desti').empty();
      $('#doc_desti').html('<option selected="selected" value="none">--SEARCH WAREHOUSE--</option>');
      $('#doc_desti').append('<option value="origin">Kaingin Warehouse</option>');
      for(var i = 0; i < s.length; i++) { 
        $('#doc_desti').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
      }       
    }  
  }); 
  //ajax end  
  $('#doc_desti').select2().select2('val','none');                      
} //

function checking_doc_op(get_val)
{
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_op.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_op').val(s[1]);
        $('#doc_op').val(get);
      }
      else
        $('#chk_doc_op').val('no-match');        
    }
  }) 
} // checking_doc_desti

function checking_doc_ve(get_val)
{
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_ve.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_ve').val(s[1]);
        $('#doc_ve').val(get);
      }
      else
        $('#chk_doc_ve').val('no-match');        
    }
  }) 
} // checking_doc_desti

function populate_var_dropdown(get_prod_name){
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/populate_var_dropdown.php',
    data: 'prod_name='+get_prod_name,
    dataType: 'json',
    success:function(s)
    {
      $('#doc_var').empty();
      $('#doc_var').append('<option value="none">--Select UOM--</option>');
      for(var i = 0; i < s.length; i++) { 
        $('#doc_var').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
      }    
    } //.function(s)
  })// .ajax*/
}     // .populate_var_dropdown

function getPrice(get_prod_id)
{
  $('#doc_price').val('');

  $.ajax
  ({
    type: 'POST',
    url: 'serverside/getPrice.php',
    data: 'prod_id='+get_prod_id,
    dataType: 'json',
    success:function(s){
        $('#doc_price').val(s[0][0]);
    }
  })
} // /.getPrice



function add_row() {   
    var prod_id = $('#doc_var').val();
        $.ajax
        ({
          type: 'POST',
          url: 'serverside/add_row.php',
          data: 'prod_id='+prod_id,
          dataType: 'json',
          success:function(s)
          {                        
            var price = (parseFloat( $('#doc_price').val(),10  )).toFixed(2);
            var prays = price.toString().replace('.', '');
              for(var i=0; i<s.length;i++)
              {            
                var A = s[i][0]; // prodid
                var B = s[i][1]; // prod_name
                var C = s[i][2]; // prod_sku
                var D =  s[i][3]; // prod_supplier
                var E = s[i][4]; //prod_category
                var F = s[i][5]; //prod_var
                var G = parseFloat($('#doc_price').val(),10).toFixed(2); //prod_price
                var H = parseInt($('#doc_qty').val()); // qty
              }//.for loop [i]   


          if($('#'+A+prays).length){


          var current = uncomma($('#qty'+A+prays).text());
          current = parseInt(current)+parseInt(H);
          $('#qty'+A+prays).html(comma(current));    

          currentTOT = parseFloat(current,10)*parseFloat(G,10);
          $('#sub'+A+prays).html(comma(currentTOT.toFixed(2)));
          clear_list(); 
          $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );          
          $('#btn_proceed').prop('disabled',false);
             
          }                
          else{
            $('#pr_table tr:last').after('<tr id='+A+prays+'> <td>'+B+'</td><td>'+C+'</td><td>'+D+'</td><td>'+E+'</td><td>'+F+'</td><td>'+comma(G)+'</td>      <td id='+"qty"+A+prays+ '>'+H+'</td><td id=sub'+A+prays+'>'+comma((H*G))+'</td> <td><button value='+A+' onclick="delRow(this.value,'+prays+')" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');            
             clear_list();
             $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );
              $('#btn_proceed').prop('disabled',false);

          } 
          $('#btn_proceed').prop('disabled',false); 
          } //.function(s)          
        })// end.ajax
}

function add_row_ve() {   
    var ve_id = $('#chk_doc_ve').val();
    var ve_no = $('#doc_ve').val();

      if($('#'+ve_id).length){
        alert('Message: Already in Vehicle List');
      }
      else{
        $('#ve_table tr:last').after('<tr id='+ve_id+'> <td>'+ve_no+'</td><td><button value='+ve_id+' onclick="delRow_ve(this.value)" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');                    
        $('#doc_ve').val('');
        $('#chk_doc_ve').val('');
      }       
}

function add_row_op() {   
    var emp_name = $('#doc_op').val();
    var emp_id = $('#chk_doc_op').val();
    var role = $('#doc_role').val();

      if($('#'+emp_id).length){
        alert('Message: Already in Operation List');
      }
      else{
        $('#op_table tr:last').after('<tr id='+emp_id+'> <td>'+emp_name+'</td><td>'+role.toUpperCase()+'</td><td><button value='+emp_id+' onclick="delRow_op(this.value)" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');                    
        $('#doc_op').val('');
        $('#chk_doc_op').val('no-match');
        $('#doc_role').val('none');
      }       
}

function delRow(get,get_prays){
  var lastdata = uncomma($('#sub'+get+get_prays).text()); 
  var tot_val = parseFloat(uncomma($('#lbl_grandtotal').text()),10) - parseFloat(lastdata,10);
  $('#lbl_grandtotal').html(comma(tot_val.toFixed(2)));
  $('#pr_table #'+get+get_prays).remove();
}

function delRow_ve(get){
  $('#ve_table #'+get).remove();
}

function delRow_op(get){
  $('#op_table #'+get).remove();
}

function clear_list(){
  $('#doc_product').val('');
  $('#doc_var').val('none');
  $('#doc_var').empty();
  $('#doc_price').val('');
  $('#doc_qty').val('');
}

$('#btn_add_list').click(function(){
  if(validate_add_list()==true){
    $('#btn_add_list').removeClass('bg-teal')
    $('#btn_add_list').addClass('bg-red');
  }
  else if(validate_add_list()==false){
    $('#btn_add_list').removeClass('bg-red')
    $('#btn_add_list').addClass('bg-teal');
    add_row();
  }
}) // /.btn_add_list

$('#btn_add_ve').click(function(){
  if(validate_add_ve()==true){
    $('#btn_add_ve').removeClass('bg-teal')
    $('#btn_add_ve').addClass('bg-red');
  }
  else if(validate_add_ve()==false){
    $('#btn_add_ve').removeClass('bg-red')
    $('#btn_add_ve').addClass('bg-teal');
    add_row_ve();
  }
}) // /.btn_add_list

$('#btn_add_op').click(function(){
  if(validate_add_op()==true){
    $('#btn_add_op').removeClass('bg-teal')
    $('#btn_add_op').addClass('bg-red');
  }
  else if(validate_add_op()==false){
    $('#btn_add_op').removeClass('bg-red')
    $('#btn_add_op').addClass('bg-teal');
    add_row_op();
  }
}) // /.btn_add_list


function validate_add_op(){
  var err = false;

  if($('#chk_doc_op').val()=='no-match' || $('#doc_op').val()=='' || $('#doc_op').val()==null){
    err = true;
    $('#err_doc_op').text('REQUIRED');
  }
  else
    $('#err_doc_op').text('');   

  if( $('#doc_role').val() == 'none' ){
    err = true;
    $('#err_doc_role').text('REQUIRED');
  }
  else
    $('#err_doc_role').text('');     

  return err; 
}


function validate_add_ve(){
  var err = false;

  if($('#chk_doc_ve').val()=='no-match' || $('#doc_ve').val()=='' || $('#doc_ve').val()==null){
    err = true;
    $('#err_doc_ve').text('REQUIRED');
  }
  else
    $('#err_doc_ve').text('');   

  return err; 
}

function validate_add_list(){
  var err = false;

  if($('#doc_var').val()=='none' || $('#doc_var').val()==null){
    err = true;
    $('#div_doc_var').addClass('has-error');
  }
  else
    $('#div_doc_var').removeClass('has-error');   

  if($('#doc_price').val()=='' || $('#doc_price').val()<0){
    err = true;
    $('#div_doc_price').addClass('has-error');
  }
  else
    $('#div_doc_price').removeClass('has-error');   

  if($('#doc_qty').val()=='' || $('#doc_qty').val()<=0){
    err = true;
    $('#div_doc_qty').addClass('has-error');
  }
  else
    $('#div_doc_qty').removeClass('has-error');   


  return err; 
}

$('#btn_proceed').click(function(){  
  if(validate_proceed()==true){}
  else{
    $('#total_at_modal').text('PHP '+ $('#lbl_grandtotal').text()  );
    $('#confirm_modal').modal('show');
  }  
})

function validate_proceed(){
  var err = false;
  var pr = $('#pr_table tr').length-1;
  var ve = $('#ve_table tr').length-1;
  var op = $('#op_table tr').length-1;

  if(pr==0){
    err=true;    
    $('#btn_add_list').removeClass('bg-teal');    
    $('#btn_add_list').addClass('bg-red');
  }else{
    $('#btn_add_list').removeClass('bg-red');    
    $('#btn_add_list').addClass('bg-teal');    
  }

  if(ve==0){
    $('#btn_add_ve').removeClass('bg-teal');    
    $('#btn_add_ve').addClass('bg-red'); 
    err=true;
  }
  else{
    $('#btn_add_ve').removeClass('bg-red');    
    $('#btn_add_ve').addClass('bg-teal'); 
  }

  if(op==0){
    $('#btn_add_op').removeClass('bg-teal');    
    $('#btn_add_op').addClass('bg-red'); 
    err=true;
  }
  else{
    $('#btn_add_op').removeClass('bg-red');    
    $('#btn_add_op').addClass('bg-teal');
  }


  if($('#doc_date').val()==''){
    err = true;
    $('#err_doc_date').text('REQUIRED');
  }
  else
    $('#err_doc_date').text('');   

  if($('#doc_ref').val()==''){
    err = true;
    $('#err_doc_ref').text('REQUIRED');
  }
  else
    $('#err_doc_ref').text('');   

  if($('#doc_desti').val()=='none' || $('#doc_desti').val()=='' || $('#doc_desti').val()==null){
    err = true;
    $('#err_doc_desti').text('REQUIRED');
  }
  else
    $('#err_doc_desti').text(''); 


  return err; 
}


function save_record(get_status){
  $('#btn_save_pending').prop('disabled',true);
  $('#btn_save_shipped').prop('disabled',true);
  var doc_date = $('#doc_date').val();
  var doc_ref = $('#doc_ref').val();
  var doc_remarks = $('#doc_remarks').val();
  var doc_desti = $('#doc_desti').val();
  var doc_status = get_status;
  var doc_fetch = $('#doc_fetch').val();
  var dataString_top = 'doc_date='+doc_date+'&doc_ref='+doc_ref+'&doc_remarks='+doc_remarks+'&doc_desti='+doc_desti+'&doc_status='+doc_status+'&doc_fetch='+doc_fetch;
      //vehicle fetch table      
    var dataString_ve = "";
    var iterator_ve = 0;
    var rowCount_ve = 0;  
    $('#ve_table td').each(function() 
    {
          var cellValue = $(this).find('button').val();
          var column = iterator_ve % 2;
          //var row = Math.round(iterator_ve / 3);
          var row = Math.floor(iterator_ve / 2);
 
          if(column == 1) {
            dataString_ve += "ve_id[" + row + "]=" + cellValue + "&";
          } 
          rowCount_ve = row + 1;
          iterator_ve++;
    });    
    dataString_ve += "rowCount_ve=" + rowCount_ve;
      //operation fetch table
    var dataString_op = "";
    var iterator_op = 0;
    var rowCount_op = 0;  
    $('#op_table td').each(function() 
    {
          var cellValue = $(this).find('button').val();
          var column = iterator_op % 3;
          //var row = Math.round(iterator_op / 3);
          var row = Math.floor(iterator_op / 3);
 
          if(column == 2) {
            dataString_op += "emp_id[" + row + "]=" + cellValue + "&";
          } 
          if(column == 1) {
            dataString_op += "emp_role[" + row + "]=" + $(this).html() + "&";
          }          
          rowCount_op = row + 1;
          iterator_op++;
    });    
    dataString_op += "rowCount_op=" + rowCount_op;
    //product fetch table
    var dataString_pr = "";
    var iterator_pr = 0;
    var rowCount_pr = 0;  
    $('#pr_table td').each(function() 
    {
          var cellValue = $(this).find('button').val();
          var column = iterator_pr % 9;
          //var row = Math.round(iterator_pr / 3);
          var row = Math.floor(iterator_pr / 9);
 
          if(column == 8) {
            dataString_pr += "pr_id[" + row + "]=" + cellValue + "&";
          } 
          if(column == 6) {
            dataString_pr += "pr_qty[" + row + "]=" + uncomma($(this).html()) + "&";
          }    
          if(column == 5) {
            dataString_pr += "pr_price[" + row + "]=" + uncomma($(this).html()) + "&";
          }                     
          rowCount_pr = row + 1;
          iterator_pr++;
    });    
    dataString_pr += "rowCount_pr=" + rowCount_pr;

    dataString = dataString_ve+'&'+dataString_op+'&'+dataString_pr+'&'+dataString_top;
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/insert_data.php",
      data: dataString,
      cache: false,
        success: function(s)
        {
          $('#confirm_modal').modal('hide');
          if(s==0){
            toastr.success('SUCCESS:  Document Record Saved');                          
            clear_list();
            $('#doc_ve').val('');
            $('#chk_doc_ve').val('');                       
            $('#doc_op').val('');
            $('#chk_doc_op').val('');
            $('#doc_role').val('none');
            $('#lbl_grandtotal').text('0.00');
            $('#doc_ref').val('');
            $('#doc_remarks').val('');            
            populate_warehouse_dropdown();
            $('#err_doc_date').text('');
            $('#err_doc_ref').text('');
            $('#err_doc_source').text(''); 
            $('#err_doc_desti').text('');              
            $("#pr_table").find("tr:gt(0)").remove();
            $("#ve_table").find("tr:gt(0)").remove();
            $("#op_table").find("tr:gt(0)").remove();
          $('#btn_save_pending').prop('disabled',false);
          $('#btn_save_shipped').prop('disabled',false);
          }
          else if(s==1){
            $(this).prop('disabled',false);
            toastr.danger('FAILED:  Document Failed Saved, Database No Connection');                                                  
          }
        }
    }); 
    //ajax end 
}

$('#btn_doc_fetch').click(function(){
  $(this).prop('disabled',true);  
  doc_fetch($('#doc_fetch').val());  
})

function doc_fetch(fetch_id){
  $('#doc_fetch').prop('disabled',true);
  $("#pr_table").find("tr:gt(0)").remove(); 
  $('#lbl_grandtotal').text('0.00');
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/fetch_po.php',
    data: "po_id="+fetch_id,
    dataType: 'json',
    success: function(s)
    {
      for(var i = 0; i < s.length; i++){
        var prays =s[i][6].toString().replace('.', '');              
        var A = s[i][0]; // prodid
        var B = s[i][1]; // prod_name
        var C = s[i][2]; // prod_sku
        var D =  s[i][3]; // prod_supplier
        var E = s[i][4]; //prod_category
        var F = s[i][5]; //prod_var
        var G = parseFloat(s[i][6],10).toFixed(2); //prod_price
        var H = parseInt(s[i][7]); // qty

        $('#pr_table tr:last').after('<tr id='+A+prays+'> <td>'+B+'</td><td>'+C+'</td><td>'+D+'</td><td>'+E+'</td><td>'+F+'</td><td>'+comma(G)+'</td>      <td id='+"qty"+A+prays+ '>'+H+'</td><td id=sub'+A+prays+'>'+comma((H*G))+'</td> <td><button value='+A+' onclick="delRow(this.value,'+prays+')" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');            
        $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );
        $('#btn_proceed').prop('disabled',false);        
      }      
    }
  })   
}


if($('#post_po_no').val()!=''){
  $('#doc_fetch').val( $('#post_po_no').val() );
  doc_fetch($('#post_po_no').val());
}

//additional functions
  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 
  function confirmCancel(){

    var choice = confirm("Are you sure you want to Cancel Record?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }       

  function logout(){

    var choice = confirm("Are you sure you want to Log-out?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }  

  function select_groupdiv(){
    populate_gr_dropdown();    
    $('#gd_name').select2().select2('val','none');              
    $('#gd_nameDiv').removeClass('has-error');
    $('#gd_modal').modal('show');    
  }    

  function populate_gr_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_gd_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#gd_name').empty();
        $('#gd_name').append('<option value="none">--Select Group--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
  } // 

  $('#btn_gd_submit').click(function(){
    if($('#gd_name').val()=='none'){
      $('#gd_nameDiv').addClass('has-error');
    }
    else{
      $('#btn_select_groupdiv').removeClass('bg-red');
      $('#btn_select_groupdiv').addClass('bg-green');         
      $('#gd_nameDiv').removeClass('has-error');
      $('#gd_selected').val($('#gd_name').val());
      $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );            
      $('#gd_submit_name').val( $( "#gd_name option:selected" ).text()  );   
      $('#gd_modal').modal('hide');      
        $.ajax({
          type: 'GET',
          url: 'serverside/session_gd_selected.php',
          dataType: 'json',
          data: 'gd='+$('#gd_name').val(),
          success: function(s) {
        }
      });                   
    }
  })    
// .addition functions  
