
  //tables
      var pr_table = $('#pr_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
        "aaSorting": []
      });  //Initialize the datatable department
      var op_table = $('#op_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
        "aaSorting": [],"bPaginate": false
      });  //Initialize the datatable department
      var ve_table = $('#ve_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
        "aaSorting": [],"bPaginate": false
      });  //Initialize the datatable department            
  //.tables  




  function load_top_labels(get_doc_no){

    //ajax
    $.ajax ({
      type: "POST",
      url: "serverside/view_top_labels.php",
      dataType: 'json',      
      data: 'doc_no='+get_doc_no,
      cache: false,
      success: function(s){
        for(var i = 0; i < s.length; i++) { 
          $('#lbl_doc_ref').text(s[i][0]);  $('#lbl_doc_date').text(s[i][1]);
          $('#lbl_grand_total').text(comma(s[i][6])); $('#lbl_doc_status').text(s[i][2].toUpperCase());
          $('#lbl_doc_gd').text(s[i][3]);  $('#lbl_doc_desti').text(s[i][5]); $('#lbl_doc_remarks').text(s[i][7]);
          if(s[i][2]!='deleted'){
            $('#btn_deliver').css('display','none');                
            $('#btn_doc_delete').css('display','block');            
          }
          if(s[i][2]=='pending'){
            $('#btn_doc_shipped').css('display','block');          
            $('#btn_doc_shipped').prop('disabled',false);
            $('#btn_deliver').css('display','none');                                                  
          }
          if(s[i][2]=='received'){
            if(s[i][8]==0)
            $('#btn_deliver').css('display','block');                
          }

        }       
        load_pr_table(get_doc_no);
        load_op_table(get_doc_no);  
        load_ve_table(get_doc_no);                
      }  
    }); 
      //ajax end    
  }

  function load_pr_table(get_doc_no){ 
    //ajax now
    pr_table.fnClearTable();            
    $.ajax ({
      type: "POST",
      url: "serverside/view_pr_table.php",
      dataType: 'json',      
      data: 'doc_no='+get_doc_no,
      cache: false,
      success: function(s)
      {
        for(var i = 0; i < s.length; i++) 
        {       
          pr_table.fnAddData
          ([
            s[i][1],s[i][2],s[i][3],s[i][4],s[i][5],s[i][6],comma(s[i][7]),comma(s[i][8]),comma(s[i][9]),
          ],false); 
          pr_table.fnDraw();

        }       
      }  
    }); 
    //ajax end  
  } //.load pr_table

  function load_op_table(get_doc_no){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/view_op_table.php",
      dataType: 'json',      
      data: 'doc_no='+get_doc_no,
      cache: false,
      success: function(s)
      {
        for(var i = 0; i < s.length; i++) 
        {       
          op_table.fnAddData
          ([
            s[i][1],s[i][2],s[i][3],s[i][4],
          ],false); 
          op_table.fnDraw();

        }       
      }  
    }); 
    //ajax end  
  } //.load op_table

  function load_ve_table(get_doc_no){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/view_ve_table.php",
      dataType: 'json',      
      data: 'doc_no='+get_doc_no,
      cache: false,
      success: function(s)
      {
        for(var i = 0; i < s.length; i++) 
        {       
          ve_table.fnAddData
          ([
            s[i][1],s[i][2],s[i][3],
          ],false); 
          ve_table.fnDraw();

        }       
      }  
    }); 
    //ajax end  
  } //.load ve_table


  $('#btn_deliver').click(function(){
    $('<form action="../dn/add.php" method="POST" target="_self">' + 
    '<input type="hidden" name="doc_no" value="'+ $(this).val()  +'">' +
    '</form>').submit();
  })


  $('#btn_doc_delete').click(function(){
    $('#btnDel').val($(this).val());
    $('#delModal').modal('show');
  })

  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/view_delete.php',
      data: 'doc_id='+$(this).val(),
      success: function(s) {          
        if(s==0){
          $('#lbl_doc_status').text('DELETED');
          $('#btn_doc_delete').prop('disabled',true);
          $('#btn_doc_shipped').prop('disabled',true);
          $('#delModal').modal('hide');     
          toastr.error('Document Deleted');
        }
        else if(s==1){
          $('#delModal').modal('hide');     
          toastr.warning('Error: No Connection');          
        }               
      }
    });  
  });

  $('#btn_doc_shipped').click(function(){
    $("#update_table").find("tr:gt(0)").remove();
    $.ajax({
      type: 'POST',
      url: 'serverside/view_update_shipped.php',
      data: 'doc_id='+$(this).val(),
      dataType: 'json',
      success: function(s) {          
        for(var i = 0; i < s.length; i++) {    
          $('#update_table tr:last').after('<tr id=tr'+s[i][0]+'> <td>'+s[i][0]+'</td> <td>'+s[i][1]+'</td> <td>'+s[i][2]+'</td> <td>'+s[i][3]+'</td> <td>'+s[i][6]+'</td><td>'+s[i][7]+'</td> <td>'+s[i][4]+'</td> <td> <input type="number" value="'+s[i][5]+'" class="form-control input-xs"></td>   </tr>');                               
          $('#update_modal').modal({backdrop: 'static'}) ;             
        }  
      }
    });  
  })

  $('#btn_update').click(function(){
    $(this).prop('disabled',true);

      //vehicle fetch table      
    var dataString = "";
    var iterator = 0;
    var rowCount = 0;  
    $('#update_table td').each(function() 
    {
          var cellValue = $(this).find('input').val();
          var column = iterator % 8;
          //var row = Math.round(iterator / 3);
          var row = Math.floor(iterator / 8);
 
          if(column == 0) {
            dataString += "pv_id[" + row + "]=" + $(this).text() + "&";
          } 
          if(column == 7) {
            dataString += "pv_dp_qty[" + row + "]=" + cellValue + "&";
          }           
          rowCount = row + 1;
          iterator++;
    });    

    dataString += "rowCount=" + rowCount+ '&doc_id='+$('#btn_doc_shipped').val();

    $.ajax({
      type: 'POST',
      url: 'serverside/view_update_shipped_save.php',
      data: dataString,
      cache: false,
      success: function(s) {          
        if(s==0){
          $('#btn_doc_shipped').prop('disabled',true);
          $('#btn_doc_shipped').css('display','none');          
          $('#update_modal').modal('hide');     
          toastr.info('SUCCESS: Document Updated');
          load_top_labels($('#btn_doc_shipped').val());
        }
        else if(s==1){
          $(this).prop('disabled',false);
          toastr.warning('Error: No Connection');          
        }               
      }
    });
  })



//additional functions


  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 


  function logout(){

    var choice = confirm("Are you sure you want to Log-out?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }  
//.addition funcitons  