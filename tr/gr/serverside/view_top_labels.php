<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT doc_ref,doc_date,doc_status,gr_name,
d.doc_source as source, (SELECT loc_name FROM location l WHERE l.loc_id = d.doc_desti) as desti,
SUM(dp.dp_qty*pv.pv_price) as tot, doc_remarks,
(SELECT COUNT(*) FROM document x WHERE x.doc_type = 'DN' and x.doc_fetch = ?  and x.doc_status != 'deleted') as delstat
FROM document d,group_div gr, document_product dp, product_version pv
WHERE (d.doc_gr_id = gr.gr_id)
AND (dp.dp_doc_id = d.doc_id)
AND (dp.dp_prod_id = pv.pv_id)
AND (d.doc_id = ?)
GROUP BY d.doc_id
ORDER BY doc_status desc, doc_date desc";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no,$doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {

    if($fetch['doc_status']=='shipped')
      $status_string = "received";
    else
      $status_string = $fetch['doc_status'];

    $output[] = array ($fetch['doc_ref'],$fetch['doc_date'],$status_string,
      $fetch['gr_name'],$fetch['source'],$fetch['desti'],$fetch['tot'],
      $fetch['doc_remarks'],$fetch['delstat']);          
  }         
$conn = null;             

echo json_encode($output);
?>    