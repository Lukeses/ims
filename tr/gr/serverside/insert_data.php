
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../../mn/include/connect.php');  
    include('../../../mn/include/log.php');  


    if(!isset($_SESSION)) { 
      session_start(); 
    } 
    $gd_selected = $_SESSION['gd_selected'];

    //vehicle array
    $rowCount_ve = $_POST['rowCount_ve'];
    $ve_id = $_POST['ve_id'];
    //operation array
    $rowCount_op = $_POST['rowCount_op'];
    $emp_id = $_POST['emp_id'];   
    $emp_role = $_POST['emp_role'];   
    //product array
    $rowCount_pr = $_POST['rowCount_pr'];
    $pr_id = $_POST['pr_id'];   
    $pr_qty = $_POST['pr_qty'];   
    $pr_price = $_POST['pr_price'];   

    $doc_date = $_POST['doc_date'];
    $doc_ref = $_POST['doc_ref'];
    $doc_remarks = $_POST['doc_remarks'];
    $doc_status = $_POST['doc_status'];
    $doc_fetch = $_POST['doc_fetch'];
    $doc_desti = $_POST['doc_desti'];


    //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('doc'.$year);   

    $sql = "INSERT INTO document values(?,?,?,?,?,?,?,?,?,?)"; //INSERT recordd
    $q = $conn -> prepare($sql);
    $q -> execute(array($id,$gd_selected,'GRN','virtual',$doc_desti,$doc_ref,$doc_remarks,$doc_date,$doc_status,$doc_fetch));

    if($doc_status == 'shipped'){
        $sql = "UPDATE purchase_order SET po_status = ? WHERE po_id = ?"; //INSERT recordd
        $q = $conn -> prepare($sql);
        $q -> execute(array('shipped',$doc_fetch));
    }

    $trail_id =uniqid('at'.$year);  
    $sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
    $q = $conn -> prepare($sql);    
    $q -> execute(array($trail_id,'Transaction','GRN', 'CREATE', 'New: ID:'.$id. ', Ref:'.$doc_ref.', Date:'.$doc_date.', Destination ID:'.$doc_desti.', Status:'.$doc_status, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));
            
            // INSERT DATA TO document_vehicle
    for($x=0; $x<$rowCount_ve; $x++){   
        $dv_id =uniqid('dv'.$year);  

        $sql = "INSERT INTO document_vehicle VALUES(?,?,?)";
        $q = $conn -> prepare($sql);
        $q -> execute(array($dv_id,$id,$ve_id[$x]));
  
    }    

            // INSERT DATA TO document_operation
    for($x=0; $x<$rowCount_op; $x++){   
        $do_id =uniqid('do'.$year);  

        $sql = "INSERT INTO document_operation VALUES(?,?,?,?)";
        $q = $conn -> prepare($sql);
        $q -> execute(array($do_id,$id,$emp_id[$x],$emp_role[$x]));
    }    


        //check product version
    for($x=0; $x<$rowCount_pr; $x++){   

        $pv_id =uniqid('pv'.$year);  
        $dp_id =uniqid('dp'.$year);  

        $pv_count;
        $sql = "SELECT COUNT(*) as counter FROM product_version pv 
        WHERE (pv_prod_id = ? )
        AND (pv_price = ?) ";
        $q = $conn->prepare($sql);
        $q -> execute(array($pr_id[$x],$pr_price[$x]));
        $browse = $q -> fetchAll();
        foreach($browse as $fetch)
            $pv_count = $fetch['counter'];

        if($pv_count == 0){
            $sql = "INSERT INTO product_version VALUES(?,?,?,NOW(),?)";
            $q = $conn -> prepare($sql);
            $q -> execute(array($pv_id,$pr_id[$x],$pr_price[$x],'active'));    

            $trail_id =uniqid('at'.$year);  
            $sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
            $q = $conn -> prepare($sql);    
            $q -> execute(array($trail_id,'Transaction','GRN-Product Version', 'CREATE', 'New: ID:'.$pv_id.', Product ID:'.$pr_id[$x].', Version Price:'.$pr_price[$x], date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

        }

        $sql = "INSERT INTO document_product VALUES(?,?,
            (SELECT pv_id FROM product_version WHERE pv_prod_id=? AND pv_price=?)
            ,?)";
        $q = $conn -> prepare($sql);
        $q -> execute(array($dp_id,$id,$pr_id[$x],$pr_price[$x],$pr_qty[$x]));        

    }        
$conn = null;



echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
