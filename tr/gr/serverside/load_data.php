<?php
    include('../../../mn/include/connect.php');

    $gd_name = $_POST['gd_name'];

  $sql = "SELECT d.doc_id,doc_ref,doc_remarks,doc_date,doc_status,gr_name,loc_name,
  SUM(dp.dp_qty*pv.pv_price) as tot, 
(SELECT COUNT(*) FROM document x WHERE x.doc_type = 'DN' and x.doc_fetch = d.doc_id  and x.doc_status != 'deleted') as delstat
  FROM document d,location l,group_div gr, document_product dp, product_version pv 
  WHERE (d.doc_desti = l.loc_id) 
  AND (d.doc_type = 'GRN') 
  AND (d.doc_gr_id = gr.gr_id) 
  AND (dp.dp_doc_id = d.doc_id) 
  AND (dp.dp_prod_id = pv.pv_id) 
  AND (gr.gr_id= ?) 
  GROUP BY d.doc_id 
  ORDER BY doc_date desc, doc_status asc";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_name));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    if($fetch['doc_status']=='shipped')
      $status_string = "received";
    else
      $status_string = $fetch['doc_status'];
    if($fetch['delstat']==0)
      $delstat = 'PENDING';
    else
      $delstat = 'DELIVERED';

    $output[] = array ($fetch['doc_id'],$fetch['doc_ref'],$fetch['doc_remarks'],
      $fetch['doc_date'],ucwords($status_string),$fetch['gr_name'],
      $fetch['loc_name'],$fetch['tot'],$delstat);          
  }         
$conn = null;             

echo json_encode($output);
?>    