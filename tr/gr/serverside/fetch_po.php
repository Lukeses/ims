<?php
    include('../../../mn/include/connect.php');

  $po_id = $_POST['po_id'];


  $sql = "SELECT pr.prod_id, pr.prod_name,pr.prod_sku,pr.prod_var,pv.pv_price,pp.pp_qty,
(SELECT cat_name FROM category c WHERE c.cat_id = pr.prod_cat_id) as category,
(SELECT sup_name FROM supplier s WHERE s.sup_id = pr.prod_sup_id) as supplier,
pv.pv_price
FROM product_version pv, product pr, purchase_product pp 
WHERE (pp.pp_po_id = ?) 
AND (pp.pp_prod_id = pv.pv_id)
AND (pv.pv_prod_id = pr.prod_id)
GROUP BY pv.pv_id";

  $q = $conn->prepare($sql);
  $q -> execute(array($po_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['prod_id'],$fetch['prod_name'],$fetch['prod_sku'],
      $fetch['supplier'],$fetch['category'],$fetch['prod_var'],$fetch['pv_price'],$fetch['pp_qty']);          
  }         
$conn = null;             

echo json_encode($output);
?>    