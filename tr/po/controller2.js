




function populate_supplier_dropdown(){ 
  //ajax now
  $.ajax ({
    type: "POST",
    url: "../../mn/pr/serverside/populate_supplier_dropdown.php",
    dataType: 'json',      
    cache: false,
    success: function(s)
    {
      $('#pr_sup').empty();
      $('#pr_sup').html('<option selected="selected" value="none">--SEARCH SUPPLIER--</option>');
      for(var i = 0; i < s.length; i++) { 
        $('#pr_sup').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
      }       
    }  
  }); 
  //ajax end  
  $('#pr_sup').select2().select2('val','none');                      
} //

function populate_product_dropdown(supplier_id){ 
  //ajax now
  $.ajax ({
    type: "POST",
    url: "serverside/search_doc_product.php",
		data: 'supplier_id='+supplier_id,
    dataType: 'json',      
    cache: false,
    success: function(s)
    {
      $('#doc_product').empty();
      $('#doc_product').html('<option selected="selected" value="none">--SEARCH PRODUCT--</option>');
      for(var i = 0; i < s.length; i++) { 
        $('#doc_product').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
      }       
    }  
  }); 
  //ajax end  
  $('#doc_product').select2().select2('val','none');                      
} //



function checking_doc_co(get_val)
{
  $.ajax  
  ({
    type: 'POST',
    url: 'serverside/checking_doc_co.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_co').val(s[1]);
        $('#doc_co').val(get_val);     
        var matches = get_val.match(/\b(\w)/g);  
        var acronym = matches.join('');  
        $('#doc_ref').val(acronym+'-'+s[2]);
      }
      else{
        $('#chk_doc_co').val('no-match');        
        $('#doc_ref').val('');                
      }
    }    
  }) 
} // checking_doc_co

function checking_doc_op(get_val)
{
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_op.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_op').val(s[1]);
        $('#doc_op').val(get_val);
      }
      else
        $('#chk_doc_op').val('no-match');        
    }
  }) 
} // checking_doc_co

function checking_doc_ve(get_val)
{
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_ve.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_ve').val(s[1]);
        $('#doc_ve').val(get_val);
      }
      else
        $('#chk_doc_ve').val('no-match');        
    }
  }) 
} // checking_doc_co

function populate_var_dropdown(get_prod_name){
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/populate_var_dropdown.php',
    data: 'prod_name='+get_prod_name,
    dataType: 'json',
    success:function(s)
    {
      $('#doc_var').empty();
      $('#doc_var').append('<option value="none">--Select UOM--</option>');
      for(var i = 0; i < s.length; i++) { 
        $('#doc_var').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
      }    
    } //.function(s)
  })// .ajax*/
}     // .populate_var_dropdown

function getPrice(get_prod_id)
{
  $('#doc_price').val('');

  $.ajax
  ({
    type: 'POST',
    url: 'serverside/getPrice.php',
    data: 'prod_id='+get_prod_id,
    dataType: 'json',
    success:function(s){
        $('#doc_price').val(s[0][0]);
    }
  })
} // /.getPrice



function add_row() {   
    var prod_id = $('#doc_var').val();
        $.ajax
        ({
          type: 'POST',
          url: 'serverside/add_row.php',
          data: 'prod_id='+prod_id,
          dataType: 'json',
          success:function(s)
          {                        
            var price = (parseFloat( $('#doc_price').val(),10  )).toFixed(2);
            var prays = price.toString().replace('.', '');
              for(var i=0; i<s.length;i++)
              {            
                var A = s[i][0]; // prodid
                var B = s[i][1]; // prod_name
                var C = s[i][2]; // prod_sku
                var D =  s[i][3]; // prod_supplier
                var E = s[i][4]; //prod_category
                var F = s[i][5]; //prod_var
                var G = parseFloat($('#doc_price').val(),10).toFixed(2); //prod_price
                var H = parseInt($('#doc_qty').val()); // qty
              }//.for loop [i]   


          if($('#'+A+prays).length){


          var current = uncomma($('#qty'+A+prays).text());
          current = parseInt(current)+parseInt(H);
          $('#qty'+A+prays).html(comma(current));    

          currentTOT = parseFloat(current,10)*parseFloat(G,10);
          $('#sub'+A+prays).html(comma(currentTOT.toFixed(2)));
          clear_list(); 
          $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );          
          $('#btn_proceed').prop('disabled',false);
             
          }                
          else{
            $('#pr_table tr:last').after('<tr id='+A+prays+'> <td>'+B+'</td><td>'+C+'</td><td>'+D+'</td><td>'+E+'</td><td>'+F+'</td><td>'+comma(G)+'</td>      <td id='+"qty"+A+prays+ '>'+H+'</td><td id=sub'+A+prays+'>'+comma((H*G))+'</td> <td><button value='+A+' onclick="delRow(this.value,'+prays+')" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');            
             clear_list();
             $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );
              $('#btn_proceed').prop('disabled',false);

          } 
          $('#btn_proceed').prop('disabled',false); 
          } //.function(s)          
        })// end.ajax
}

function add_row_ve() {   
    var ve_id = $('#chk_doc_ve').val();
    var ve_no = $('#doc_ve').val();

      if($('#'+ve_id).length){
        alert('Message: Already in Vehicle List');
      }
      else{
        $('#ve_table tr:last').after('<tr id='+ve_id+'> <td>'+ve_no+'</td><td><button value='+ve_id+' onclick="delRow_ve(this.value)" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');                    
        $('#doc_ve').val('');
        $('#chk_doc_ve').val('');
      }       
}

function add_row_op() {   
    var emp_name = $('#doc_op').val();
    var emp_id = $('#chk_doc_op').val();
    var role = $('#doc_role').val();

      if($('#'+emp_id).length){
        alert('Message: Already in Operation List');
      }
      else{
        $('#op_table tr:last').after('<tr id='+emp_id+'> <td>'+emp_name+'</td><td>'+role.toUpperCase()+'</td><td><button value='+emp_id+' onclick="delRow_op(this.value)" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');                    
        $('#doc_op').val('');
        $('#chk_doc_op').val('no-match');
        $('#doc_role').val('none');
      }       
}

function delRow(get,get_prays){
  var lastdata = uncomma($('#sub'+get+get_prays).text()); 
  var tot_val = parseFloat(uncomma($('#lbl_grandtotal').text()),10) - parseFloat(lastdata,10);
  $('#lbl_grandtotal').html(comma(tot_val.toFixed(2)));
  $('#pr_table #'+get+get_prays).remove();
}

function delRow_ve(get){
  $('#ve_table #'+get).remove();
}

function delRow_op(get){
  $('#op_table #'+get).remove();
}

function clear_list(){
  $('#doc_product').select2().select2('val','none');                      
  $('#doc_var').val('none');
  $('#doc_var').empty();
  $('#doc_price').val('');
  $('#doc_qty').val('');
}

$('#btn_add_list').click(function(){
  if(validate_add_list()==true){
    $('#btn_add_list').removeClass('bg-teal')
    $('#btn_add_list').addClass('bg-red');
  }
  else if(validate_add_list()==false){
    $('#btn_add_list').removeClass('bg-red')
    $('#btn_add_list').addClass('bg-teal');
    add_row();
  }
}) // /.btn_add_list


function validate_add_list(){
  var err = false;

	if($('#doc_product').val()=='none'){
    err = true;
    $('#div_doc_product').addClass('has-error');
  }
  else
    $('#div_doc_product').removeClass('has-error');   

  if($('#doc_var').val()=='none' || $('#doc_var').val()==null){
    err = true;
    $('#div_doc_var').addClass('has-error');
  }
  else
    $('#div_doc_var').removeClass('has-error');   

  if($('#doc_price').val()=='' || $('#doc_price').val()<0){
    err = true;
    $('#div_doc_price').addClass('has-error');
  }
  else
    $('#div_doc_price').removeClass('has-error');   

  if($('#doc_qty').val()=='' || $('#doc_qty').val()<=0){
    err = true;
    $('#div_doc_qty').addClass('has-error');
  }
  else
    $('#div_doc_qty').removeClass('has-error');   


  return err; 
}

$('#btn_proceed').click(function(){  
  if(validate_proceed()==true){  }
  else{
    $('#total_at_modal').text('PHP '+ $('#lbl_grandtotal').text()  );
    $('#confirm_modal').modal({backdrop: 'static'})                     
  }   
})

function validate_proceed(){
  var err = false;
  var pr = $('#pr_table tr').length-1;

  if(pr==0){
    err=true;    
    $('#btn_add_list').removeClass('bg-teal');    
    $('#btn_add_list').addClass('bg-red');
  }else{
    $('#btn_add_list').removeClass('bg-red');    
    $('#btn_add_list').addClass('bg-teal');    
  }


  if($('#doc_date').val()==''){
    err = true;
    $('#err_doc_date').text('REQUIRED');
  }
  else
    $('#err_doc_date').text('');   

  if($('#doc_ref').val()==''){
    err = true;
    $('#err_doc_ref').text('REQUIRED');
  }
  else
    $('#err_doc_ref').text('');   

  if($('#chk_doc_co').val()=='no-match' || $('#doc_co').val()=='' || $('#doc_co').val()==null){
    err = true;
    $('#err_doc_co').text('REQUIRED');
  }
  else
    $('#err_doc_co').text(''); 

  if($('#doc_terms').val()==''){
    err = true;
    $('#err_doc_terms').text('REQUIRED');
  }
  else
    $('#err_doc_terms').text('');   


  return err; 
}


function save_record(get_status){
  $('#btn_save_pending').prop('disabled',true);
  $('#btn_save_shipped').prop('disabled',true);
  $("body").css("cursor", "progress"); // busy cursor  

  var doc_date = $('#doc_date').val();
  var doc_ref = $('#doc_ref').val();
  var doc_pr_no = $('#doc_pr_no').val();
  var doc_co = $('#chk_doc_co').val();
  var doc_status = get_status;
  var doc_desc = $('#doc_desc').val();
  var doc_remarks = $('#doc_remarks').val();
  var doc_terms = $('#doc_terms').val();
  var save_type = $('#btn_save_pending').val();
  var dataString_top = 'doc_date='+doc_date+'&doc_ref='+doc_ref+'&doc_pr_no='+doc_pr_no+'&doc_co='+doc_co+'&doc_status='+doc_status+'&doc_terms='+doc_terms+'&doc_desc='+doc_desc+'&doc_remarks='+doc_remarks+'&save_type='+save_type;

    var dataString_pr = "";
    var iterator_pr = 0;
    var rowCount_pr = 0;  
    $('#pr_table td').each(function() 
    {
          var cellValue = $(this).find('button').val();
          var column = iterator_pr % 9;
          //var row = Math.round(iterator_pr / 3);
          var row = Math.floor(iterator_pr / 9);
 
          if(column == 8) {
            dataString_pr += "pr_id[" + row + "]=" + cellValue + "&";
          } 
          if(column == 6) {
            dataString_pr += "pr_qty[" + row + "]=" + uncomma($(this).html()) + "&";
          }    
          if(column == 5) {
            dataString_pr += "pr_price[" + row + "]=" + uncomma($(this).html()) + "&";
          }                     
          rowCount_pr = row + 1;
          iterator_pr++;
    });    
    dataString_pr += "rowCount_pr=" + rowCount_pr;

    dataString = dataString_pr+'&'+dataString_top;
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/insert_data.php",
      data: dataString,
      dataType: 'json',
      cache: false,
        success: function(s)
        {
          $('#confirm_modal').modal('hide');
          if(s!=1){
            clear_list();
            $('#lbl_grandtotal').text('0.00');
            $('#doc_ref').val('');
            $('#doc_pr_no').val('');
            $('#doc_co').val('');
            $('chk_doc_co').val('');
            $('#err_doc_date').text('');
            $('#err_doc_ref').text('');
            $('#err_doc_co').text(''); 
            $('#doc_desc').val('');
            $('#doc_remarks').val('');
            $('err_doc_terms').text('');
            $('#doc_terms').val('');             
            $("#pr_table").find("tr:gt(0)").remove();
            $('#fetch_po_id').val('');
            $('#btn_save_pending').val('create');
          $('#btn_save_pending').prop('disabled',false);
          $('#btn_save_shipped').prop('disabled',false);
          $("body").css("cursor", "default"); // default cursor            
          $('<form action="view.php" method="GET" target="_blank">' + 
            '<input type="hidden" name="doc_no" value="'+s+'">' +
            '</form>').submit();     
            toastr.success('SUCCESS:  Document Record Saved');                                           
          }
          else if(s==1){
            $(this).prop('disabled',false);
            toastr.danger('FAILED:  Document Failed Saved, Database No Connection');                                                  
          }
        }
    }); 
    //ajax end 
}



  
  if($('#fetch_po_id').val()!=''){
  $('#btn_save_pending').val($('#fetch_po_id').val());

  load_top_labels();
  doc_fetch($('#btn_save_pending').val());
  }

  function load_top_labels(){
    var po_id = $('#btn_save_pending').val();
    //ajax
    $.ajax ({
      type: "POST",
      url: "serverside/view_top_labels.php",
      dataType: 'json',      
      data: 'doc_no='+po_id,
      cache: false,
      success: function(s){
        for(var i = 0; i < s.length; i++) { 
          $('#doc_date').val(s[i][1]);
          $('#doc_co').val(s[i][5]);
          $('#chk_doc_co').val(s[i][10]);
          $('#doc_ref').val(s[i][0]);
          $('#doc_pr_no').val(s[i][9]);
          $('#doc_terms').val(s[i][7]);
          $('#doc_desc').val(s[i][4]);
          $('#doc_remarks').val(s[i][11]);
        }                     
      }  
    }); 
      //ajax end    
  }


  function doc_fetch(fetch_id){ // fetch for edit
  //$('#doc_fetch').prop('disabled',true);
  $("#pr_table").find("tr:gt(0)").remove(); 
  $('#lbl_grandtotal').text('0.00');
  $.ajax
  ({
    type: 'POST',
    url: '../gr/serverside/fetch_po.php',
    data: "po_id="+fetch_id,
    dataType: 'json',
    success: function(s)
    {
      for(var i = 0; i < s.length; i++){
        var prays =s[i][6].toString().replace('.', '');              
        var A = s[i][0]; // prodid
        var B = s[i][1]; // prod_name
        var C = s[i][2]; // prod_sku
        var D =  s[i][3]; // prod_supplier
        var E = s[i][4]; //prod_category
        var F = s[i][5]; //prod_var
        var G = parseFloat(s[i][6],10).toFixed(2); //prod_price
        var H = parseInt(s[i][7]); // qty

        $('#pr_table tr:last').after('<tr id='+A+prays+'> <td>'+B+'</td><td>'+C+'</td><td>'+D+'</td><td>'+E+'</td><td>'+F+'</td><td>'+comma(G)+'</td>      <td id='+"qty"+A+prays+ '>'+H+'</td><td id=sub'+A+prays+'>'+comma((H*G))+'</td> <td><button value='+A+' onclick="delRow(this.value,'+prays+')" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');            
        $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );
        $('#btn_proceed').prop('disabled',false);        
      }      
    }
  })   
}

//additional functions
  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 
  function confirmCancel(){

    var choice = confirm("Are you sure you want to Cancel Record?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }       

  function logout(){

    var choice = confirm("Are you sure you want to Log-out?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }  



// .addition functions  
