<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title id="title">PURCHASE ORDER INVOICE</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/print.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />          
    <style type="text/css">

    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      @media print{
          #delivery_term {
            page-break-before: avoid;   
            display: block;
            bottom: 0px;
            width: 100%;
           }
      }

      .invoice-info{
        font-size: 10px;
      }

      #amount_summary{
        font-size: 12px;
      }

      #delivery_term{
        margin-top: 15px;
        font-size: 10px;
      }
      #po_desc{
        font-size: 10px;
      }
      #top_header{
        text-align: center;
        font-size: 12px;
        padding: 0px 0px 0px 0px;
      }
      #pr_table{
        width: 100%;
        margin-bottom: 5px;
      }
      #pr_table table,th,tr,td{
        border: solid 1px grey;
      }
      #pr_table td,th{padding-left: 4px}
      #pr_table th{height: 25px}
      #pr_table td{font-size: 12px;}



/*
@media print
  {
  #delivery_term .page-break   
    { 
    display: block;
    color:red; 
    font-family:Arial; 
    font-size: 16px; 
    text-transform: uppercase; 
    }
  .page-break
    {
    page-break-before:always;
    } 
}*/
         
    </style>

  </head>
  <input id="po_id" value="<?php echo $_POST['doc_no'];?>" type="hidden" >      
  <body ><!--onload="window.print();"-->
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice">
        <!-- title row -->
        <div class="row" id="top_header">
          <div class="col-xs-12">           
              <p style="line-height:10px;"><b style="font-size:16px" id="top_header_co">CLOUDIPARK</b><br>
              <small id="top_header_shipping_division" > <br></small>
              <small >San Juan City, Philippines 1500</small><br>
              <small >Contact: +639162554441 | +639162554441</small><br>
              <p style="line-height:10px;"><b style="font-size:14px">PURCHASE ORDER</b><br>

          </div><!-- /.col -->
       

        </div>

        <!-- info row -->
        <div class="row invoice-info" style="border-top: solid 1px grey; padding-top:10px" >
					
          <div class="col-xs-6">
            <b>SUPPLIER: </b><text id="sup_name"></text><br>
            <b>Contact: </b><text id="sup_contact"></text>
            <br><text id="sup_desc"></text>
          </div><!--col-->                  
          <div class="col-xs-3">
            <b>P.O. No: </b><text id="po_no"></text><br>
            <b>Purchase Date: </b> <text id="po_date"></text>                          
          </div><!-- /.col -->
          <div class="col-xs-3">
            <b>Terms: </b><text id="po_terms"></text> <br>
            <b>PR No: </b><text id="pr_no"></text>                                
          </div><!-- /.col -->
       </div><!-- /.row -->
        <!-- Table row -->
        <div class="row" style="margin-top:5px;">
          <div class="col-xs-12">
            <table id="pr_table">
              <thead style="font-size:12px">
                <tr>
									<th>#</th>
                  <th style="width:200px">ITEM</th>
                  <th>CATEGORY</th>
									<th>UOM</th>
                  <th>QTY</th>          
                  <th>UNIT PRICE</th>
									<th>AMOUNT</th>
                </tr>
              </thead>
            </table>
            <text id="amount_summary" ></text>            
          </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row" style="margin-top:3px">
          <div class="col-xs-12 "> <!--remarks-->
            <div style="border: grey solid 1px; padding:2px">          
            <div style="border: grey solid 1px; padding:3px 10px">
              <p id="po_desc"></p>
            </div>
            </div>
          </div><!--col- remarks-->             
        </div><!--remarks row-->

      </section><!-- /.content -->
    </div><!-- ./wrapper -->

          <div class="" id="delivery_term" >
            <div class="col-xs-4" style="text-align:center"><!--delivery term-->
              <h6>Prepared By: <br><b><text id="purchaser"></text></b></h6><br>
            </div><!--col-3-->

            <div class="col-xs-4" style="text-align:center"><!--delivery term-->
              <h6 >Noted By: <br><b>Moses Lucas</b></h6><br>
            </div><!--col-3-->

            <div class="col-xs-4" style="text-align:center"><!--delivery term-->
              <h6 >Approved by: <br><b>Ramon Lucas</b></h6>            </div><!--col-3-->                                
          </div><!--/.row-->


    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <script src="../../resources/dist/towords/towords.js"></script>
    <script src="../../resources/plugins/towords/towords.js"></script><!--to words -->
    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>      
<script>

  function load_top_labels(get_po_id){
    $('#po_id_lbl').text(get_po_id); // put po_id to html lbl po_id
    //ajax
    $.ajax ({
      type: "POST",
      url: "serverside/view_top_labels.php",
      dataType: 'json',      
      data: 'doc_no='+get_po_id,
      cache: false,
      success: function(s){
        for(var i = 0; i < s.length; i++) { 
          $('#po_no').text(s[i][0]);  $('#title').text('PURCHASE ORDER '+s[i][0]);
          /*$('#po_group').text(s[i][3]); $('#po_company').text(s[i][5]);*/ $('#po_terms').text(s[i][7]);    
          $('#po_desc').text(s[i][4]+" "+s[i][11]);  $('#po_date').text(s[i][1]);  $('#purchaser').text(s[i][8].toUpperCase());
          $('#pr_no').text(s[i][9]); $('#top_header_co').text(s[i][5]);
          if(s[i][5]!='OCEAN COAST SHIPPING COMPANY'){            
            $('#top_header_shipping_division').css('display','none');

          }
        }       
        load_pr_table(get_po_id);
        //load_op_table(get_doc_no);  
        //load_ve_table(get_doc_no);                
      }  
    }); 
      //ajax end    
  }

  function load_pr_table(get_doc_no){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/view_pr_table.php",
      dataType: 'json',      
      data: 'doc_no='+get_doc_no,
      cache: false,
      success: function(s)
      {
        var grand_total = 0;
				var item='';


        for(var i = 0; i < s.length; i++) {       
          grand_total+=parseFloat(s[i][9],10);

				var subtot = parseFloat(s[i][8],10)*parseFloat(s[i][7],10);

          $('#pr_table tr:last').after('<tr><td>'+(i+1)+'</td><td>'+s[i][1]+'</td><td>'+s[i][3]+'</td> <td>'+s[i][4]+'</td> <td>'+comma(s[i][8])+'</td><td>'+comma(s[i][7])+'</td><td>'+comma(subtot.toFixed(2))+'</td></tr>');       
        }       
        $('#pr_table tr:last').after('<tr><th></th><th></th><th></th><th></th><th></th><th>TOTAL:</th><th>'+comma(grand_total.toFixed(2))+'</th></tr>');
				$('#sup_name').text(s[0][5].toUpperCase());	 $('#sup_desc').text(s[0][12]); 
				if(s[0][10]==null)//if telephone only
					$('#sup_contact').text(s[0][11]);
				else if(s[0][11]==null)//if phone only
					$('#sup_contact').text(s[0][10]);
				else//if both is present
					$('#sup_contact').text(s[0][10]+' '+s[0][11]);

        //for total amount in word
				if(isWhole(grand_total)==false)
          $('#amount_summary').text((toWords(grand_total.toFixed(2)   )+' PESOS ONLY').toUpperCase()+' (₱'+comma(grand_total.toFixed(2))+')');
				else if(isWhole(grand_total)==true)
          $('#amount_summary').text((toWords(grand_total.toFixed(0)   )+' PESOS ONLY').toUpperCase()+' (₱'+comma(grand_total.toFixed(2))+')');
       print_it_out();
      }  
    }); 
    //ajax end  
  } //.load pr_table



  load_top_labels($('#po_id').val());


  //additional functions
  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 
  function print_it_out(){
   // window.print();
    setTimeout( 'window.print()',300);                
  }

  function isWhole(x) {
      return x % 1 === 0;
  }

</script>


  </body>

</html>
