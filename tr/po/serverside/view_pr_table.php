<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT pv.pv_id as id, p.prod_name as name, p.prod_sku as sku, 
(SELECT c.cat_name FROM category c WHERE c.cat_id = p.prod_cat_id) as category, 
s.sup_name,s.sup_tel,s.sup_phone,s.sup_desc,
(SELECT co.co_name FROM company co WHERE co.co_id = p.prod_co_id) as company, 
p.prod_var as variant, pv.pv_price as price, pp.pp_qty as qty, (pv.pv_price*pp.pp_qty) as total
FROM product p, product_version pv, purchase_product pp, purchase_order po, supplier s
WHERE (po.po_id = ?) 
AND (s.sup_id = p.prod_sup_id)
AND (pp.pp_po_id = po.po_id) 
AND (pp.pp_prod_id = pv.pv_id) 
AND (pv.pv_prod_id = p.prod_id)
GROUP BY pp.pp_id  ";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['id'],$fetch['name'],$fetch['sku'],$fetch['category'],
      $fetch['variant'],$fetch['sup_name'],$fetch['company'],$fetch['price'],$fetch['qty'],$fetch['total'],
			$fetch['sup_tel'],$fetch['sup_phone'],$fetch['sup_desc']);          
  }         
$conn = null;             

echo json_encode($output);
?>    
