<?php
include('../../../mn/include/connect.php');
if(!isset($_SESSION)) { 
  session_start(); 
} 
$gd_selected = $_SESSION['gd_selected'];

   $checker = $_POST['checker'];
	 $year = date('Y');
	 $year = substr($year,2,3);
	 $month = date('m');
	$ref1 = $year.$month.'-';



          $sql = "SELECT COUNT(*) as count, co_id, 
			(SELECT COUNT(*)+1 FROM purchase_order po WHERE po.po_co_id = co.co_id AND po.po_status != 'deleted') as po_ref
			FROM company co, group_div gr
			WHERE co.co_name LIKE ? 
			AND (co.co_status='active') 
			AND (co.co_gr_id = gr.gr_id)
			AND (gr.gr_id = ?) ";

          $q = $conn->prepare($sql);
          $q -> execute(array($checker,$gd_selected));
          $browse = $q -> fetchAll();
          $rowcount = $q -> rowCount();
		  foreach($browse as $fetch)
		  {

            $po_ref_concat = $ref1.str_pad($fetch['po_ref'],4,"0",STR_PAD_LEFT);
            $output = array($fetch['count'],$fetch['co_id'],$po_ref_concat);         		 	
		  }                  

echo json_encode($output);
$conn = null;
?>
