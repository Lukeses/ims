<?php
    include('../../../mn/include/connect.php');


$prod_id = $_POST['prod_id'];


  $sql = "SELECT prod_id, prod_name, prod_sku,sup_name,cat_name, prod_var, prod_price
    FROM product p, category cat, supplier s
   WHERE (p.prod_cat_id = cat.cat_id)
   AND (p.prod_sup_id = s.sup_id)
   AND (p.prod_status = 'active')
   AND (p.prod_id = ?)
   GROUP BY prod_id
   ORDER BY prod_name ASC ";

  $q = $conn->prepare($sql);
  $q -> execute(array($prod_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['prod_id'],$fetch['prod_name'],$fetch['prod_sku'],$fetch['sup_name'],
      $fetch['cat_name'],$fetch['prod_var'],$fetch['prod_price']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    