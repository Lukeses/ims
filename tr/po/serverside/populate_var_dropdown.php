<?php

if(!isset($_SESSION)) { 
  session_start(); 
} 
$gd_selected = $_SESSION['gd_selected'];

include('../../../mn/include/connect.php');

   $prod_name = $_POST['prod_name'];

$sql = "SELECT prod_id,prod_var
FROM product p, company c, group_div gr
WHERE prod_name LIKE (SELECT prod_name FROM product WHERE prod_id = ?) 
AND (prod_sup_id = (SELECT prod_sup_id FROM product WHERE prod_id = ?))
AND (p.prod_co_id = c.co_id) 
AND (c.co_gr_id = gr.gr_id)
AND (gr.gr_id = ?)
AND (prod_status = 'active')
GROUP BY prod_id";

$q = $conn->prepare($sql);
$q -> execute(array($prod_name,$prod_name,$gd_selected));
$browse = $q -> fetchAll();
foreach($browse as $fetch){
$output[] = array ($fetch['prod_id'],$fetch['prod_var']);				 	
}                      

echo json_encode($output);
?>
