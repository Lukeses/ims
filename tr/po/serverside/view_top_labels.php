<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT po_id,po_ref,po_pr_no,po_date,po_status,gr_name,po_desc,po_remarks,co_name,co_id,po_terms,
SUM(pp.pp_qty*pv.pv_price) as tot,
(SELECT u_desc 
FROM user_account u, trail tr 
WHERE tr.trail_desc LIKE '%{$doc_no}%'
AND tr.user_name = u.u_name
AND (tr.module='Purchase Order') 
AND (tr.action = 'CREATE')) as purchaser
FROM purchase_order po, company co, group_div gr, purchase_product pp, product_version pv
WHERE (po.po_co_id = co.co_id)
AND (co.co_gr_id = gr.gr_id)
AND (po.po_id = pp.pp_po_id)
AND (pv.pv_id = pp.pp_prod_id)
AND (po.po_id = ?)
GROUP by po.po_id
ORDER BY po.po_status asc, po.po_date desc";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['po_ref'],$fetch['po_date'],$fetch['po_status'],$fetch['gr_name'],
      $fetch['po_desc'],$fetch['co_name'],$fetch['tot'],$fetch['po_terms'],$fetch['purchaser'],
      $fetch['po_pr_no'],$fetch['co_id'],$fetch['po_remarks']);          
  }         
$conn = null;             

echo json_encode($output);
?>    