
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../../mn/include/connect.php');  
    include('../../../mn/include/log.php');  


    if(!isset($_SESSION)) { 
      session_start(); 
    } 
    $gd_selected = $_SESSION['gd_selected'];

    //product array
    $rowCount_pr = $_POST['rowCount_pr'];
    $pr_id = $_POST['pr_id'];   
    $pr_qty = $_POST['pr_qty'];   
    $pr_price = $_POST['pr_price'];   

    $doc_date = $_POST['doc_date'];
    $doc_ref = $_POST['doc_ref'];
    $doc_pr_no = $_POST['doc_pr_no'];
    $doc_status = $_POST['doc_status'];
    $doc_co = $_POST['doc_co'];
  	$doc_terms = $_POST['doc_terms'];
    $doc_desc = trim($_POST['doc_desc']);
    $doc_remarks = trim($_POST['doc_remarks']);
    $save_type = $_POST['save_type'];

    //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('po'.$year);   

    if($save_type!='create')
      $id = $save_type;


    if($save_type=='create'){
        $sql = "INSERT INTO purchase_order values(?,?,?,?,?,?,?,?,?)"; //INSERT recordd
        $q = $conn -> prepare($sql);
        $q -> execute(array($id,$doc_co,$doc_ref,$doc_date,$doc_terms,$doc_desc,$doc_remarks,$doc_pr_no,$doc_status));
    }
    else if($save_type!='create'){
        $sql = "UPDATE purchase_order SET po_co_id=?, po_ref=?,po_date=?,po_terms=?,
        po_desc=?,po_remarks=?,po_pr_no=?,po_status=? WHERE po_id = ?"; //INSERT recordd
        $q = $conn -> prepare($sql);
        $q -> execute(array($doc_co,$doc_ref,$doc_date,$doc_terms,$doc_desc,$doc_remarks,$doc_pr_no,$doc_status,$id));

        $sql = "DELETE FROM purchase_product WHERE pp_po_id = ?"; //INSERT recordd
        $q = $conn -> prepare($sql);
        $q -> execute(array($id));        
    }


    if($save_type=='create'){
      $trail_id =uniqid('at'.$year);  
      $sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
      $q = $conn -> prepare($sql);    
      $q -> execute(array($trail_id,'Transaction','Purchase Order', 'CREATE', 'New: PO:'.$id. ', Po-Ref:'.$doc_ref.', Date:'.$doc_date.', Company:'.$doc_co.', Status:'.$doc_status, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));      
    }
    else if($save_type!='create'){
      $trail_id =uniqid('at'.$year);  
      $sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
      $q = $conn -> prepare($sql);    
      $q -> execute(array($trail_id,'Transaction','Purchase Order', 'EDIT', 'New: PO:'.$id. ', Po-Ref:'.$doc_ref.', Date:'.$doc_date.', Company:'.$doc_co.', Status:'.$doc_status, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));      
    }
            

        //check product version
    for($x=0; $x<$rowCount_pr; $x++){   

        $pv_id =uniqid('pv'.$year);  
        $pp_id =uniqid('pp'.$year);  

        $pv_count;
        $sql = "SELECT COUNT(*) as counter FROM product_version pv 
        WHERE (pv_prod_id = ? )
        AND (pv_price = ?) ";
        $q = $conn->prepare($sql);
        $q -> execute(array($pr_id[$x],$pr_price[$x]));
        $browse = $q -> fetchAll();
        foreach($browse as $fetch)
            $pv_count = $fetch['counter'];

        if($pv_count == 0){
            $sql = "INSERT INTO product_version VALUES(?,?,?,NOW(),?)";
            $q = $conn -> prepare($sql);
            $q -> execute(array($pv_id,$pr_id[$x],$pr_price[$x],'active'));    

            $trail_id =uniqid('at'.$year);  
            $sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
            $q = $conn -> prepare($sql);    
            $q -> execute(array($trail_id,'Transaction','PO-Product Version', 'CREATE', 'New: ID:'.$pv_id.', Product ID:'.$pr_id[$x].', Version Price:'.$pr_price[$x], date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

        }

        $sql = "INSERT INTO purchase_product VALUES(?,?,
            (SELECT pv_id FROM product_version WHERE pv_prod_id=? AND pv_price=?)
            ,?)";
        $q = $conn -> prepare($sql);
        $q -> execute(array($pp_id,$id,$pr_id[$x],$pr_price[$x],$pr_qty[$x]));        

    }        
$conn = null;



echo json_encode($id); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}


$conn = null;
?>
