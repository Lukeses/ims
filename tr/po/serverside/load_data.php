<?php
    include('../../../mn/include/connect.php');

    $gd_name = $_POST['gd_name'];

  $sql = "SELECT po_id,po_ref,po_pr_no,po_date,po_desc,po_remarks,po_status,co.co_name,gr.gr_name,
SUM(pp.pp_qty*pv.pv_price) as tot
FROM purchase_order po, company co, group_div gr,purchase_product pp, product_version pv
WHERE (po.po_co_id = co.co_id)
AND (co.co_gr_id = gr.gr_id)
AND (po.po_id = pp.pp_po_id)
AND (pv.pv_id = pp.pp_prod_id)
AND (gr.gr_id = ?)
GROUP BY po_id
ORDER BY po_date DESC";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_name));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['po_id'],$fetch['po_ref'],$fetch['po_pr_no'],
      $fetch['po_date'],ucwords($fetch['po_status']),
      $fetch['co_name'],$fetch['po_desc'],$fetch['tot'],$fetch['po_remarks']);          
  }         
$conn = null;             

echo json_encode($output);
?>    
