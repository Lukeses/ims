<?php
require('../../mn/include/log_tr.php');
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Add | Purchase Order</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- toastr-->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />    
    <!-- icheck-->
    <link rel="stylesheet" href="../../resources/plugins/iCheck/all.css">

    <!--<link href="typeaheadjs.css" rel="stylesheet" type="text/css" />    -->

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/typeahead.min.js" type="text/javascript"></script>    
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>
    <!--icheck js-->
    <script src="../../resources/plugins/iCheck/icheck.min.js"></script>

      <style type="text/css">


        .typeahead{
        width:100%;
        background-color: #fff;
        }        

        .tt-query {
          box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;


        }
        .tt-hint {
          color: white;

        }
        .tt-dropdown-menu {
          background-color: #FFFFFF;
          border: 1px solid rgba(0, 0, 0, 0.2);
          border-radius: 8px;
          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
          margin-top: 12px;
          padding: 8px 0;
          width: 250px;
        }
        .tt-suggestion {
          font-size: 16px;
          line-height: 22px;
          padding: 3px 18px;
        }
        .tt-suggestion.tt-is-under-cursor {
          background-color: #0097CF;
          color: #FFFFFF;
        }
        .tt-suggestion p {
          margin: 0;
        }        
      </style>



  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini ">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <!--<a href="javascript:select_groupdiv()" id="btn_select_groupdiv" role="button" class="btn btn-default pull-right" data-toggle='tooltip' title="Re-Select Group " data-placement='left'><i class="ion-android-checkbox-outline"></i> Re-Select Group</a>-->                              
          <h1>
            Purchase Order
            <small> Add New</small>
          </h1>
          <?php echo'<text class="lead" id="gd_selected_name">'.$session_group_name.'</text>';  ?>                    
          <?php echo'<input type="hidden" id="gd_selected" value="'.$session_group.'">';  ?>            
          <?php error_reporting(0); echo'<input type="hidden" id="fetch_po_id" value="'.$_POST["doc_no"].'">';  ?>

          <?php           
          $_SESSION["gd_selected"] = $session_group;
              ?>          
        </section>

        <!-- Main content -->
        <section class="content">

          <?php include('modals/confirm_modal.html') ?>         
          <?php include('../../mn/include/loading.html') ?>
          <?php include('../../mn/include/group_div.html') ?>


          <div class="row">
            <div class="col-sm-1 col-xs-2">                        
              <label style="margin-left:13px">
                   <a href="po.php" onclick="return confirmCancel()" role="button" data-toggle='tooltip' title="Cancel" data-placement='bottom' class="btn text-red"
                   style="box-shadow: 0px 3px 7px #888888; border-radius:100px; width:50px; height:50px; margin-bottom:5px; outline:none;
                   text-align: center; font-size:25px; background-color:white"> <i class="ion-android-close"></i> </a>                               
              </label>     
            </div> 

            <div class="col-sm-1 col-xs-2">
              <label>
                   <button id="btn_proceed" role="button" data-toggle='tooltip' title="Proceed" data-placement='bottom' class="btn text-green"
                   style="box-shadow: 0px 3px 7px #888888; border-radius:100px; width:50px; height:50px; margin-bottom:5px; outline:none;
                   text-align: center; font-size:25px; background-color:white; "> <i class="ion-android-done"></i> </button>                               
              </label>                             
            </div>   

            <div class="pull-right col-sm-3 col-xs-12">
              <div class="box box-solid">
                <div class="box-body">
                  <div class="description-block" >
                    <h5 class="description-header" id="lbl_grandtotal" style="font-size:20px">0.00</h5>
                    <span class="description-text">GRAND TOTAL</span>
                  </div><!-- /.description-block -->
                </div> <!-- /.box-body -->
              </div> <!-- /. box-solid -->
            </div><!--/.col--> 

          </div> <!-- ./row-->

            


          <div class="row" >                     <!-- TABLES -->
          <div class="col-sm-12">
              <div class="box box-solid">
                <div class="box-header">     

                  <div class="row" style="margin-top:15px">

                    <div class="col-md-2 col-sm-6 col-xs-12"  id="div_doc_date">
                      <label><font color="darkred">*</font>Date :</label> 
                      <strong class="text-red" id="err_doc_date"></strong>
                        <input id="doc_date" type="date" value="<?php echo"".date('Y-m-d')."" ?>"  class="form-control" >
                    </div> <!-- /.col-->    

                    <div class="col-md-2 col-sm-6 col-xs-12" id="div_doc_co">
                      <label ><font color="darkred">*</font>Company:</label>
                      <strong class="text-red" id="err_doc_co"></strong><br>                  
                       <input id="doc_co" type="text" name="doc_co" onblur="checking_doc_co(this.value)" onfocus="checking_doc_co(this.value)"  class="form-control"  placeholder=" Search Company " data-provdie="typeahead">
                      <input type='hidden' id="chk_doc_co">
                    </div> <!-- /.col-->  <!--Customer Field-->

                    <div class="col-md-2 col-sm-6 col-xs-6" id="div_doc_ref" style="margin-top:0">
                      <label ><font color="darkred">*</font>Ref No:</label>
                      <strong class="text-red" id="err_doc_ref"></strong>                      
                       <input id="doc_ref" type="text" name="tra" class="form-control" >
                    </div>                              

                    <div class="col-md-2 col-sm-6 col-xs-6" id="div_doc_ref" style="margin-top:0">
                      <label >PR No:</label>
                       <input id="doc_pr_no" type="text" name="tra" class="form-control" >
                    </div>    
                       
                    <div class="col-xs-2 " id="div_doc_terms" style="margin-top:0">
                      <label ><font color="darkred">*</font>Terms</label>
                      <strong class="text-red" id="err_doc_terms"></strong>                      
                      <input id="doc_terms" type="text" name="doc_terms" class="form-control" data-provdie="typeahead" placeholder="Input Terms">
                    </div><!--/.div-col-xs-2-->

                    <div class="col-md-2 col-sm-12 col-xs-12" id="div_doc_desc">
                      <label >Vessel:</label>
                      <strong class="text-red" id="err_doc_desc"></strong>                      
                      <input id="doc_desc" type="text" name="doc_terms" class="form-control" data-provdie="typeahead" placeholder="Input Vessel">                       
                    </div>       

                  </div> <!-- /. row -->

                  <hr>

                  <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12" id="pr_supDiv" style="margin-top:3px">
                      <label><font color="darkred"></font>FILTER BY SUPPLIER</label> <!-- FILTER SUPPLIER -->
                      <select type="text" class="form-control " id="pr_sup" onchange="populate_product_dropdown(this.value)">
                        <option value="none" >--SEARCH SUPPLIER--</option>                     
                      </select>
                    </div>                
                    <div class="col-md-3 col-sm-6 col-xs-12" id="div_doc_product" style="margin-top:3px">
                      <label><font color="darkred">*</font>Product :</label>
                      <strong id="prodnameErr" class="text-red"></strong>       <!-- err label -->    
											<select type="text" class="form-control " id="doc_product" onchange="populate_var_dropdown(this.value)">
												<option value="none" >--SEARCH PRODUCT--</option>                     
											</select>
                    </div> <!-- /.col-->   
                    <div class="col-md-3 col-sm-6 col-xs-12" id="div_doc_var" style="margin-top:3px">
                      <label><font color="darkred">*</font> Unit of Measurement (UOM) :</label>   
                      <strong id="variantErr" class="text-red"></strong>       <!-- err label -->                                                            
                      <div class="input-group" >
                       <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                        <select id="doc_var" class="form-control" placeholder="Select Variant" onchange="getPrice(this.value)">
                        </select>
                      </div>    
                    </div> <!-- /.col-->    



 

                  </div>

                  <div class="row"  style="margin-top:15px">


                    <div class="col-md-3 col-sm-6 col-xs-12" id="div_doc_price">
                      <label><font color="darkred">*</font>Price :</label>   
                      <div class="input-group" style="margin-top:3px" >
                       <span class="input-group-addon">₱</span>
                        <input id="doc_price" class="form-control" type="number">
                      </div>    
                    </div> <!-- /.col--> 

                    <div class="col-md-3 col-sm-6 col-xs-12" id="div_doc_qty">
                      <label><font color="darkred">*</font>Quantity :</label>   
                      <div class="input-group" style="margin-top:3px" >
                        <input id="doc_qty" class="form-control" type="number">
                      <span class="input-group-btn">
                      <button class="btn bg-teal" id="btn_add_list" title="Add Product to list below" data-toggle="tooltip" data-placement="bottom"> <i class="ion-android-arrow-down"></i>  ADD TO LIST    </button>
                      </span>                                  
                      </div>    
                    </div> <!-- /.col--> 

                    <div class="col-md-4 col-sm-4 col-xs-12" id="div_doc_remarks">
                      <label>Remarks</label>
                      <textarea id="doc_remarks" class="form-control" style="resize:none"></textarea>
                    </div>




                  </div> <!--/.row-->

                  <hr>
                    
            <div class="row">

     

              <div class="col-sm-12">
                <div class="box-body" >
                  <div class="box-header"style="text-align:center" >
                    <h3 class="box-title" ><i class="fa fa-tags"></i> Product List</h3>
                  </div><!-- /.box-header -->                      
                  <table id="pr_table" class="table table-bordered table-striped table-condensed table-hover">
                    <thead>
                        <th>Product</th>
                        <th>SKU</th> 
                        <th>Supplier</th>                         
                        <th>Category</th>                         
                        <th>UOM</th>
                        <th>Price</th>                                               
                        <th>Qty</th>                                                                                                                      
                        <th>Total</th>  
                        <th style="width:10px"></th>                                               
                    </thead>
                   </table>
                </div><!-- /.box-body -->                       
              </div><!--/.col-sm-7-->
            </div><!--/.row-->

              </div><!-- /.box -->  <!-- Input Fields -->
            </div><!-- /.col -->
            </div>  <!-- /.row -->

        </section><!-- /.content -->


      </div><!-- /.content-wrapper -->

      <?php include('../../mn/include/footer.php'); ?>
    </div><!-- ./wrapper -->
    <script src="controller2.js"></script>
    <script src="edit.js"></script>

    <!--MAIN CONTROLLER -->
    <script>
      $(document).ready(function () {



            $('[data-toggle="tooltip"]').tooltip();

            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-bottom-left",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "5000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "shoIMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }//.toastr config  

            //Initialize Select2 Elements
            $("#gd_name").select2();   
            $('#lo_co').select2();

            $('#doc_co').typeahead({
                name: 'doc_co',
                remote:'serverside/search_doc_co.php?key=%QUERY',
                limit : 10
            }); 

            $('#doc_op').typeahead({
                name: 'doc_op',
                remote:'serverside/search_doc_op.php?key=%QUERY',
                limit : 10
            }); 

            $('#doc_ve').typeahead({
                name: 'doc_ve',
                remote:'serverside/search_doc_ve.php?key=%QUERY',
                limit : 10
            }); 

            $('#doc_terms').typeahead({
                name: 'doc_terms',
                remote:'serverside/search_doc_terms.php?key=%QUERY',
                limit : 10
            }); 

            $('#doc_desc').typeahead({
                name: 'doc_desc',
                remote:'serverside/search_doc_desc.php?key=%QUERY',
                limit : 10
            }); 

            /*

            $('#doc_product').typeahead({
                name: 'doc_product',
                remote:'serverside/search_doc_product.php?key=%QUERY',
                limit : 10
            }); 

            $('#doc_product').typeahead({
              name:  'doc_product'
              limit: 10
              remote: {
                url: 'serverside/search_doc_product.php?key=%QUERY&supplier=%SUPPLIER'
                replace: function(url, query) {
                  var supplier = encodeURIComponent($('#pr_sup').val());

                  return url.replace('%QUERY', query).replace('%SUPPLIER', supplier);
                }
              }
            })
            */

            //Flat  scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
              checkboxClass: 'icheckbox_flat-green',
              radioClass: 'iradio_flat-green'
            });

            $('#pr_sup').select2(); 
            $('#doc_product').select2();                           
            populate_supplier_dropdown();
            populate_product_dropdown('none');
						//load_po_ref();
          });          



    </script>
  </body>
</html>
