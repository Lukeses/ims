<?php
    include('../../../mn/include/connect.php');

    $gd_name = $_POST['gd_name'];

  $sql = "SELECT doc_id,doc_ref,doc_remarks,doc_date,doc_status,gr_name,
(SELECT loc_name FROM location l WHERE l.loc_id = d.doc_source) as source, (SELECT loc_name FROM location l WHERE l.loc_id = d.doc_desti) as desti,
SUM(dp.dp_qty*pv.pv_price) as tot
FROM document d,group_div gr, document_product dp, product_version pv
WHERE (d.doc_type = 'STN-BB')
AND (d.doc_gr_id = gr.gr_id)
AND (dp.dp_doc_id = d.doc_id)
AND (dp.dp_prod_id = pv.pv_id)
AND (gr.gr_id= ?)
GROUP BY d.doc_id
ORDER BY doc_date desc,doc_status asc";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_name));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['doc_id'],$fetch['doc_ref'],$fetch['doc_remarks'],
      $fetch['doc_date'],ucwords($fetch['doc_status']),$fetch['gr_name'],
      $fetch['source'],$fetch['desti'],$fetch['tot']);          
  }         
$conn = null;             

echo json_encode($output);
?>    