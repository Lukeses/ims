
    <?php           // ESTABLISH CONNECTION TO MYSQL
try{
    include('../../../mn/include/connect.php');                             //FETCH ALL VARIABLES
    include('../../../mn/include/log.php');   

    $doc_id = $_POST['doc_id'];
    $pv_id = $_POST['pv_id'];
    $pv_dp_qty = $_POST['pv_dp_qty'];
    $rowCount = $_POST['rowCount'];


    for($x=0; $x<$rowCount; $x++){   
		$sql = "UPDATE document_product SET dp_qty = ? WHERE dp_prod_id = ? AND dp_doc_id = ?  ";
        $q = $conn -> prepare($sql);
        $q -> execute(array($pv_dp_qty[$x],$pv_id[$x],$doc_id));        
    }  

		$sql = "UPDATE document SET doc_status = ? WHERE doc_id = ?  ";
        $q = $conn -> prepare($sql);
        $q -> execute(array('shipped',$doc_id));

        $year = date('Y');
        $year = substr($year,2,3);
        $trail_id =uniqid('at'.$year);  
        $sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
        $q = $conn -> prepare($sql);    
        $q -> execute(array($trail_id,'Transaction','STN-Branch', 'EDIT', 'Updated to Shipped: ID:'.$doc_id, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

	$conn = null;
	echo json_encode(0); 	
	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}



?>  
