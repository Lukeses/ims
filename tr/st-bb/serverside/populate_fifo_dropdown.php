<?php

if(!isset($_SESSION)) { 
  session_start(); 
} 
$gd_selected = $_SESSION['gd_selected'];

include('../../../mn/include/connect.php');

   $prod_id = $_POST['prod_id'];
   $loc_origin = $_POST['loc_origin'];


$sql = "SELECT pv.pv_id as id, pv.pv_price as price, p.prod_name as name, 
SUM(case when d.doc_desti = ? then dp.dp_qty else 0 end) as inbound,
SUM(case when d.doc_source = ? then dp.dp_qty else 0 end) as outbound
FROM product_version pv, document d, document_product dp ,product p
WHERE (pv.pv_id = dp.dp_prod_id) 
AND (d.doc_id = dp.dp_doc_id) 
AND (p.prod_id = pv.pv_prod_id)
AND (p.prod_id = ?)
AND (d.doc_status = 'shipped')
GROUP BY pv.pv_id
ORDER BY d.doc_date asc";

$q = $conn->prepare($sql);
$q -> execute(array($loc_origin,$loc_origin,$prod_id));
$browse = $q -> fetchAll();
foreach($browse as $fetch){
$output[] = array ($fetch['id'],$fetch['name'],$fetch['price'],($fetch['inbound']-$fetch['outbound']));				 	
}                      

echo json_encode($output);
?>