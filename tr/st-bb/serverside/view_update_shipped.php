<?php
    include('../../../mn/include/connect.php');

  $doc_id = $_POST['doc_id'];


  $sql = "SELECT pv.pv_id, pr.prod_name, pr.prod_sku,pr.prod_var,pv.pv_price,dp.dp_qty,
  (SELECT cat_name FROM category c WHERE c.cat_id = pr.prod_cat_id) as category,
  (SELECT sup_name FROM supplier s WHERE s.sup_id = pr.prod_sup_id) as supplier
  FROM product_version pv, product pr, document_product dp
  WHERE (dp.dp_doc_id = ?) 
  AND (dp.dp_prod_id = pv.pv_id) 
  AND (pv.pv_prod_id = pr.prod_id) ";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['pv_id'],$fetch['prod_name'],$fetch['prod_sku'],
    	$fetch['prod_var'],$fetch['pv_price'],$fetch['dp_qty'],$fetch['category'],
    	$fetch['supplier']);          
  }         
$conn = null;             

echo json_encode($output);
?>    