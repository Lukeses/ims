<?php
    include('../../../mn/include/connect.php');


$pv_id = $_POST['pv_id'];


  $sql = "SELECT pv.pv_id as id, p.prod_name as name, prod_sku,sup_name,cat_name, p.prod_var as var, pv.pv_price as price
    FROM product p, category cat, supplier s, product_version pv
   WHERE (p.prod_cat_id = cat.cat_id)
   AND (p.prod_sup_id = s.sup_id)
   AND (p.prod_status = 'active')
   AND (pv.pv_id = ?)
   AND (pv.pv_prod_id = p.prod_id)
   GROUP BY prod_id
   ORDER BY prod_name ASC";

  $q = $conn->prepare($sql);
  $q -> execute(array($pv_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['id'],$fetch['name'],$fetch['prod_sku'],$fetch['sup_name'],
      $fetch['cat_name'],$fetch['var'],$fetch['price']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    