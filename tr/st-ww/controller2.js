function checking_doc_desti(get_val)
{
  
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_loc.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_desti').val(s[1]);
        $('#doc_desti').val(get_val);
      }
      else{
        $('#chk_doc_desti').val('no-match');       
      }

    }
  }) 
} // checking_doc_desti

function checking_doc_source(get_val)
{
  
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_loc.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#doc_product').prop('disabled',false);                
        $('#doc_var').prop('disabled',false);
        $('#doc_fifo').prop('disabled',false);                
        $('#doc_qty').prop('disabled',false);         
        $('#chk_doc_source').val(s[1]);
        $('#doc_source').val(get_val);
      }
      else{
        $('#doc_product').prop('disabled',true);                
        $('#doc_var').prop('disabled',true);
        $('#doc_fifo').prop('disabled',true);                
        $('#doc_qty').prop('disabled',true);        
        $('#chk_doc_source').val('no-match');        

      }
    }
  }) 
} // checking_doc_desti


$('#doc_source').change(function(){
  clear_list();
  $("#lbl_grandtotal").text("0.00");
  $("#pr_table").find("tr:gt(0)").remove();  

  if($(this).val()!=null || $(this).val()!='none'){
    $('#doc_product').prop('disabled',false);                
    $('#doc_var').prop('disabled',false);
    $('#doc_fifo').prop('disabled',false);                
    $('#doc_qty').prop('disabled',false);         
  }
  if($(this).val()==null || $(this).val()=='none'){
    $('#doc_product').prop('disabled',true);                
    $('#doc_var').prop('disabled',true);
    $('#doc_fifo').prop('disabled',true);                
    $('#doc_qty').prop('disabled',true);        
  }
})

function populate_warehouse_dropdown(){ 
  //ajax now
  $.ajax ({
    type: "POST",
    url: "../gr/serverside/populate_warehouse_dropdown.php",
    dataType: 'json',      
    cache: false,
    success: function(s)
    {
      $('#doc_desti').empty();
      $('#doc_source').empty();      
      $('#doc_source').html('<option selected="selected" value="none">--SEARCH WAREHOUSE--</option>');      
      $('#doc_desti').html('<option selected="selected" value="none">--SEARCH WAREHOUSE--</option>');
      $('#doc_source').append('<option value="origin">Kaingin Warehouse</option>');
      $('#doc_desti').append('<option value="origin">Kaingin Warehouse</option>');      
      for(var i = 0; i < s.length; i++) { 
        $('#doc_source').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');        
        $('#doc_desti').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
      }       
    }  
  }); 
  //ajax end  
  $('#doc_desti').select2().select2('val','none');     
  $('#doc_source').select2().select2('val','none');                                         
} //

function checking_doc_op(get_val)
{
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_op.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_op').val(s[1]);
        $('#doc_op').val(get_val);
      }
      else
        $('#chk_doc_op').val('no-match');        
    }
  }) 
} // checking_doc_desti

function checking_doc_ve(get_val)
{
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/checking_doc_ve.php',
    data: "checker="+ get_val,
    dataType: 'json',
    success: function(s)
    {
      if(s[0]==1){
        $('#chk_doc_ve').val(s[1]);
        $('#doc_ve').val(get_val);
      }
      else
        $('#chk_doc_ve').val('no-match');        
    }
  }) 
} // checking_doc_desti

function populate_var_dropdown(get_prod_name){
  $.ajax
  ({

    type: 'POST',
    url: 'serverside/populate_var_dropdown.php',
    data: 'prod_name='+get_prod_name,
    dataType: 'json',
    success:function(s)
    {
      $('#doc_var').empty();
      $('#doc_var').append('<option value="none">--Select UOM--</option>');
      for(var i = 0; i < s.length; i++) { 
        $('#doc_var').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
      }    
    } //.function(s)
  })// .ajax*/
}     // .populate_var_dropdown

function populate_fifo_dropdown(get_prod_id){
  $('#btn_add_list').val('');
  var loc_origin = $('#doc_source').val();
  $.ajax
  ({
    type: 'POST',
    url: 'serverside/populate_fifo_dropdown.php',
    data: 'prod_id='+get_prod_id+'&loc_origin='+loc_origin,
    dataType: 'json',
    success:function(s)
    {
      $('#doc_fifo').empty();
      $('#doc_fifo').append('<option value="none">--Select FIFO--</option>');
      for(var i = 0; i < s.length; i++) { 
        if(s[i][3]!=0){
        $('#doc_fifo').append('<option id="fifo'+s[i][0]+'" value="'+s[i][0]+'">   ₱'+s[i][2]+'  ---  ('+s[i][3]+')Qty'+'</option>');
        }
        
      }    
    } //.function(s)
  })// .ajax*/
}     // .populate_var_dropdown

function priceTObutton(){
  var fifo = $('#doc_fifo option:selected').text();
  var regExp = /\(([^)]+)\)/;
  var matches = regExp.exec(fifo);
  $('#btn_add_list').val(matches[1]);

}


function add_row() {   
    var pv_id = $('#doc_fifo').val();

        $.ajax
        ({
          type: 'POST',
          url: 'serverside/add_row.php',
          data: 'pv_id='+pv_id,
          dataType: 'json',
          success:function(s)
          {                        
              for(var i=0; i<s.length;i++)
              {            
                var A = s[i][0]; // prodid
                var B = s[i][1]; // prod_name
                var C = s[i][2]; // prod_sku
                var D =  s[i][3]; // prod_supplier
                var E = s[i][4]; //prod_category
                var F = s[i][5]; //prod_var
                var G = parseFloat(s[i][6],10);//fifo
                var H = parseInt($('#doc_qty').val()); // qty
              }//.for loop [i]   


          if($('#tr'+A).length){
            var current = uncomma($('#qty'+A).text());
            current = parseInt(current)+parseInt(H);
            $('#qty'+A).html(comma(current));    

            currentTOT = parseFloat(current,10)*parseFloat(G,10);
            $('#sub'+A).html(comma(currentTOT.toFixed(2)));
            clear_list(); 
            $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );          
            $('#btn_proceed').prop('disabled',false);
               
          }                
          
          else{
            $('#pr_table tr:last').after('<tr id=tr'+A+'> <td>'+B+'</td><td>'+C+'</td><td>'+D+'</td><td>'+E+'</td><td>'+F+'</td><td>'+comma(G.toFixed(2))+'</td>      <td id='+"qty"+A+'>'+H+'</td><td id=sub'+A+'>'+comma((H*G))+'</td> <td><button value='+A+' onclick="delRow(this.value)" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');                        
            clear_list();
            $('#lbl_grandtotal').text( comma((parseFloat(uncomma($('#lbl_grandtotal').text()),10)+parseFloat((G*H),10)).toFixed(2)) );
          } 

          } //.function(s)     

        })// end.ajax 
}

function add_row_ve() {   
    var ve_id = $('#chk_doc_ve').val();
    var ve_no = $('#doc_ve').val();

      if($('#'+ve_id).length){
        $('#err_doc_ve').text('Already at List');
      }
      else{
        $('#ve_table tr:last').after('<tr id='+ve_id+'> <td>'+ve_no+'</td><td><button value='+ve_id+' onclick="delRow_ve(this.value)" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');                    
        $('#doc_ve').val('');
        $('#chk_doc_ve').val('');
        $('#err_doc_ve').text('');        
      }       
}

function add_row_op() {   
    var emp_name = $('#doc_op').val();
    var emp_id = $('#chk_doc_op').val();
    var role = $('#doc_role').val();

      if($('#'+emp_id).length){
        $('#err_doc_role').text("Already at List");
      }
      else{
        $('#op_table tr:last').after('<tr id='+emp_id+'> <td>'+emp_name+'</td><td>'+role.toUpperCase()+'</td><td><button value='+emp_id+' onclick="delRow_op(this.value)" class="btn btn-xs text-red" title="Remove"><i class="fa fa-close"></i></td></tr>');                    
        $('#doc_op').val('');
        $('#chk_doc_op').val('no-match');
        $('#doc_role').val('none');
        $('#err_doc_role').text("");        
      }       
}

function delRow(get){
  var lastdata = uncomma($('#sub'+get).text()); 
  var tot_val = parseFloat(uncomma($('#lbl_grandtotal').text()),10) - parseFloat(lastdata,10);
  $('#lbl_grandtotal').html(comma(tot_val.toFixed(2)));
  $('#pr_table #tr'+get).remove();
}

function delRow_ve(get){
  $('#ve_table #'+get).remove();
}

function delRow_op(get){
  $('#op_table #'+get).remove();
}

function clear_list(){
  $('#doc_product').val('');
  $('#doc_var').val('none');
  $('#doc_var').empty();
  $('#doc_fifo').empty();
  $('#doc_fifo').append('<option value="none"></option>');
  $('#doc_qty').val('');
}

$('#btn_add_list').click(function(){
  if(validate_add_list()==true){
    $('#btn_add_list').removeClass('bg-teal')
    $('#btn_add_list').addClass('bg-red');
  }
  else if(validate_add_list()==false){
    $('#btn_add_list').removeClass('bg-red')
    $('#btn_add_list').addClass('bg-teal');
    add_row();
  }
}) // /.btn_add_list

$('#btn_add_ve').click(function(){
  if(validate_add_ve()==true){
    $('#btn_add_ve').removeClass('bg-teal')
    $('#btn_add_ve').addClass('bg-red');
  }
  else if(validate_add_ve()==false){
    $('#btn_add_ve').removeClass('bg-red')
    $('#btn_add_ve').addClass('bg-teal');
    add_row_ve();
  }
}) // /.btn_add_list

$('#btn_add_op').click(function(){
  if(validate_add_op()==true){
    $('#btn_add_op').removeClass('bg-teal')
    $('#btn_add_op').addClass('bg-red');
  }
  else if(validate_add_op()==false){
    $('#btn_add_op').removeClass('bg-red')
    $('#btn_add_op').addClass('bg-teal');
    add_row_op();
  }
}) // /.btn_add_list


function validate_add_op(){
  var err = false;

  if($('#chk_doc_op').val()=='no-match' || $('#doc_op').val()=='' || $('#doc_op').val()==null){
    err = true;
    $('#err_doc_op').text('REQUIRED');
  }
  else
    $('#err_doc_op').text('');   

  if( $('#doc_role').val() == 'none' ){
    err = true;
    $('#err_doc_role').text('REQUIRED');
  }
  else
    $('#err_doc_role').text('');     

  return err; 
}


function validate_add_ve(){
  var err = false;

  if($('#chk_doc_ve').val()=='no-match' || $('#doc_ve').val()=='' || $('#doc_ve').val()==null){
    err = true;
    $('#err_doc_ve').text('REQUIRED');
  }
  else
    $('#err_doc_ve').text('');   

  return err; 
}

function validate_add_list(){
  var err = false;

  if($('#doc_var').val()=='none' || $('#doc_var').val()==null){
    err = true;
    $('#div_doc_var').addClass('has-error');
  }
  else
    $('#div_doc_var').removeClass('has-error');   

  if($('#doc_fifo').val()=='none'){
    err = true;
    $('#div_doc_fifo').addClass('has-error');
  }
  else
    $('#div_doc_fifo').removeClass('has-error');   

  if($('#doc_qty').val()=='' || $('#doc_qty').val()<=0 ){
    err = true;
    $('#div_doc_qty').addClass('has-error');
  }
  else if(  parseInt($('#doc_qty').val()) > parseInt($('#btn_add_list').val()) ){
    err = true;
    $('#div_doc_qty').addClass('has-error');
    $('#div_doc_fifo').addClass('has-success');
    $('#err_doc_qty').text('Exceeded Stock Qty');      
  }    
  else if( $('#tr'+$('#doc_fifo').val()).length  ){
    $('#err_doc_qty').text('');           
    var table_qty = uncomma($('#qty'+$('#doc_fifo').val()).text());
    if((parseInt(table_qty)+parseInt($('#doc_qty').val())) >$('#btn_add_list').val()  ){
      err = true;
      $('#div_doc_qty').addClass('has-error');
      $('#div_doc_fifo').addClass('has-success');       
      $('#err_doc_qty').text('Exceeded at List'); 
    }
  }
  else{
    $('#div_doc_qty').removeClass('has-error');   
    $('#div_doc_fifo').removeClass('has-success');  
    $('#err_doc_qty').text('');       
  }


  return err; 
}

$('#btn_proceed').click(function(){  
  if(validate_proceed()==true){}
  else{
    $('#total_at_modal').text('PHP '+ $('#lbl_grandtotal').text()  );
    $('#confirm_modal').modal('show');
  }  
})

function validate_proceed(){
  var err = false;
  var pr = $('#pr_table tr').length-1;
  var ve = $('#ve_table tr').length-1;
  var op = $('#op_table tr').length-1;

  if(pr==0){
    err=true;    
    $('#btn_add_list').removeClass('bg-teal');    
    $('#btn_add_list').addClass('bg-red');
  }else{
    $('#btn_add_list').removeClass('bg-red');    
    $('#btn_add_list').addClass('bg-teal');    
  }

  if(ve==0){
    $('#btn_add_ve').removeClass('bg-teal');    
    $('#btn_add_ve').addClass('bg-red'); 
    err=true;
  }
  else{
    $('#btn_add_ve').removeClass('bg-red');    
    $('#btn_add_ve').addClass('bg-teal'); 
  }

  if(op==0){
    $('#btn_add_op').removeClass('bg-teal');    
    $('#btn_add_op').addClass('bg-red'); 
    err=true;
  }
  else{
    $('#btn_add_op').removeClass('bg-red');    
    $('#btn_add_op').addClass('bg-teal');
  }


  if($('#doc_date').val()==''){
    err = true;
    $('#err_doc_date').text('REQUIRED');
  }
  else
    $('#err_doc_date').text('');   

  if($('#doc_ref').val()==''){
    err = true;
    $('#err_doc_ref').text('REQUIRED');
  }
  else
    $('#err_doc_ref').text('');   

  if($('#doc_source').val()=='none' || $('#doc_source').val()=='' || $('#doc_source').val()==null){
    err = true;
    $('#err_doc_source').text('REQUIRED');
  }
  else
    $('#err_doc_source').text(''); 

  if($('#doc_desti').val()=='none' || $('#doc_desti').val()=='' || $('#doc_desti').val()==null){
    err = true;
    $('#err_doc_desti').text('REQUIRED');
  }
  else
    $('#err_doc_desti').text(''); 



  return err; 
}

function save_record(get_status){
  $('#btn_save_pending').prop('disabled',true);
  $('#btn_save_shipped').prop('disabled',true);

  var doc_date = $('#doc_date').val();
  var doc_ref = $('#doc_ref').val();
  var doc_remarks = $('#doc_remarks').val();
  var doc_status = get_status;  
  var doc_source = $('#doc_source').val();  
  var doc_desti = $('#doc_desti').val();
  var dataString_top = 'doc_date='+doc_date+'&doc_ref='+doc_ref+'&doc_remarks='+doc_remarks+'&doc_source='+doc_source+'&doc_desti='+doc_desti+'&doc_status='+doc_status;
      //vehicle fetch table
    var dataString_ve = "";
    var iterator_ve = 0;
    var rowCount_ve = 0;  
    $('#ve_table td').each(function() 
    {
          var cellValue = $(this).find('button').val();
          var column = iterator_ve % 2;
          //var row = Math.round(iterator_ve / 3);
          var row = Math.floor(iterator_ve / 2);
 
          if(column == 1) {
            dataString_ve += "ve_id[" + row + "]=" + cellValue + "&";
          } 
          rowCount_ve = row + 1;
          iterator_ve++;
    });    
    dataString_ve += "rowCount_ve=" + rowCount_ve;
      //operation fetch table
    var dataString_op = "";
    var iterator_op = 0;
    var rowCount_op = 0;  
    $('#op_table td').each(function() 
    {
          var cellValue = $(this).find('button').val();
          var column = iterator_op % 3;
          //var row = Math.round(iterator_op / 3);
          var row = Math.floor(iterator_op / 3);
 
          if(column == 2) {
            dataString_op += "emp_id[" + row + "]=" + cellValue + "&";
          } 
          if(column == 1) {
            dataString_op += "emp_role[" + row + "]=" + $(this).html() + "&";
          }          
          rowCount_op = row + 1;
          iterator_op++;
    });    
    dataString_op += "rowCount_op=" + rowCount_op;
    //product fetch table
    var dataString_pr = "";
    var iterator_pr = 0;
    var rowCount_pr = 0;  
    $('#pr_table td').each(function() 
    {
          var cellValue = $(this).find('button').val();
          var column = iterator_pr % 9;
          //var row = Math.round(iterator_pr / 3);
          var row = Math.floor(iterator_pr / 9);
 
          if(column == 8) {
            dataString_pr += "pr_id[" + row + "]=" + cellValue + "&";
          } 
          if(column == 6) {
            dataString_pr += "pr_qty[" + row + "]=" + uncomma($(this).html()) + "&";
          }    
          if(column == 5) {
            dataString_pr += "pr_price[" + row + "]=" + uncomma($(this).html()) + "&";
          }                     
          rowCount_pr = row + 1;
          iterator_pr++;
    });    
    dataString_pr += "rowCount_pr=" + rowCount_pr;

    dataString = dataString_ve+'&'+dataString_op+'&'+dataString_pr+'&'+dataString_top;
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/insert_data.php",
      data: dataString,
      cache: false,
        success: function(s)
        {
          $('#confirm_modal').modal('hide');
          if(s==0){
            toastr.success('SUCCESS:  Document Record Saved');                          
            clear_list();
            $('#doc_ve').val('');
            $('#chk_doc_ve').val('');                       
            $('#doc_op').val('');
            $('#chk_doc_op').val('');
            $('#doc_role').val('none');
            $('#lbl_grandtotal').text('0.00');
            $('#doc_ref').val('');
            populate_warehouse_dropdown(); $('#doc_source').prop('disabled',false);
            populate_branch_dropdown();
            $('#doc_remarks').val('');                              
            $('#err_doc_qty').text('');
            $('#err_doc_ve').text('');
            $('#err_doc_role').text('');          
            $('#err_doc_date').text('');
            $('#err_doc_ref').text('');
            $('#err_doc_source').text(''); 
            $('#err_doc_desti').text('');                                           
            $("#pr_table").find("tr:gt(0)").remove();
            $("#ve_table").find("tr:gt(0)").remove();
            $("#op_table").find("tr:gt(0)").remove();
          $('#btn_save_pending').prop('disabled',false);
          $('#btn_save_shipped').prop('disabled',false);
          }
          else if(s==1){
            $(this).prop('disabled',false);            
            toastr.danger('FAILED:  Document Failed Saved, Database No Connection');                                      

          }
        }
    }); 
    //ajax end */
}



//additional functions
  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 
  function confirmCancel(){

    var choice = confirm("Are you sure you want to Cancel Record?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }       

  function logout(){

    var choice = confirm("Are you sure you want to Log-out?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }  

  function select_groupdiv(){
    populate_gr_dropdown();    
    $('#gd_name').select2().select2('val','none');              
    $('#gd_nameDiv').removeClass('has-error');
    $('#gd_modal').modal('show');    
  }    

  function populate_gr_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_gd_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#gd_name').empty();
        $('#gd_name').append('<option value="none">--Select Group--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
  } // 

  $('#btn_gd_submit').click(function(){
    if($('#gd_name').val()=='none'){
      $('#gd_nameDiv').addClass('has-error');
    }
    else{
      $('#btn_select_groupdiv').removeClass('bg-red');
      $('#btn_select_groupdiv').addClass('bg-green');         
      $('#gd_nameDiv').removeClass('has-error');
      $('#gd_selected').val($('#gd_name').val());
      $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );            
      $('#gd_submit_name').val( $( "#gd_name option:selected" ).text()  );   
      $('#gd_modal').modal('hide');      
        $.ajax({
          type: 'GET',
          url: 'serverside/session_gd_selected.php',
          dataType: 'json',
          data: 'gd='+$('#gd_name').val(),
          success: function(s) {
        }
      });                   
    }
  })    
// .addition functions  
