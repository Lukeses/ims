<?php
require('../../mn/include/log_tr.php');
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Add | Stock Transfer | Warehouse</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- toastr-->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />    

    <!--<link href="typeaheadjs.css" rel="stylesheet" type="text/css" />    -->

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/typeahead.min.js" type="text/javascript"></script>    
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>

      <style type="text/css">

        .typeahead {
          background-color: #FFFFFF;
          font-size: 14px;
          height: 35px;
          width:200px;
          outline:none;
          border: lightgrey solid 1px;  
        }

        .typeahead:focus {
          border: 2px solid #0097CF;
        }
        .tt-query {
          box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;


        }
        .tt-hint {
          color: white;

        }
        .tt-dropdown-menu {
          background-color: #FFFFFF;
          border: 1px solid rgba(0, 0, 0, 0.2);
          border-radius: 8px;
          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
          margin-top: 12px;
          padding: 8px 0;
          width: 250px;
        }
        .tt-suggestion {
          font-size: 16px;
          line-height: 22px;
          padding: 3px 18px;
        }
        .tt-suggestion.tt-is-under-cursor {
          background-color: #0097CF;
          color: #FFFFFF;
        }
        .tt-suggestion p {
          margin: 0;
        }        
      </style>



  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini ">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <!--<a href="javascript:select_groupdiv()" id="btn_select_groupdiv" role="button" class="btn btn-default pull-right" data-toggle='tooltip' title="Re-Select Group " data-placement='left'><i class="ion-android-checkbox-outline"></i> Re-Select Group</a>-->                              
          <h1>
            Warehouse-Warehouse
            <small> Add Stock Transfer</small>
          </h1>
          <?php echo'<text class="lead" id="gd_selected_name">'.$session_group_name.'</text>';  ?>                    
          <?php echo'<input type="hidden" id="gd_selected" value="'.$session_group.'">';  ?>            
          <?php  
          $_SESSION["gd_selected"] = $session_group;
              ?>          
        </section>

        <!-- Main content -->
        <section class="content">

          <?php include('modals/confirm_modal.html') ?>         
          <?php include('../../mn/include/loading.html') ?>
          <?php include('../../mn/include/group_div.html') ?>


          <div class="row">

            <div class="col-sm-1 col-xs-2">                        
              <label style="margin-left:13px">
                   <a href="st.php" onclick="return confirmCancel()" role="button" data-toggle='tooltip' title="Cancel" data-placement='bottom' class="btn text-red"
                   style="box-shadow: 0px 3px 7px #888888; border-radius:100px; width:50px; height:50px; margin-bottom:5px; outline:none;
                   text-align: center; font-size:25px; background-color:white"> <i class="ion-android-close"></i> </a>                               
              </label>     
            </div> 

            <div class="col-sm-1 col-xs-2">
              <label>
                   <button id="btn_proceed" role="button" data-toggle='tooltip' title="Proceed" data-placement='bottom' class="btn text-green"
                   style="box-shadow: 0px 3px 7px #888888; border-radius:100px; width:50px; height:50px; margin-bottom:5px; outline:none;
                   text-align: center; font-size:25px; background-color:white; "> <i class="ion-android-done"></i> </button>                               
              </label>                             
            </div>   

            <div class="pull-right col-sm-3 col-xs-12">
              <div class="box box-solid">
                <div class="box-body">
                  <div class="description-block" >
                    <h5 class="description-header" id="lbl_grandtotal" style="font-size:20px">0.00</h5>
                    <span class="description-text">GRAND TOTAL</span>
                  </div><!-- /.description-block -->
                </div> <!-- /.box-body -->
              </div> <!-- /. box-solid -->
            </div><!--/.col--> 

          </div> <!-- ./row-->

            


          <div class="row" >                     <!-- TABLES -->
          <div class="col-sm-12">
              <div class="box box-solid">
                <div class="box-header">     

                  <div class="row" style="margin-top:15px">
                    <div class="col-xs-0"></div>

                    <div class="col-xs-3" id="div_doc_date">
                      <label><font color="darkred">*</font>Date :</label> 
                      <strong class="text-red" id="err_doc_date"></strong>
                      <div class="input-group" style="margin-top:3px">
                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input id="doc_date" type="date" value="<?php echo"".date('Y-m-d')."" ?>"  class="form-control" >
                      </div>    
                    </div> <!-- /.col-->    
                    <div class="col-xs-3" id="div_doc_ref">
                      <label ><font color="darkred">*</font>Ref No:</label>
                      <strong class="text-red" id="err_doc_ref"></strong>                      
                      <div class="input-group" style="margin-top:3px">
                       <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                       <input id="doc_ref" type="text" name="tra" class="form-control" >
                      </div>            
                    </div>                               

                    <div class="col-xs-3" id="div_doc_source" > 
                      <label ><font color="darkred">*</font>Source:</label>
                      <strong class="text-red" id="err_doc_source"></strong>                      
                        <select id="doc_source" class="form-control">
                        </select>
                    </div> <!-- /.col-->   

                    <div class="col-xs-3" id="div_doc_desti" > 
                      <label ><font color="darkred">*</font>Destination:</label>
                      <strong class="text-red" id="err_doc_desti"></strong>                      
                        <select id="doc_desti" class="form-control">
                        </select>
                    </div> <!-- /.col-->   

                  </div> <!-- /. row -->
                  <div class="row"  style="margin-top:15px">
                    <div class="col-xs-3" id="div_doc_product">
                      <label ><font color="darkred">*</font>Product :</label>
                      <strong id="prodnameErr" class="text-red"></strong>       <!-- err label -->                                                               
                      <div class="input-group" style="margin-top:3px">
                       <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>
                        <input id="doc_product" type="text" name="doc_product" class="typeahead tt-query"  onblur="populate_var_dropdown(this.value);"  onfocus="populate_var_dropdown(this.value)" onfocus="populate_var_dropdown(this.value)" placeholder=" Search Product" disabled>
                      </div>    
                    </div> <!-- /.col-->
                    <div class="col-xs-3" id="div_doc_var">
                      <label ><font color="darkred">*</font>Unit of Measurement (UOM) :</label>   
                      <strong id="variantErr" class="text-red"></strong>       <!-- err label -->                                                            
                      <div class="input-group" style="margin-top:3px" >
                       <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                        <select id="doc_var" class="form-control" placeholder="Select UOM" onchange="populate_fifo_dropdown(this.value)" disabled>
                        </select>
                      </div>    
                    </div> <!-- /.col-->    

                    <div class="col-xs-3" id="div_doc_fifo">
                      <label ><font color="darkred">*</font>FIFO Selector:</label>   
                      <div class="input-group" style="margin-top:3px" >
                       <span class="input-group-addon"><i class="fa fa-sort-amount-desc"></i></span>
                        <select id="doc_fifo" class="form-control" onchange="priceTObutton()" disabled>
                          <option value="none" selected="selected" ></option>
                        </select>
                      </div>    
                    </div> <!-- /.col--> 

                    <div class="col-xs-3" id="div_doc_qty">
                      <label ><font color="darkred">*</font>Quantity :</label>   
                      <strong id="err_doc_qty" class="text-red"></strong>                      
                      <div class="input-group" style="margin-top:3px" >
                        <input id="doc_qty" class="form-control" type="number" disabled>
                      <span class="input-group-btn">
                      <button class="btn bg-teal" id="btn_add_list" data-toggle='tooltip' title="Add to Product List Below " data-placement='bottom'> <i class="ion-android-arrow-down"></i>  ADD TO LIST    </button >
                      </span>                                  
                      </div>    
                    </div> <!-- /.col--> 
                  </div> <!--/.row-->

                  <div class="row"  style="margin-top:15px">


                    <div class="col-sm-3" id="div_doc_ve">
                      <label ><font color="darkred">*</font>Vehicle :</label>
                      <strong id="err_doc_ve" class="text-red"></strong>       <!-- err label -->                                                               
                      <div class="input-group" style="margin-top:3px">
                        <span class="input-group-btn">
                        <button class="btn bg-teal" style="height:35px" id="btn_add_ve" title="Add Vehicle to list below" data-toggle="tooltip"> <i class="ion-android-arrow-down"></i>  ADD TO LIST    </button>
                        </span>
                        <input id="doc_ve" type="text" name="doc_ve" class="typeahead tt-query"  style="width:150px"  onblur="checking_doc_ve(this.value)" onfocus="checking_doc_ve(this.value)"  placeholder=" Search Vehicle">
                        <input type='hidden' id="chk_doc_ve">                                                                      
                      </div>    
                    </div> <!-- /.col-->

                    <div class="col-sm-3" id="div_doc_op">
                      <label ><font color="darkred">*</font>Operations :</label>
                      <strong id="err_doc_op" class="text-red"></strong>       <!-- err label -->                                                               
                      <div class="input-group" style="margin-top:3px">
                        <span class="input-group-btn">
                        <button class="btn bg-teal" id="btn_add_op" style="height:35px" title="Add Employee to list below" data-toggle="tooltip"> <i class="ion-android-arrow-down"></i>  ADD TO LIST    </button>
                        </span>  
                        <input id="doc_op" type="text" name="doc_op" class="typeahead tt-query" style="width:150px"  onblur="checking_doc_op(this.value)" onfocus="checking_doc_op(this.value)"  placeholder=" Search Employee">
                        <input type='hidden' id="chk_doc_op">                                    
                      </div>    
                    </div> <!-- /.col-->

                    <div class="col-sm-3" id="div_doc_role" style="margin-top:3px">
                      <label ><font color="darkred">*</font>Employee Role :</label>
                      <strong id="err_doc_role" class="text-red"></strong>       <!-- err label --> 
                        <select class="form-control" id="doc_role" style="width:200px">
                          <option value="none">--Select Role--</option>
                            <option value="stockman">Stockman</option>
                            <option value="clerk">Clerk</option>
                            <option value="driver">Driver</option> 
                            <option value="helper">Helper</option>                                                                               
                        </select>    
                    </div> <!-- /.col-->

                      <div class="col-xs-3" id="div_doc_remarks">
                        <label >Remarks:</label>
                        <strong class="text-red" id="err_doc_remarks"></strong>                      
                         <textarea id="doc_remarks" class="form-control" style="resize:none"></textarea>
                      </div>   <!--col-->

                    
                  </div> <!--/.row-->
                  <hr>
                    
            <div class="row" >
              <div class="col-sm-6">
                <div class="box-body">
                  <table id="ve_table" class="table table-bordered table-striped table-condensed table-hover">
                    <thead>
                        <th>VEHICLE</th>
                        <th style="width:10px"></th>                                               
                    </thead>
                   </table>
                </div><!-- /.box-body -->   
              </div> <!--/.col-3-->  

              <div class="col-sm-6">
                <div class="box-body">
                  <table id="op_table" class="table table-bordered table-striped table-condensed table-hover">
                    <thead>
                        <th>EMPLOYEE</th>
                        <th>Role</th> 
                        <th style="width:10px"></th>                                               
                    </thead>
                   </table>
                </div><!-- /.box-body -->                       
              </div><!--/.col-sm-3--> 
						</div> <!--/.row-->
						<hr>
						<div class="row">
              <div class="col-sm-12">
                <div class="box-body" >
                  <table id="pr_table" class="table table-bordered table-striped table-condensed table-hover">
                    <thead>
                        <th>PRODUCT</th>
                        <th>SKU</th> 
                        <th>Supplier</th>                         
                        <th>Category</th>                         
                        <th>UOM</th>
                        <th>Price</th>                                               
                        <th>Qty</th>                                                                                                                      
                        <th>Total</th>  
                        <th style="width:10px"></th>                                               
                    </thead>
                   </table>
                </div><!-- /.box-body -->                       
              </div><!--/.col-sm-7-->
            </div><!--/.row-->

              </div><!-- /.box -->  <!-- Input Fields -->
            </div><!-- /.col -->
            </div>  <!-- /.row -->

        </section><!-- /.content -->


      </div><!-- /.content-wrapper -->

      <?php include('../../mn/include/footer.php'); ?>
    </div><!-- ./wrapper -->
    <script src="controller2.js"></script>

    <!--MAIN CONTROLLER -->
    <script>
      $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-bottom-left",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "6000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "shoIMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }//.toastr config  

            //Initialize Select2 Elements
            $("#gd_name").select2();   
            $('#lo_co').select2();
            $('#doc_source').select2();
            $('#doc_desti').select2();
            populate_warehouse_dropdown();

            $('#doc_op').typeahead({
                name: 'doc_op',
                remote:'serverside/search_doc_op.php?key=%QUERY',
                limit : 10
            }); 

            $('#doc_ve').typeahead({
                name: 'doc_ve',
                remote:'serverside/search_doc_ve.php?key=%QUERY',
                limit : 10
            }); 

            $('#doc_product').typeahead({
                name: 'doc_product',
                remote:'serverside/search_doc_product.php?key=%QUERY',
                limit : 10
            }); 
          });          
    </script>
  </body>
</html>
