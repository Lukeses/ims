<?php
    include('../../../mn/include/connect.php');

if(!isset($_SESSION)) { 
  session_start(); 
} 
$gd_selected = $_SESSION['gd_selected'];


  $sql = "SELECT loc_id, loc_name
  FROM location l, company co, group_div gr
  WHERE loc_status = 'active' 
  AND (loc_type = 'branch')
  AND (l.loc_co_id = co.co_id) 
  AND (co.co_gr_id = gr.gr_id)
  AND (gr.gr_id = ?)
  ORDER BY loc_name ASC";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_selected));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['loc_id'],$fetch['loc_name']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    