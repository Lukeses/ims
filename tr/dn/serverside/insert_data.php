
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../../mn/include/connect.php');  
    include('../../../mn/include/log.php');  


    if(!isset($_SESSION)) { 
      session_start(); 
    } 
    $gd_selected = $_SESSION['gd_selected'];

    //vehicle array
    $rowCount_ve = $_POST['rowCount_ve'];
    $ve_id = $_POST['ve_id'];
    //operation array
    $rowCount_op = $_POST['rowCount_op'];
    $emp_id = $_POST['emp_id'];   
    $emp_role = $_POST['emp_role'];   
    //product array
    $rowCount_pr = $_POST['rowCount_pr'];
    $pr_id = $_POST['pr_id'];   
    $pr_qty = $_POST['pr_qty'];   
    $pr_price = $_POST['pr_price'];   

    $doc_date = $_POST['doc_date'];
    $doc_ref = $_POST['doc_ref'];
    $doc_remarks = $_POST['doc_remarks'];
    $doc_status = $_POST['doc_status'];    
    $doc_desti = $_POST['doc_desti'];
    $doc_fetch = $_POST['doc_fetch'];
    $doc_source = $_POST['doc_source'];

    //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('doc'.$year);   

    $sql = "INSERT INTO document values(?,?,?,?,?,?,?,?,?,?)"; //INSERT recordd
    $q = $conn -> prepare($sql);
    $q -> execute(array($id,$gd_selected,'DN',$doc_source,$doc_desti,$doc_ref,$doc_remarks,$doc_date,$doc_status,$doc_fetch));

    $trail_id =uniqid('at'.$year);  
    $sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
    $q = $conn -> prepare($sql);    
    $q -> execute(array($trail_id,'Transaction','DN', 'CREATE', 'New: ID:'.$id. ', Ref:'.$doc_ref.', Date:'.$doc_date.', Source ID:'.$doc_source.', Destination ID:'.$doc_desti.', Status:'.$doc_status, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));
            


            // INSERT DATA TO document_vehicle
    for($x=0; $x<$rowCount_ve; $x++){   
        $dv_id =uniqid('dv'.$year);  

        $sql = "INSERT INTO document_vehicle VALUES(?,?,?)";
        $q = $conn -> prepare($sql);
        $q -> execute(array($dv_id,$id,$ve_id[$x]));
    }    

            // INSERT DATA TO document_operation
    for($x=0; $x<$rowCount_op; $x++){   
        $do_id =uniqid('do'.$year);  

        $sql = "INSERT INTO document_operation VALUES(?,?,?,?)";
        $q = $conn -> prepare($sql);
        $q -> execute(array($do_id,$id,$emp_id[$x],$emp_role[$x]));
    }    


        //check product version
    for($x=0; $x<$rowCount_pr; $x++){   

        $dp_id =uniqid('dp'.$year);  

        $sql = "INSERT INTO document_product VALUES(?,?,?,?)";
        $q = $conn -> prepare($sql);
        $q -> execute(array($dp_id,$id,$pr_id[$x],$pr_qty[$x]));        

    }        
$conn = null;



echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
