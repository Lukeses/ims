


	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	  

  function validateAdd(){

    if($('#gd_selected').val()==''){
      $('#btn_select_groupdiv').removeClass('bg-green');
      $('#btn_select_groupdiv').addClass('bg-red');      
      return false;      
    }
    else{   
      return true;
    }    
  }

  function select_groupdiv(){
    populate_dn_dropdown();    
    $('#gd_name').select2().select2('val','none');              
    $('#gd_nameDiv').removeClass('has-error');
    $('#gd_modal').modal('show');    
  }     

//GRN

  $('#btn_gd_submit').click(function(){
    if($('#gd_name').val()=='none'){
      $('#gd_nameDiv').addClass('has-error');
    }
    else{
      $('#btn_select_groupdiv').removeClass('bg-red');
      $('#btn_select_groupdiv').addClass('bg-green');         
      $('#gd_nameDiv').removeClass('has-error');
      $('#gd_selected').val($('#gd_name').val());
      $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );            
      $('#gd_submit_name').val( $( "#gd_name option:selected" ).text()  );   
      $('#gd_modal').modal('hide');                   
      load_dn_table($('#gd_selected').val());
      change_session_group();
    }
  })

  function change_session_group(){
    var gr_name = $( "#gd_name option:selected" ).text();  
    var gr_id = $('#gd_name').val();
    var dataString = 'gr_id='+gr_id+'&gr_name='+gr_name;
    //ajax now
    $.ajax ({
      type: "POST",
      url: "../../mn/include/change_session_group.php",
      data: dataString,
      success: function(s){         
      }  
    }); 
    //ajax end      
  }

  function populate_dn_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_gd_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#gd_name').empty();
        $('#gd_name').append('<option value="none">--Select Group--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
  } // 


	//tables
	    var dn_table = $('#dn_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [9] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    


  function load_dn_table(gd_name){ 
    //ajax now
    $('#loading').modal({backdrop: 'static'})                     
    $('#loading').modal('show');
        dn_table.fnClearTable();            
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      data: 'gd_name='+gd_name,
      cache: false,
      success: function(s)
      {
        $('#loading').modal('hide');
        for(var i = 0; i < s.length; i++) 
        { 
             var statcol; 
              if(s[i][4]=='Shipped')
                statcol = "label label-success";
              else if(s[i][4]=='Pending')
                statcol = "label label-warning";     
              else if(s[i][4]=='Deleted')
                statcol = "label label-default";             

          dn_table.fnAddData
          ([
            s[i][0],s[i][1],s[i][2],s[i][3],
             '<span class="'+statcol+'">'+s[i][4].toUpperCase()+'</span>',
            s[i][5],s[i][6],s[i][7],comma(s[i][8]),
          '<form action="view.php" target="_blank"  method="GET" ><input name="doc_no"  type="hidden" value='+s[i][0]+'><input name="gd_selected" type="hidden" value='+gd_name+'> <button data-toggle="tooltip" id="view" class="btn btn-xs btn-default" value='+s[i][0]+' title="View"><i class="fa fa-eye"></i></button></form>',
          ],false); 
          dn_table.fnDraw();

        }       
      }  
    }); 
    //ajax end  
  } //.load dn_table

  function initialize(){
    if($('#gd_selected').val()==''){
      select_groupdiv();
    }
    else{
      load_dn_table($('#gd_selected').val());
    }
  }

  initialize();

//. GRN	   

//additional functions
  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 

 
// .addition functions  
