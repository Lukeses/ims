<?php
    session_start();    

    if(!isset($_SESSION["u_name"]) || !isset($_SESSION["u_type"]) )
    { ?>
      <script type="text/javascript">   
        window.location="mn/user/login.php";
      </script>
   <?php } 
    else {  
      $u_name = $_SESSION["u_name"];
      $u_type = $_SESSION["u_type"];

      if(isset($_SESSION['session_group']) ){
        $session_group = $_SESSION['session_group'];
        $session_group_name = $_SESSION['session_group_name'];        
      }
      else{
        $session_group = '';
        $session_group_name = '';        
      }
  }

?>
<?php include('mn/include/group_div.html'); ?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link href="resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />      
    <!-- Font Awesome -->
    <link href="resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="resources/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="resources/dist/css/skins/_all-skins.min.css">
    <link href="resources/plugins/select2/select2.css" rel="stylesheet" />
    <link href="resources/plugins/select2/bootsrap-select.css" rel="stylesheet" />    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- More App -->
    <script src="resources/dist/js/app.min.js"></script>
    <script src="resources/plugins/select2/select2.min.js"></script>
    <script src="resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>         
    <!-- ChartJS 1.0.1 -->
    <script src="resources/plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="resources/dist/js/pages/dashboard2.js"></script>   
    <!-- AdminLTE for demo purposes -->
    <script src="resources/dist/js/demo.js"></script>     
    <script src="resources/plugins/datatables/formatted-num.js" type="text/javascript"></script> 
    <script src="resources/plugins/datatables/formatted-numbers.js" type="text/javascript"></script>       
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <?php echo'<img src="resources/dist/img/'.$u_type.'.png"'.' class="user-image" alt="User Image">'; ?>
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo(ucwords($u_name)); ?></span>
                </a>
                  <ul class="dropdown-menu" style="width:10%;border-radius:5px">
                    <li style="text-align:center"> 
                      <small style="font-size:0.8em"><?php echo ucfirst($u_type); ?></small>
                    </li>
                      <li class="divider"></li>
                  <?php if($u_type=="admin"){ ?>                      
                    <li><a href="mn/uc/uc.php"><i class="fa fa-users"></i> User Accounts</a></li>
                  <?php } ?>
                    <li><a onclick="return logout()" href="mn/user/processlogout.php"> <i class="fa fa-sign-in"></i><span>Log-out</span>    
              </a></li><br>
                  </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">MENU NAVIGATION</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
              <a href="index.php"><i class="fa fa-gear"></i> <span>Maintenance</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="mn/pr/pr.php"><i class="fa fa-circle-o"></i> Product</a></li>
                <li><a href="mn/su/su.php"><i class="fa fa-circle-o"></i> Supplier</a></li>
                <li><a href="mn/ve/ve.php"><i class="fa fa-circle-o"></i> Vehicle</a></li>                
                <li><a href="mn/em/em.php"><i class="fa fa-circle-o"></i> Employee</a></li>
                <li><a href="mn/lo/lo.php"><i class="fa fa-circle-o"></i> Location</a></li>                
                <li><a href="mn/gc/gc.php"><i class="fa fa-circle-o"></i>Groups & Company</a></li>    
              </ul>
            </li>

            <li><a href="tr/po/po.php"><i class="fa fa-cart-arrow-down"></i> <span>Purchase Order</span></a></li>

            <li class="treeview">
              <a href="#"><i class="fa fa-bar-chart"></i> <span>P.O. Analysis</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="tr/poa/poa_sup.php"><i class="fa fa-circle-o"></i>By Supplier</a></li>
                <li><a href="tr/poa/poa_vessel.php"><i class="fa fa-circle-o"></i>By Vessel</a></li>
                <li><a href="tr/poa/poa_cat.php"><i class="fa fa-circle-o"></i>By Category</a></li>                                              
              </ul>
            </li>    


            <li class="treeview">
              <a href="#"><i class="fa fa-paste"></i> <span>Operation</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="tr/gr/gr.php"><i class="fa fa-circle-o"></i>Goods Received</a></li>
                <li><a href="tr/dn/dn.php"><i class="fa fa-circle-o"></i>Delivery</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Stock Transfer</a>
                  <ul class="treeview-menu">
                    <li><a href="tr/st-bb/st.php"><i class="fa fa-circle-o"></i>Branch</a></li>
                    <li><a href="tr/st-ww/st.php"><i class="fa fa-circle-o"></i>Warehouse</a></li> 
                    <li><a href="tr/st-bw/st.php"><i class="fa fa-circle-o"></i>Pull-Out</a></li>                                                                                                                       
                  </ul>                  
                </li>                                              
              </ul>
            </li>            

            <li class="treeview">
              <a href="#"><i class="fa fa-exchange"></i> 
                <span>Stock Movement</span><i class="fa fa-angle-left pull-right"></i></a>    
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Warehouse </a>
                  <ul class="treeview-menu">
                    <li><a href="sm/wh-i/sm.php"><i class="fa fa-circle-o"></i>Inbound</a></li>
                    <li><a href="sm/wh-o/sm.php"><i class="fa fa-circle-o"></i>Outbound</a></li>    
                    <li><a href="sm/pr/sm.php"><i class="fa fa-circle-o"></i>Procurement</a></li>                                                                                                                    
                  </ul>
                </li>

                <li><a href="#"><i class="fa fa-circle-o"></i> Branch </a>
                  <ul class="treeview-menu">
                    <li><a href="sm/br-i/sm.php"><i class="fa fa-circle-o"></i>Inbound</a></li>
                    <li><a href="sm/br-o/sm.php"><i class="fa fa-circle-o"></i>Outbound</a></li>                                                                                                                                     
                  </ul>
                </li>                            
              </ul> 

            <li class="treeview">
              <a href="#"><i class="fa fa-sitemap"></i> 
                <span>Inventory Level</span><i class="fa fa-angle-left pull-right"></i></a>    
              </a>
              <ul class="treeview-menu">
                <li><a href="il/wh/il.php"><i class="fa fa-circle-o"></i> Warehouse </a></li>
                <li><a href="il/br/il.php"><i class="fa fa-circle-o"></i> Branch </a></li>
                <li><a href="il/tot/il.php"><i class="fa fa-circle-o"></i> Total </a></li>
                <li><a href="il/pr/il.php"><i class="fa fa-circle-o"></i> Product </a></li>                
              </ul> 
            </li>


            <li><a href="tr/ld/ld.php"><i class="fa fa-ban"></i> <span>Lost/Damaged/Return</span></a></li>

            <li class="treeview">
              <a href="#"><i class="fa fa-wrench"></i> <span>Utilities</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="mn/at/at.php"><i class="fa fa-circle-o"></i>Audit Trail</a></li>   
                <li><a href="mn/lb/lb.php"><i class="fa fa-circle-o"></i>Local Backup</a></li>                                                                           
              </ul>
            </li>      
                        
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <a href="javascript:select_groupdiv()" id="btn_select_groupdiv" role="button" class="btn bg-green pull-right" data-toggle='tooltip' title="Re-Select Group " data-placement='left'><i class="ion-android-checkbox-outline"></i> Re-Select Group</a>                                        
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <?php echo'<text class="lead" id="gd_selected_name">'.$session_group_name.'</text>';  ?>                    
          <?php echo'<input type="hidden" id="gd_selected" value="'.$session_group.'">';  ?>          

        </section>

        <!-- Main content -->
        <section class="content">

          <!--modal-->
                <div id="gd_modal" class="modal fade" >
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content" >
                            <div class="modal-header">
                                <button type="butt on" class="close" title="Close" data-dismiss="modal" aria-hidden="true">&times;</button>                
                                <h4 class="modal-title"> <i class="fa fa-folder-o"></i> Select Group </h4>
                            </div>          
                            <div class="modal-body" >
              <!-- ------------------------------------------------------------------------------------------- -->
                              <div class="row" style="margin-bottom:5px"> <!-- ROW 1 -->

                                <div class="col-md-12">
                                  <div class="form-group" id="gd_nameDiv">
                                    <select id="gd_name" class="form-control input-lg" placeholder="--- Select a Group --">
                                    </select>
                                  </div><!-- /.form-group -->                    
                                </div>



                              </div> <!-- /.row -->   
              <!-- ------------------------------------------------------------------------------------------- -->



                              </form>
                            </div>
                            <div class="modal-footer" style="text-align:center ">
                                <button class="btn btn-lg btn-block bg-green" style="font-size:22px;" id="btn_gd_submit" data-toggle='tooltip' title="Select Group" data-placement='top'>
                                  <i class="ion-android-checkbox-outline"></i> Select
                                </button>                    
                            </div>
                        </div>
                    </div>
                </div> 
          <!--modal-->


          <div class="row" style="margin-top:10px">
            <div class="col-sm-12">
              <div class="box box-danger collapse-box">
                <div class="box-header with-border">
                  <h2 class="box-title"><i class="fa fa-calendar-times-o"></i> Procurement Schedule by Stock Level</h2>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" title="Minimize / Maximize"><i class="fa fa-minus"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body" style="display: block;">
                  <div class="table-responsive">
                    <table id="ps_table" class="table-striped table-hover table no-margin">
                      <thead>
                        <tr>
                          <th>Warehouse</th>
                          <th style="min-width:100px">Product</th>
                          <th style="width:100px">SKU</th>
                          <th>UOM</th>
                          <th>Category</th>
                          <th>Supplier</th>
                          <th>Owner</th>
                          <th>Price</th>
                          <th style="width:80px">Treshold</th>
                          <th style="width:80px">In-Stock</th>
                        </tr>
                      </thead>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
              </div>              
            </div>
          </div>

          <div class="row">    
            <div class="col-sm-6 col-xs-12">
              <div class="box box-default" >
                <div class="box-body">
                  <div class="description-block" >
                    <h5 class="description-header" id="lbl_grn_pending" ></h5>
                    <span class="description-text text-success">PENDING</span>
                  </div><!-- /.description-block -->

                  <div class="description-block" >
                    <h5 class="description-header" id="lbl_grn_shipped"></h5>
                    <span class="description-text text-success">TODAY RECEIVED</span>
                  </div><!-- /.description-block -->
              <div style="text-align:center">
                <h3 class="text-success"><strong>GRN</strong></h3>
              </div>
                </div> <!-- /.box-body -->
              </div> <!-- /. box-solid -->
            </div><!--/.col-->   
            <div class="col-sm-6 col-xs-12">
              <div class="box box-default">
                <div class="box-body">
                  <div class="description-block" >
                    <h5 class="description-header" id="lbl_dn_pending" ></h5>
                    <span class="description-text text-success">PENDING</span>
                  </div><!-- /.description-block -->

                  <div class="description-block" >
                    <h5 class="description-header" id="lbl_dn_shipped"></h5>
                    <span class="description-text text-success">TODAY SHIPPED</span>
                  </div><!-- /.description-block -->
              <div style="text-align:center">
                <h3 class="text-navy"><strong>DN</strong></h3>
              </div>
                </div> <!-- /.box-body -->
              </div> <!-- /. box-solid -->
            </div><!--/.col-->
          </div>   <!--/.row-->      






          <!-- Your page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <?php include('mn/include/footer.php'); ?>


    </div><!-- ./wrapper -->



<script> // MAIN SCRIPT
  $(document).ready(function () {
    $('#gd_selected').val('');
    $('#gd_selected_name').text('');
    $('[data-toggle="tooltip"]').tooltip();
    //Initialize Select2 Elements
    $("#gd_name").select2(); 

  });  



  function logout(){

    var choice = confirm("Are you sure you want to Log-out?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }

  function select_groupdiv(){
    populate_gr_dropdown();    
    $('#gd_name').select2().select2('val','none');                  
    $('#gd_nameDiv').removeClass('has-error');    
    $('#gd_modal').modal('show');    
  }   

  function populate_gr_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "mn/pr/serverside/populate_gd_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#gd_name').empty();
        $('#gd_name').append('<option value="none">--Select Group--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
  } // 

  $('#btn_gd_submit').click(function(){
    if($('#gd_name').val()=='none'){
      $('#gd_nameDiv').addClass('has-error');
    }
    else{
      $('#btn_select_groupdiv').removeClass('bg-red');
      $('#btn_select_groupdiv').addClass('bg-green');         
      $('#gd_nameDiv').removeClass('has-error');
      $('#gd_selected').val($('#gd_name').val());
      $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );
      $('#gd_modal').modal('hide');    
      load_ps_table(); 
      load_doc_labels();   
      change_session_group();
    }
  })


  function change_session_group(){
    var gr_name = $( "#gd_name option:selected" ).text();  
    var gr_id = $('#gd_name').val();
    var dataString = 'gr_id='+gr_id+'&gr_name='+gr_name;

    //ajax now
    $.ajax ({
      type: "POST",
      url: "mn/include/change_session_group.php",
      data: dataString,
      success: function(s){         
      }  
    }); 
    //ajax end      
  }

  //table
    //Initialize Datatables
      var ps_table = $('#ps_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
        "aaSorting": []
      });  //Initialize the datatable department      
  ///table

  function load_ps_table(){ 
    ps_table.fnClearTable();            
    //ajax now
    $.ajax ({
      type: "POST",
      url: "mn/dash/serverside/load_ps_table.php",
      dataType: 'json',      
      data: 'gr_id='+$('#gd_selected').val(),
      cache: false,
      success: function(s)
      {
        for(var i = 0; i < s.length; i++) {

         var statcol; 
          if(parseInt(s[i][11])==parseInt(s[i][10]))
            statcol = "label label-warning";
          else if(parseInt(s[i][11])<parseInt(s[i][10]))
            statcol = "label label-danger";     
 

          if(parseInt(s[i][11])<=parseInt(s[i][10])){
            ps_table.fnAddData
            ([
              s[i][2],s[i][3],s[i][4],s[i][5],s[i][6],s[i][7],s[i][8],s[i][9],s[i][10],'<span class="'+statcol+'">'+s[i][11]+'</span>',
            ],false); 
            ps_table.fnDraw();
          }
        }       
      }  
    }); 
    //ajax end  
  } // 

  function load_doc_labels(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "mn/dash/serverside/load_doc_labels.php",
      dataType: 'json',      
      data: 'gr_id='+$('#gd_selected').val(),
      cache: false,
      success: function(s)
      {
        for(var i = 0; i < s.length; i++) {
          $('#lbl_grn_pending').text('PHP '+comma(s[i][0])); $('#lbl_dn_pending').text('PHP '+comma(s[i][2])); 
          $('#lbl_grn_shipped').text('PHP '+comma(s[i][1])); $('#lbl_dn_shipped').text('PHP '+comma(s[i][3]));            
        }       
      }  
    }); 
    //ajax end  
  } // 

  function initialize(){
    if($('#gd_selected').val()==''){
      select_groupdiv();
    }
    else{
      load_ps_table();
      load_doc_labels();
    }
  }

  initialize();

  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 
</script>


  </body>
</html>
