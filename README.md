# Instructions to setup#

1. Clone this repo
2. Create a database on your mysql server. Name it to ims
3. On phpmyadmin, (or use other methods) import the database in /db/ims.sql  to your created database (must be same database name)
4. Configure database settings: Open to /mn/include/connect.php file. And rename the database name, username and password.
5. Done