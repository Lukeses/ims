<?php
    include('../../../mn/include/connect.php');

  $lo_id = $_POST['lo_id'];


  $sql = "SELECT pv.pv_id, pr.prod_name, pr.prod_sku,pr.prod_var,
(SELECT c.cat_name FROM category c WHERE c.cat_id = pr.prod_cat_id) as category, 
(SELECT s.sup_name FROM supplier s WHERE s.sup_id = pr.prod_sup_id) as supplier, 
(SELECT co.co_name FROM company co WHERE co.co_id = pr.prod_co_id) as company,
pv.pv_price, SUM(dp.dp_qty) as qty, SUM(pv.pv_price*dp.dp_qty) as value
  FROM product_version pv, product pr, document_product dp, document d 
  WHERE (pv.pv_prod_id = pr.prod_id) 
  AND (d.doc_id = dp.dp_doc_id)
  AND (dp.dp_prod_id = pv.pv_id)
  AND (d.doc_desti = ?)
  AND (d.doc_status='shipped')
  GROUP BY pv.pv_id
  ORDER BY pr.prod_name ASC, pv.pv_date DESC";

  $q = $conn->prepare($sql);
  $q -> execute(array($lo_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['pv_id'],$fetch['prod_name'],$fetch['prod_sku'],$fetch['prod_var'],
      $fetch['category'],$fetch['supplier'],$fetch['company'],$fetch['pv_price'],$fetch['qty'],
      $fetch['value']);          
  }         
$conn = null;             

echo json_encode($output);
?>    