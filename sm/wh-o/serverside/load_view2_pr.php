<?php
    include('../../../mn/include/connect.php');

  $lo_id = $_POST['lo_id'];
  $pv_id = $_POST['pv_id'];


  $sql = "SELECT d.doc_id, d.doc_type,d.doc_ref, d.doc_date , dp.dp_qty, 
COALESCE((SELECT l.loc_name FROM location l WHERE l.loc_id = d.doc_source),d.doc_source) as source,
COALESCE((SELECT l.loc_name FROM location l WHERE l.loc_id = d.doc_desti),d.doc_desti) as desti, 
(dp.dp_qty*pv.pv_price) as total
FROM document d, document_product dp, location l, product_version pv 
WHERE (d.doc_id = dp.dp_doc_id) 
AND (dp.dp_prod_id = pv.pv_id) 
AND (pv.pv_id = ?)
AND (d.doc_source = ?)
AND (d.doc_status = 'shipped')
GROUP BY d.doc_id";

  $q = $conn->prepare($sql);
  $q -> execute(array($pv_id,$lo_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['doc_id'],$fetch['doc_type'],$fetch['doc_ref'],$fetch['doc_date'],
      ucWords($fetch['source']),$fetch['desti'],$fetch['dp_qty'],$fetch['total']);          
  }         
$conn = null;             

echo json_encode($output);
?>    