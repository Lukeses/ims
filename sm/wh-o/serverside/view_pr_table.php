<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT pv.pv_id as id, p.prod_name as name, p.prod_sku as sku, 
(SELECT c.cat_name FROM category c WHERE c.cat_id = p.prod_cat_id) as category, 
(SELECT s.sup_name FROM supplier s WHERE s.sup_id = p.prod_sup_id) as supplier, 
(SELECT co.co_name FROM company co WHERE co.co_id = p.prod_co_id) as company, 
p.prod_var as variant, pv.pv_price as price, dp.dp_qty as qty, (pv.pv_price*dp.dp_qty) as total
FROM product p, product_version pv, document_product dp, document d 
WHERE (d.doc_id = ?) 
AND (dp.dp_doc_id = d.doc_id) 
AND (dp.dp_prod_id = pv.pv_id) 
AND (pv.pv_prod_id = p.prod_id)
GROUP BY dp.dp_id  ";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['id'],$fetch['name'],$fetch['sku'],$fetch['category'],
      $fetch['variant'],$fetch['supplier'],$fetch['company'],$fetch['price'],$fetch['qty'],$fetch['total']);          
  }         
$conn = null;             

echo json_encode($output);
?>    