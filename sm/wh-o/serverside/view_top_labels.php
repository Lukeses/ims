<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT doc_ref,doc_date,doc_status,gr_name,
coalesce((SELECT loc_name FROM location l WHERE l.loc_id = d.doc_source),d.doc_source) as source, 
coalesce((SELECT loc_name FROM location l WHERE l.loc_id = d.doc_desti),d.doc_desti) as desti,
SUM(dp.dp_qty*pv.pv_price) as tot, doc_type
FROM document d,group_div gr, document_product dp, product_version pv
WHERE (d.doc_gr_id = gr.gr_id)
AND (dp.dp_doc_id = d.doc_id)
AND (dp.dp_prod_id = pv.pv_id)
AND (d.doc_id = ?)
GROUP BY d.doc_id
ORDER BY doc_status desc, doc_date desc";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['doc_ref'],$fetch['doc_date'],$fetch['doc_status'],
      $fetch['gr_name'],ucwords($fetch['source']),$fetch['desti'],$fetch['tot'],$fetch['doc_type']);          
  }         
$conn = null;             

echo json_encode($output);
?>    