

   

//Stock Movement

  $('#btn_gd_submit').click(function(){
    if($('#gd_name').val()=='none'){
      $('#gd_nameDiv').addClass('has-error');
    }
    else{
      $('#btn_select_groupdiv').removeClass('bg-red');
      $('#btn_select_groupdiv').addClass('bg-green');         
      $('#gd_nameDiv').removeClass('has-error');
      $('#gd_selected').val($('#gd_name').val());
      $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );      
      load_lo_table($('#gd_selected').val());
      $('#gd_modal').modal('hide');
      change_session_group();
    }
  })

  function change_session_group(){
    var gr_name = $( "#gd_name option:selected" ).text();  
    var gr_id = $('#gd_name').val();
    var dataString = 'gr_id='+gr_id+'&gr_name='+gr_name;
    //ajax now
    $.ajax ({
      type: "POST",
      url: "../../mn/include/change_session_group.php",
      data: dataString,
      success: function(s){         
      }  
    }); 
    //ajax end      
  }

  function populate_gr_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_gd_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#gd_name').empty();
        $('#gd_name').append('<option value="none">--Select Group--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
  } // 


	//tables
	    var lo_table = $('#lo_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [5,6] } ],
	      "aaSorting": []
	    });  //Initialize the datatable location

      var pr_table = $('#pr_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [9] } ],
        "aaSorting": []
      });  //Initialize the datatable product      

      var in_table = $('#in_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
        "aaSorting": []
      });  //Initialize the datatable inbound          

      var doc_table = $('#doc_table').dataTable({
          columnDefs: [
         { type: 'formatted-num', targets: 0 }
         ],       
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [7] } ],
        "aaSorting": []
      });  //Initialize the datatable document        

	//.tables    


  function load_lo_table(gd_name){ 
    $('#lbl_grand_total').text('');
    //ajax now
    $('#loading').modal('show');
        lo_table.fnClearTable();            
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      data: 'gd_name='+gd_name,
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        var gt = 0;
        for(var i = 0; i < s.length; i++) 
        { 
          gt = parseFloat(gt,10)+parseFloat(s[i][5],10);
          lo_table.fnAddData
          ([
            s[i][1],s[i][2],s[i][3],s[i][4],comma(s[i][5]),
            '<form action="view1_pr.php" method="GET" ><input name="lo_id"  type="hidden" value='+s[i][0]+' > <textarea name="lo_name" style="display:none" >'+s[i][1]+'</textarea> <textarea name="gd_name" style="display:none" >'+$('#gd_selected_name').text()+'</textarea> <button id="view" class="btn btn-xs btn-default" value='+s[i][0]+' title="View By Products" data-toggle="tooltip" title="View By Product" data-placement="top"> <i class="fa fa-tag"></i> </button></form>',                  
            '<form action="view1_doc.php" method="GET" ><input name="lo_id"  type="hidden" value='+s[i][0]+'>  <textarea name="lo_name" style="display:none" >'+s[i][1]+'</textarea> <textarea name="gd_name" style="display:none" >'+$('#gd_selected_name').text()+'</textarea> <button id="view" class="btn btn-xs btn-default" value='+s[i][0]+' title="View By Document" data-toggle="tooltip" title="View By Document" data-placement="top"> <i class="fa fa-file-text-o"></i> </button></form>'   
          ],false); 
          lo_table.fnDraw();

        }       
        $('#lbl_grand_total').text('₱'+comma(gt.toFixed(2)));
      }  
    }); 
    //ajax end  
  } //.load lo_table

  function load_pr_table(lo_id){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_view1_pr.php",
      dataType: 'json',      
      data: 'lo_id='+lo_id,
      cache: false,
      success: function(s)
      {
        $('#loading').modal('hide');
        var gt=0;
        for(var i = 0; i < s.length; i++) 
        { 
          gt = parseFloat(gt,10)+parseFloat(s[i][9],10);          
          pr_table.fnAddData
          ([
            s[i][1],comma(s[i][7]),comma(s[i][8]),comma(s[i][9]),s[i][2],s[i][3],s[i][4],s[i][5],s[i][6],
            '<form action="view2_pr.php" method="GET" ><input name="lo_id"  type="hidden" value='+$('#lo_id').val()+' ><input name="pv_id"  type="hidden" value='+s[i][0]+' >  <textarea name="gd_name" style="display:none" >'+$('#gd_selected_name').text()+'</textarea><textarea name="lo_name" style="display:none" >'+$('#lbl_lo_name').text()+'</textarea> <textarea name="pr_name" style="display:none" >'+s[i][1]+'</textarea><textarea name="pr_price" style="display:none" >₱'+comma(s[i][7])+'</textarea><textarea name="pr_var" style="display:none" >'+s[i][3]+'</textarea><textarea name="pr_sku" style="display:none" >'+s[i][2]+'</textarea><textarea name="pr_cat" style="display:none" >'+s[i][4]+'</textarea><textarea name="pr_sup" style="display:none" >'+s[i][5]+'</textarea><textarea name="pr_co" style="display:none" >'+s[i][6]+'</textarea>        <button id="view" class="btn btn-xs btn-default" value='+s[i][0]+' data-toggle="tooltip" title="View" data-placement="top"> <i class="fa fa-eye"></i> </button></form>',                              
          ],false); 
          pr_table.fnDraw();

        }      
        $('#lbl_grand_total').text('₱'+comma(gt.toFixed(2))); 
      }  
    }); 
    //ajax end  
  } //.load lo_table

  function load_in_table(lo_id,pv_id){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_view2_pr.php",
      dataType: 'json',      
      data: 'pv_id='+pv_id+'&lo_id='+lo_id,
      cache: false,
      success: function(s)
      {
        $('#loading').modal('hide');
        var gt = 0;
        var inbound = 0;
        for(var i = 0; i < s.length; i++) 
        { 
          gt = parseFloat(gt,10)+parseFloat(s[i][7],10);
          inbound = parseInt(inbound)+parseInt(s[i][6]);          
          in_table.fnAddData
          ([ 
            s[i][1],s[i][2],s[i][3],s[i][4],s[i][5],comma(s[i][6]),
          ],false); 
          in_table.fnDraw();
        }       
        $('#lbl_grand_total').text('₱'+comma(gt.toFixed(2)));
        $('#lbl_total_inbound').text(comma(inbound));
      }  
    }); 
    //ajax end  
  } //.load lo_table

  function load_doc_table(lo_id){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_view1_doc.php",
      dataType: 'json',      
      data: 'lo_id='+lo_id,
      cache: false,
      success: function(s)
      {
        $('#loading').modal('hide');
        var gt = 0;
        for(var i = 0; i < s.length; i++) 
        { 
          gt = parseFloat(gt,10)+parseFloat(s[i][6],10);                      
             var statcol; 
              if(s[i][3]=='Shipped')
                statcol = "label label-success";
              else if(s[i][3]=='Pending')
                statcol = "label label-warning";     
              else if(s[i][3]=='Deleted')
                statcol = "label label-danger";              

          doc_table.fnAddData
          ([
            s[i][0],s[i][7].toUpperCase(),s[i][1],s[i][2],
             '<span class="'+statcol+'">'+s[i][3].toUpperCase()+'</span>',
            s[i][4],s[i][5],comma(s[i][6]),
          '<form action="view2_doc.php" method="GET" ><input name="doc_no"  type="hidden" value='+s[i][0]+' > <input name="gd_selected" type="hidden" value='+s[i][0]+'> <button id="view" class="btn btn-xs btn-default" value='+s[i][0]+' data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></button></form>',
          ],false); 
          doc_table.fnDraw();
        }       
        $('#lbl_grand_total').text('₱'+comma(gt.toFixed(2)));         
      }  
    }); 
    //ajax end  
  } //.load doc_table

  function initialize(){
    if($('#gd_selected').val()==''){
      select_groupdiv();
    }
    else{
      load_lo_table($('#gd_selected').val());
    }
  }

  initialize();

//. stock movement	   




//additional functions

  function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function uncomma(x) {
    var string1 = x;
    for (y = 0; y < 12; y++) {
      string1 = string1.replace(/\,/g, '');
    }
    return string1;
  } 

    function logout(){

    var choice = confirm("Are you sure you want to Log-out?");
    if(choice==true)
    {
      return true;
    }
    else
      return false;
  }
     
  function select_groupdiv(){
    populate_gr_dropdown();    
    $('#gd_name').select2().select2('val','none');              
    $('#gd_nameDiv').removeClass('has-error');
    $('#gd_modal').modal('show');    
  }  

  $('[data-toggle="tooltip"]').tooltip();

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "500",
  "hideDuration": "1000",
  "timeOut": "6000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}//.toastr config  