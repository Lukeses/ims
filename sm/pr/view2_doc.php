<?php
require('../../mn/include/log_tr.php');
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>View | Document</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- toastr-->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />    

    <!--<link href="typeaheadjs.css" rel="stylesheet" type="text/css" />    -->

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/typeahead.min.js" type="text/javascript"></script>    
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>

      <style type="text/css">

        .typeahead {
          background-color: #FFFFFF;
          font-size: 14px;
          height: 35px;
          width:200px;
          outline:none;
          border: lightgrey solid 1px;  
        }

        .typeahead:focus {
          border: 2px solid #0097CF;
        }
        .tt-query {
          box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;


        }
        .tt-hint {
          color: white;

        }
        .tt-dropdown-menu {
          background-color: #FFFFFF;
          border: 1px solid rgba(0, 0, 0, 0.2);
          border-radius: 8px;
          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
          margin-top: 12px;
          padding: 8px 0;
          width: 250px;
        }
        .tt-suggestion {
          font-size: 16px;
          line-height: 22px;
          padding: 3px 18px;
        }
        .tt-suggestion.tt-is-under-cursor {
          background-color: #0097CF;
          color: #FFFFFF;
        }
        .tt-suggestion p {
          margin: 0;
        }        
      </style>



  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <!--<a href="javascript:select_groupdiv()" id="btn_select_groupdiv" role="button" class="btn btn-default pull-right" data-toggle='tooltip' title="Re-Select Group " data-placement='left'><i class="ion-android-checkbox-outline"></i> Re-Select Group</a>-->                              
          <h1>
            Procurement
            <small>
            <?php 
              $gd = $_GET['gd_selected']; 
              $doc_no = $_GET['doc_no'];
              echo'<input type="hidden" id="gd_selected" value='.$gd.'>';
              echo'<input type="hidden" id="doc_no" value='.$doc_no.'>';              
              echo($doc_no);
            ?>   
            </small>
          </h1>       
        </section>

        <!-- Main content -->
        <section class="content">

          <?php include('../../mn/include/loading.html') ?>
          <?php include('../../mn/include/group_div.html') ?>




          <div class="row" style="margin-top:10px">                     <!-- TOP -->
            <div class="col-md-12">
              <div class="box box-default">
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-3 col-xs-12">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-cube"></i></span>
                        <h5 class="description-header" id="lbl_doc_gd"></h5>
                        <span class="description-text">GROUP</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-12">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-building"></i></span>
                        <h5 class="description-header" id="lbl_doc_source"></h5>
                        <span class="description-text">SOURCE</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-12">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-building-o"></i></span>
                        <h5 class="description-header" id="lbl_doc_desti"></h5>
                        <span class="description-text">DESTINATION</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-12">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-file-text-o"></i></span>
                        <h5 class="description-header" id="lbl_doc_type"></h5>
                        <span class="description-text">DOCUMENT TYPE</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->                    
                  </div><!-- /.row -->
                </div><!-- /.box-footer -->                
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-barcode"></i></span>
                        <h5 class="description-header" id="lbl_doc_ref"></h5>
                        <span class="description-text">REFERENCE NO</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-calendar"></i></span>
                        <h5 class="description-header" id="lbl_doc_date"></h5>
                        <span class="description-text">DATE</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-calculator"></i></span>
                        <h5 class="description-header" id="lbl_grand_total"></h5>
                        <span class="description-text">GRAND TOTAL</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                      <div class="description-block border-right">
                        <span class="description-percentage"><i class="fa fa-truck"></i></span>
                        <h5 class="description-header" id="lbl_doc_status"></h5>
                        <span class="description-text">STATUS</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>  <!-- /.row -->



          <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-tags"></i> Product</a></li>
                  <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-truck"></i> OPERATION & VEHICLE</a></li>
                </ul>

                <div class="tab-content">

                  <div class="tab-pane active" id="tab_1">
                    <div class="row" style="margin-top:10px">
                      <div class="col-xs-12"> <!--PRODUCT TABLE-->
                        <div class="box box-solid">
                          <div class="box-body">
                            <table id="pr_table" class="table  table-striped table-hover">
                              <thead>
                                <tr>
                                  <th>Product</th>     
                                  <th>SKU</th>                                                    
                                  <th>Category</th>
                                  <th>UOM</th>
                                  <th>Supplier</th>
                                  <th>Owner</th>  
                                  <th>Price</th>                                                                           
                                  <th>Qty</th>                                                                                                                      
                                  <th>Total</th>                        
                                </tr>
                                <tbody></tbody>
                              </thead>
                            </table>
                          </div><!-- /.box-body -->
                        </div><!-- /.box -->
                      </div><!-- /.col -->                      
                    </div>
                  </div><!-- /.tab-pane product-->

                  <div class="tab-pane" id="tab_2">

                    <div class="row">
                      <div class="col-sm-6 col-xs-12" style="margin-top:10px"> <!--OPERATION TABLE-->
                        <div class="box box-solid">
                          <div class="box-body">
                            <table id="op_table" class="table table-condensed table-striped table-hover">
                              <thead>
                                <tr>
                                  <th>Employee</th>
                                  <th>Cellphone</th>
                                  <th>Department</th>                        
                                  <th>Role</th>
                                </tr>
                                <tbody></tbody>
                              </thead>
                            </table>
                          </div><!-- /.box-body -->
                        </div><!-- /.box -->
                      </div><!-- /.col -->    

                      <div class="col-sm-6 col-xs-12" style="margin-top:10px"> <!--VEHICLE TABLE-->
                        <div class="box box-solid">
                          <div class="box-body">
                            <table id="ve_table" class="table table-condensed table-striped table-hover">
                              <thead>
                                <tr>
                                  <th>Plt/No</th>
                                  <th>Type</th>
                                  <th>Desc</th>                        
                                </tr>
                                <tbody></tbody>
                              </thead>
                            </table>
                          </div><!-- /.box-body -->
                        </div><!-- /.box -->
                      </div><!-- /.col -->


                    </div>

                  </div><!-- /.tab-operation|vehicle -->

                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
          </div> <!-- /.row -->



        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <?php include('../../mn/include/footer.php'); ?>
    </div><!-- ./wrapper -->
    <script src="controller3.js"></script>

    <!--MAIN CONTROLLER -->
    <script>
      $(document).ready(function () {

            load_top_labels( $('#doc_no').val() );

            $('[data-toggle="tooltip"]').tooltip();

            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-bottom-left",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "500",
              "hideDuration": "1000",
              "timeOut": "6000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "shoIMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }//.toastr config  


            //controller for this
              //tables
                  var pr_table = $('#pr_table').dataTable({
                      columnDefs: [
                     { type: 'formatted-num', targets: 0 }
                     ],       
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
                    "aaSorting": []
                  });  //Initialize the datatable department
                  var op_table = $('#op_table').dataTable({
                      columnDefs: [
                     { type: 'formatted-num', targets: 0 }
                     ],       
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
                    "aaSorting": [],"bPaginate": false
                  });  //Initialize the datatable department
                  var ve_table = $('#ve_table').dataTable({
                      columnDefs: [
                     { type: 'formatted-num', targets: 0 }
                     ],       
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
                    "aaSorting": [],"bPaginate": false
                  });  //Initialize the datatable department            
              //.tables  

              function load_top_labels(get_doc_no){

                //ajax
                $.ajax ({
                  type: "POST",
                  url: "serverside/view_top_labels.php",
                  dataType: 'json',      
                  data: 'doc_no='+get_doc_no,
                  cache: false,
                  success: function(s){
                    for(var i = 0; i < s.length; i++) { 
                      $('#lbl_doc_ref').text(s[i][0]);  $('#lbl_doc_date').text(s[i][1]);
                      $('#lbl_grand_total').text(comma(s[i][6])); $('#lbl_doc_status').text(s[i][2].toUpperCase());
                      $('#lbl_doc_gd').text(s[i][3]); $('#lbl_doc_source').text(s[i][4]); $('#lbl_doc_desti').text(s[i][5]);
                      $('#lbl_doc_type').text(s[i][7]);
                    }       
                    load_pr_table(get_doc_no);
                    load_op_table(get_doc_no);  
                    load_ve_table(get_doc_no);                
                  }  
                }); 
                  //ajax end    
              }

              function load_pr_table(get_doc_no){ 
                //ajax now
                $.ajax ({
                  type: "POST",
                  url: "serverside/view_pr_table.php",
                  dataType: 'json',      
                  data: 'doc_no='+get_doc_no,
                  cache: false,
                  success: function(s)
                  {
                    for(var i = 0; i < s.length; i++) 
                    {       
                      pr_table.fnAddData
                      ([
                        s[i][1],s[i][2],s[i][3],s[i][4],s[i][5],s[i][6],comma(s[i][7]),comma(s[i][8]),comma(s[i][9]),
                      ],false); 
                      pr_table.fnDraw();

                    }       
                  }  
                }); 
                //ajax end  
              } //.load pr_table

              function load_op_table(get_doc_no){ 
                //ajax now
                $.ajax ({
                  type: "POST",
                  url: "serverside/view_op_table.php",
                  dataType: 'json',      
                  data: 'doc_no='+get_doc_no,
                  cache: false,
                  success: function(s)
                  {
                    for(var i = 0; i < s.length; i++) 
                    {       
                      op_table.fnAddData
                      ([
                        s[i][1],s[i][2],s[i][3],s[i][4],
                      ],false); 
                      op_table.fnDraw();

                    }       
                  }  
                }); 
                //ajax end  
              } //.load op_table

              function load_ve_table(get_doc_no){ 
                //ajax now
                $.ajax ({
                  type: "POST",
                  url: "serverside/view_ve_table.php",
                  dataType: 'json',      
                  data: 'doc_no='+get_doc_no,
                  cache: false,
                  success: function(s)
                  {
                    for(var i = 0; i < s.length; i++) 
                    {       
                      ve_table.fnAddData
                      ([
                        s[i][1],s[i][2],s[i][3],
                      ],false); 
                      ve_table.fnDraw();

                    }       
                  }  
                }); 
                //ajax end  
              } //.load ve_table

              //additional functions


                function comma(val){
                  while (/(\d+)(\d{3})/.test(val.toString())){
                    val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                  }
                  return val;
                }
                function uncomma(x) {
                  var string1 = x;
                  for (y = 0; y < 12; y++) {
                    string1 = string1.replace(/\,/g, '');
                  }
                  return string1;
                } 


                function logout(){

                  var choice = confirm("Are you sure you want to Log-out?");
                  if(choice==true)
                  {
                    return true;
                  }
                  else
                    return false;
                }  
              //.addition funcitons                            
            //.controller for this



          });          
    </script>
  </body>
</html>
