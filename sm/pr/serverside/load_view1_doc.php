<?php
    include('../../../mn/include/connect.php');

    $lo_id = $_POST['lo_id'];

  $sql = "SELECT doc_id,doc_ref,doc_type,doc_date,doc_status,
coalesce((SELECT loc_name FROM location l WHERE l.loc_id = d.doc_source),d.doc_source) as source, 
(SELECT loc_name FROM location l WHERE l.loc_id = d.doc_desti) as desti,
SUM(dp.dp_qty*pv.pv_price) as tot
FROM document d, document_product dp, product_version pv
WHERE (dp.dp_doc_id = d.doc_id)
AND (dp.dp_prod_id = pv.pv_id)
AND (d.doc_status='shipped')
AND (d.doc_desti = ?)
GROUP BY d.doc_id
ORDER BY doc_status desc, doc_date desc";

  $q = $conn->prepare($sql);
  $q -> execute(array($lo_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['doc_id'],$fetch['doc_ref'],
      $fetch['doc_date'],ucwords($fetch['doc_status']),
      ucwords($fetch['source']),$fetch['desti'],$fetch['tot'],$fetch['doc_type']);          
  }         
$conn = null;             

echo json_encode($output);
?>    