<?php
    include('../../../mn/include/connect.php');

    $gd_name = $_POST['gd_name'];

  $sql = "SELECT l.loc_id, l.loc_name, l.loc_add, l.loc_type, c.co_name, 
  SUM(pv.pv_price * dp.dp_qty) as value
   FROM location l, company c, group_div g, document d, document_product dp, product_version pv
   WHERE (c.co_id=l.loc_co_id)
   AND (c.co_gr_id=g.gr_id)
   AND (g.gr_id = ? )
   AND (l.loc_status = 'active')
   AND (d.doc_id = dp.dp_doc_id)
   AND (d.doc_desti = l.loc_id)
   AND (l.loc_type = 'branch')
   AND (dp.dp_prod_id = pv.pv_id)
   AND (d.doc_status = 'shipped')
   GROUP BY l.loc_id
   ORDER BY l.loc_id ASC ";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_name));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['loc_id'],$fetch['loc_name'],
      $fetch['loc_add'],$fetch['co_name'],ucwords($fetch['loc_type']),$fetch['value'] );				 	
  }         
$conn = null;             

echo json_encode($output);
?>    