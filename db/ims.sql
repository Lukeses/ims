-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 21, 2016 at 09:44 AM
-- Server version: 5.6.33
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cloudipark_ims`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` varchar(25) NOT NULL,
  `cat_name` char(100) NOT NULL,
  `cat_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_status`) VALUES
('ct165832a3da8c61b', 'Smart Phone', 'active'),
('ct165832a3df0d831', 'Application', 'active'),
('ct165832a3e6350d6', 'Information System', 'active'),
('ct165832a3eeba8f2', 'Web Service', 'active'),
('ct165832a3f248055', 'Design', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `co_id` varchar(25) NOT NULL,
  `co_gr_id` varchar(25) NOT NULL,
  `co_name` varchar(100) NOT NULL,
  `co_desc` varchar(200) NOT NULL,
  `co_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`co_id`, `co_gr_id`, `co_name`, `co_desc`, `co_status`) VALUES
('co16583286fb295b5', 'gr16583286f254041', 'CLOUDIHOST', 'xyz', 'inactive'),
('co1658328735a00b9', 'gr16583286f254041', 'CLOUDIAPP', 'Application', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dep_id` varchar(25) NOT NULL,
  `dep_name` varchar(75) NOT NULL,
  `dep_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`, `dep_status`) VALUES
('dep165832a48fde80d', 'Operations', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `doc_id` varchar(25) NOT NULL,
  `doc_gr_id` varchar(25) NOT NULL,
  `doc_type` char(10) NOT NULL,
  `doc_source` varchar(25) NOT NULL,
  `doc_desti` varchar(25) NOT NULL,
  `doc_ref` varchar(50) NOT NULL,
  `doc_remarks` varchar(100) DEFAULT NULL,
  `doc_date` date NOT NULL,
  `doc_status` char(10) NOT NULL,
  `doc_fetch` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`doc_id`, `doc_gr_id`, `doc_type`, `doc_source`, `doc_desti`, `doc_ref`, `doc_remarks`, `doc_date`, `doc_status`, `doc_fetch`) VALUES
('doc165832a6aba859b', 'gr16583286f254041', 'GRN', 'virtual', 'lo165832a4d5a99d5', 'MOSES001', '', '2016-11-21', 'shipped', 'po165832a60b97fd4');

-- --------------------------------------------------------

--
-- Table structure for table `document_operation`
--

CREATE TABLE `document_operation` (
  `do_id` varchar(25) NOT NULL,
  `do_doc_id` varchar(25) NOT NULL,
  `do_emp_id` varchar(25) NOT NULL,
  `do_desc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_operation`
--

INSERT INTO `document_operation` (`do_id`, `do_doc_id`, `do_emp_id`, `do_desc`) VALUES
('do165832a6aba939d', 'doc165832a6aba859b', 'emp165832a6931094b', 'STOCKMAN'),
('do165832a6aba9662', 'doc165832a6aba859b', 'emp165832a687bdbcf', 'DRIVER');

-- --------------------------------------------------------

--
-- Table structure for table `document_product`
--

CREATE TABLE `document_product` (
  `dp_id` varchar(25) NOT NULL,
  `dp_doc_id` varchar(25) NOT NULL,
  `dp_prod_id` varchar(25) NOT NULL,
  `dp_qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_product`
--

INSERT INTO `document_product` (`dp_id`, `dp_doc_id`, `dp_prod_id`, `dp_qty`) VALUES
('dp165832a6aba9962', 'doc165832a6aba859b', 'pv165832a5d9bf811', 1);

-- --------------------------------------------------------

--
-- Table structure for table `document_vehicle`
--

CREATE TABLE `document_vehicle` (
  `dv_id` varchar(25) NOT NULL,
  `dv_doc_id` varchar(25) NOT NULL,
  `dv_ve_id` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_vehicle`
--

INSERT INTO `document_vehicle` (`dv_id`, `dv_doc_id`, `dv_ve_id`) VALUES
('dv165832a6aba8f9c', 'doc165832a6aba859b', 've165832a43209ca3');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` varchar(25) NOT NULL,
  `emp_name` varchar(75) NOT NULL,
  `emp_phone` varchar(15) NOT NULL,
  `emp_tel` varchar(15) NOT NULL,
  `emp_add` varchar(200) NOT NULL,
  `emp_pos_id` varchar(25) NOT NULL,
  `emp_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `emp_name`, `emp_phone`, `emp_tel`, `emp_add`, `emp_pos_id`, `emp_status`) VALUES
('emp165832a687bdbcf', 'Monet Baker', '', '', '', 'pos165832a49d46125', 'active'),
('emp165832a6931094b', 'John Doe', '', '', '', 'pos165832a4afa5dbf', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `group_div`
--

CREATE TABLE `group_div` (
  `gr_id` varchar(25) NOT NULL,
  `gr_name` varchar(100) NOT NULL,
  `gr_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_div`
--

INSERT INTO `group_div` (`gr_id`, `gr_name`, `gr_status`) VALUES
('gr16583286f254041', 'CLOUDIPARK', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `loc_id` varchar(25) NOT NULL,
  `loc_co_id` varchar(25) NOT NULL,
  `loc_name` varchar(100) NOT NULL,
  `loc_add` varchar(250) NOT NULL,
  `loc_type` char(15) NOT NULL,
  `loc_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`loc_id`, `loc_co_id`, `loc_name`, `loc_add`, `loc_type`, `loc_status`) VALUES
('lo165832a4d5a99d5', 'co1658328735a00b9', 'Chisaii Park Warehouse', 'St. John City', 'warehouse', 'active'),
('lo165832a4f5b7c5a', 'co1658328735a00b9', 'Clikha QC', 'Quezon City', 'warehouse', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `pos_id` varchar(25) NOT NULL,
  `pos_dep_id` varchar(25) NOT NULL,
  `pos_name` varchar(75) NOT NULL,
  `pos_optype` varchar(100) NOT NULL,
  `pos_desc` varchar(200) NOT NULL,
  `pos_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`pos_id`, `pos_dep_id`, `pos_name`, `pos_optype`, `pos_desc`, `pos_status`) VALUES
('pos165832a49d46125', 'dep165832a48fde80d', 'Driver', 'transport operation', '', 'active'),
('pos165832a4afa5dbf', 'dep165832a48fde80d', 'Administrator', 'logistic administration', '', 'active'),
('pos165832a4b783f40', 'dep165832a48fde80d', 'Helper', 'general', '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prod_id` varchar(25) NOT NULL,
  `prod_sku` varchar(25) NOT NULL,
  `prod_name` varchar(100) NOT NULL,
  `prod_cat_id` varchar(25) NOT NULL,
  `prod_var` varchar(100) NOT NULL,
  `prod_price` decimal(9,2) NOT NULL,
  `prod_sup_id` varchar(25) NOT NULL,
  `prod_co_id` varchar(25) NOT NULL,
  `prod_treshold` int(9) NOT NULL,
  `prod_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prod_id`, `prod_sku`, `prod_name`, `prod_cat_id`, `prod_var`, `prod_price`, `prod_sup_id`, `prod_co_id`, `prod_treshold`, `prod_status`) VALUES
('pr165832a5d9bf802', 'Food Tracker', 'Food Tracker', 'ct165832a3df0d831', 'Virtual', '75000.00', 'su1658328797ecc77', 'co1658328735a00b9', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `product_version`
--

CREATE TABLE `product_version` (
  `pv_id` varchar(25) NOT NULL,
  `pv_prod_id` varchar(25) NOT NULL,
  `pv_price` decimal(9,2) NOT NULL,
  `pv_date` datetime NOT NULL,
  `pv_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_version`
--

INSERT INTO `product_version` (`pv_id`, `pv_prod_id`, `pv_price`, `pv_date`, `pv_status`) VALUES
('pv165832a5d9bf811', 'pr165832a5d9bf802', '75000.00', '2016-11-21 15:44:25', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `po_id` varchar(25) NOT NULL,
  `po_co_id` varchar(25) NOT NULL,
  `po_ref` varchar(50) NOT NULL,
  `po_date` date NOT NULL,
  `po_terms` varchar(50) NOT NULL,
  `po_desc` varchar(300) NOT NULL,
  `po_remarks` varchar(100) NOT NULL,
  `po_pr_no` varchar(10) DEFAULT NULL,
  `po_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`po_id`, `po_co_id`, `po_ref`, `po_date`, `po_terms`, `po_desc`, `po_remarks`, `po_pr_no`, `po_status`) VALUES
('po165832a60b97fd4', 'co1658328735a00b9', 'C-1611-0001', '2016-11-21', 'P.D.C. 90 Days', 'CloudiShip', 'Test App on Manila Phlippines', '', 'shipped');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_product`
--

CREATE TABLE `purchase_product` (
  `pp_id` varchar(25) NOT NULL,
  `pp_po_id` varchar(25) NOT NULL,
  `pp_prod_id` varchar(25) NOT NULL,
  `pp_qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_product`
--

INSERT INTO `purchase_product` (`pp_id`, `pp_po_id`, `pp_prod_id`, `pp_qty`) VALUES
('pp165832a60b9896e', 'po165832a60b97fd4', 'pv165832a5d9bf811', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `sup_id` varchar(25) NOT NULL,
  `sup_name` varchar(100) NOT NULL,
  `sup_desc` varchar(200) NOT NULL,
  `sup_tel` varchar(15) NOT NULL,
  `sup_phone` varchar(15) NOT NULL,
  `sup_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`sup_id`, `sup_name`, `sup_desc`, `sup_tel`, `sup_phone`, `sup_status`) VALUES
('su165832876ee8a4e', 'Juan Dela Cruz', '', '', '', 'active'),
('su165832877b858d9', 'Fibisco', '', '', '', 'active'),
('su165832878b548d9', 'Rebisco', '', '', '', 'active'),
('su1658328793269a5', 'Leonardos', '', '', '', 'active'),
('su1658328797ecc77', 'Castaniedas', '', '', '', 'active'),
('su16583287a223a17', 'Vape Nation', '', '', '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `trail`
--

CREATE TABLE `trail` (
  `trail_id` varchar(25) NOT NULL,
  `module_type` varchar(25) NOT NULL,
  `module` varchar(50) NOT NULL,
  `action` varchar(25) NOT NULL,
  `trail_desc` varchar(300) NOT NULL,
  `trail_date` datetime NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trail`
--

INSERT INTO `trail` (`trail_id`, `module_type`, `module`, `action`, `trail_desc`, `trail_date`, `user_name`, `user_type`) VALUES
('at16583286d0966ea', 'Session', 'Log', 'Sign-Out', 'Users\'s Log-Out Session', '2016-11-21 06:32:00', 'moses', 'admin'),
('at16583286d3aa15a', 'Session', 'Log', 'Sign-In', 'Users\'s Log-in Session', '2016-11-21 06:32:03', 'moses', 'admin'),
('at16583286f254ee9', 'Maintenance', 'Group', 'CREATE', 'New: ID:gr16583286f254041, Name:cloudipark', '2016-11-21 06:32:34', 'moses', 'admin'),
('at16583286fb29a03', 'Maintenance', 'Company', 'CREATE', 'New: ID:co16583286fb295b5, Name:cloudihost, Group ID:gr16583286f254041, Description:xyz', '2016-11-21 06:32:43', 'moses', 'admin'),
('at165832872a3d117', 'Maintenance', 'Company', 'REMOVE', 'Edited: ID:co16583286fb295b5', '2016-11-21 06:33:30', 'moses', 'admin'),
('at1658328735a08e4', 'Maintenance', 'Company', 'CREATE', 'New: ID:co1658328735a00b9, Name:cloudiapp, Group ID:gr16583286f254041, Description:Application', '2016-11-21 06:33:41', 'moses', 'admin'),
('at165832873b26f58', 'Session', 'Group', 'Select', 'Selected: CLOUDIPARK, ID:gr16583286f254041', '2016-11-21 06:33:47', 'moses', 'admin'),
('at165832876ee8d1b', 'Maintenance', 'Supplier', 'CREATE', 'New: ID:su165832876ee8a4e, Name:Juan Dela Cruz, Phone:, Tel:, Description:', '2016-11-21 06:34:38', 'moses', 'admin'),
('at165832877b85b68', 'Maintenance', 'Supplier', 'CREATE', 'New: ID:su165832877b858d9, Name:Fibisco, Phone:, Tel:, Description:', '2016-11-21 06:34:51', 'moses', 'admin'),
('at165832878b54c36', 'Maintenance', 'Supplier', 'CREATE', 'New: ID:su165832878b548d9, Name:Rebisco, Phone:, Tel:, Description:', '2016-11-21 06:35:07', 'moses', 'admin'),
('at165832879326d42', 'Maintenance', 'Supplier', 'CREATE', 'New: ID:su1658328793269a5, Name:Leonardos, Phone:, Tel:, Description:', '2016-11-21 06:35:15', 'moses', 'admin'),
('at1658328797ed9c4', 'Maintenance', 'Supplier', 'CREATE', 'New: ID:su1658328797ecc77, Name:Castaniedas, Phone:, Tel:, Description:', '2016-11-21 06:35:19', 'moses', 'admin'),
('at16583287a223cea', 'Maintenance', 'Supplier', 'CREATE', 'New: ID:su16583287a223a17, Name:Vape Nation, Phone:, Tel:, Description:', '2016-11-21 06:35:30', 'moses', 'admin'),
('at16583293ce70708', 'Session', 'Log', 'Sign-In', 'Users\'s Log-in Session', '2016-11-21 07:27:26', 'moses', 'admin'),
('at165832a376ada10', 'Session', 'Group', 'Select', 'Selected: CLOUDIPARK, ID:gr16583286f254041', '2016-11-21 08:34:14', 'moses', 'admin'),
('at165832a381c42a3', 'Session', 'Log', 'Sign-Out', 'Users\'s Log-Out Session', '2016-11-21 08:34:25', 'moses', 'admin'),
('at165832a384e9d1c', 'Session', 'Log', 'Sign-In', 'Users\'s Log-in Session', '2016-11-21 08:34:28', 'moses', 'admin'),
('at165832a38bae517', 'Session', 'Group', 'Select', 'Selected: CLOUDIPARK, ID:gr16583286f254041', '2016-11-21 08:34:35', 'moses', 'admin'),
('at165832a3da8c879', 'Maintenance', 'Category', 'CREATE', 'New: ID:ct165832a3da8c61b, Name:Smart Phone', '2016-11-21 08:35:54', 'moses', 'admin'),
('at165832a3df0ddb2', 'Maintenance', 'Category', 'CREATE', 'New: ID:ct165832a3df0d831, Name:Application', '2016-11-21 08:35:59', 'moses', 'admin'),
('at165832a3e6354fd', 'Maintenance', 'Category', 'CREATE', 'New: ID:ct165832a3e6350d6, Name:Information System', '2016-11-21 08:36:06', 'moses', 'admin'),
('at165832a3eebabbb', 'Maintenance', 'Category', 'CREATE', 'New: ID:ct165832a3eeba8f2, Name:Web Service', '2016-11-21 08:36:14', 'moses', 'admin'),
('at165832a3f2483b5', 'Maintenance', 'Category', 'CREATE', 'New: ID:ct165832a3f248055, Name:Design', '2016-11-21 08:36:18', 'moses', 'admin'),
('at165832a405284fa', 'Maintenance', 'Vehicle Type', 'CREATE', 'New: ID:vt165832a40528290, Type Name:SUV', '2016-11-21 08:36:37', 'moses', 'admin'),
('at165832a40a34cdd', 'Maintenance', 'Vehicle Type', 'CREATE', 'New: ID:vt165832a40a349e5, Type Name:Sedan', '2016-11-21 08:36:42', 'moses', 'admin'),
('at165832a40e47703', 'Maintenance', 'Vehicle Type', 'CREATE', 'New: ID:vt165832a40e4743a, Type Name:Pick-up', '2016-11-21 08:36:46', 'moses', 'admin'),
('at165832a41164775', 'Maintenance', 'Vehicle Type', 'CREATE', 'New: ID:vt165832a4116452f, Type Name:Jeep', '2016-11-21 08:36:49', 'moses', 'admin'),
('at165832a4144f122', 'Maintenance', 'Vehicle Type', 'CREATE', 'New: ID:vt165832a4144edf3, Type Name:Truck', '2016-11-21 08:36:52', 'moses', 'admin'),
('at165832a4320a14d', 'Maintenance', 'Vehicle', 'CREATE', 'Updated: ID:ve165832a43209ca3, Type ID:Vt165832a40528290, Plt/No:PON-369, Description:Mitsubishi Montero Sports', '2016-11-21 08:37:22', 'moses', 'admin'),
('at165832a44739d56', 'Maintenance', 'Vehicle', 'CREATE', 'Updated: ID:ve165832a44739340, Type ID:Vt165832a40e4743a, Plt/No:ONP-128, Description:Mitsubishi Strada', '2016-11-21 08:37:43', 'moses', 'admin'),
('at165832a454ce758', 'Maintenance', 'Vehicle', 'CREATE', 'Updated: ID:ve165832a454ce2ba, Type ID:Vt165832a40528290, Plt/No:FRD-192, Description:Ford Evereset', '2016-11-21 08:37:56', 'moses', 'admin'),
('at165832a46398dc9', 'Maintenance', 'Vehicle', 'CREATE', 'Updated: ID:ve165832a4639847f, Type ID:Vt165832a40a349e5, Plt/No:TSL-111, Description:Tesla SX 2', '2016-11-21 08:38:11', 'moses', 'admin'),
('at165832a476c0789', 'Maintenance', 'Vehicle', 'CREATE', 'Updated: ID:ve165832a476bfdf6, Type ID:Vt165832a4144edf3, Plt/No:2TXS-12, Description:Truck De Bale', '2016-11-21 08:38:30', 'moses', 'admin'),
('at165832a48fdedc2', 'Maintenance', 'Department', 'CREATE', 'New: ID:dep165832a48fde80d, Name:Operations', '2016-11-21 08:38:55', 'moses', 'admin'),
('at165832a49d463db', 'Maintenance', 'Position', 'CREATE', 'New: ID:pos165832a49d46125, Name:Driver, Department ID:, Operation Type:transport operation, Description:', '2016-11-21 08:39:09', 'moses', 'admin'),
('at165832a4afa60b1', 'Maintenance', 'Position', 'CREATE', 'New: ID:pos165832a4afa5dbf, Name:Administrator, Department ID:, Operation Type:logistic administration, Description:', '2016-11-21 08:39:27', 'moses', 'admin'),
('at165832a4b78498b', 'Maintenance', 'Position', 'CREATE', 'New: ID:pos165832a4b783f40, Name:Helper, Department ID:, Operation Type:general, Description:', '2016-11-21 08:39:35', 'moses', 'admin'),
('at165832a4d5a9d05', 'Maintenance', 'Location', 'CREATE', 'New: ID:lo165832a4d5a99d5, Name:Oohki Park, Company:co1658328735a00b9, Type:warehouse, Address:St. John City', '2016-11-21 08:40:05', 'moses', 'admin'),
('at165832a4de85e1d', 'Maintenance', 'Location', 'EDIT', 'Updated: ID:lo165832a4d5a99d5, Name:Oohki Park Warehouse, Company:co1658328735a00b9, Type:warehouse, Address:St. John City', '2016-11-21 08:40:14', 'moses', 'admin'),
('at165832a4e374eb3', 'Maintenance', 'Location', 'EDIT', 'Updated: ID:lo165832a4d5a99d5, Name:Chisaii Park Warehouse, Company:co1658328735a00b9, Type:warehouse, Address:St. John City', '2016-11-21 08:40:19', 'moses', 'admin'),
('at165832a4f5b861f', 'Maintenance', 'Location', 'CREATE', 'New: ID:lo165832a4f5b7c5a, Name:Clikha QC, Company:co1658328735a00b9, Type:warehouse, Address:Quezon City', '2016-11-21 08:40:37', 'moses', 'admin'),
('at165832a5d9c0238', 'Maintenance', 'Product', 'CREATE', 'New: ID:pr165832a5d9bf802, Name:Food Tracker, SKU:Food Tracker, Variant:Virtual, Treshold:0, Price:75000', '2016-11-21 08:44:25', 'moses', 'admin'),
('at165832a5d9c0545', 'Maintenance', 'Product Version', 'CREATE', 'New: ID:pv165832a5d9bf811, Product ID:pr165832a5d9bf802, Version Price:75000', '2016-11-21 08:44:25', 'moses', 'admin'),
('at165832a60b985d4', 'Transaction', 'Purchase Order', 'CREATE', 'New: PO:po165832a60b97fd4, Po-Ref:C-1611-0001, Date:2016-11-21, Company:co1658328735a00b9, Status:pending', '2016-11-21 08:45:15', 'moses', 'admin'),
('at165832a687bde8c', 'Maintenance', 'Employee', 'CREATE', 'New: ID:emp165832a687bdbcf, Name:Monet Baker, Phone:, Tel:, Position ID:pos165832a49d46125 Address:', '2016-11-21 08:47:19', 'moses', 'admin'),
('at165832a69310bdd', 'Maintenance', 'Employee', 'CREATE', 'New: ID:emp165832a6931094b, Name:John Doe, Phone:, Tel:, Position ID:pos165832a4afa5dbf Address:', '2016-11-21 08:47:31', 'moses', 'admin'),
('at165832a6aba8a92', 'Transaction', 'GRN', 'CREATE', 'New: ID:doc165832a6aba859b, Ref:MOSES001, Date:2016-11-21, Destination ID:lo165832a4d5a99d5, Status:pending', '2016-11-21 08:47:55', 'moses', 'admin'),
('at165832a6b86aefd', 'Transaction', 'GRN', 'EDIT', 'Updated to Shipped: ID:doc165832a6aba859b', '2016-11-21 08:48:08', 'moses', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `u_name` varchar(25) NOT NULL,
  `u_pass` varchar(50) NOT NULL,
  `u_type` varchar(15) NOT NULL,
  `u_status` varchar(10) NOT NULL,
  `u_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`u_name`, `u_pass`, `u_type`, `u_status`, `u_desc`) VALUES
('admin', 'admin', 'admin', 'active', 'Administrator'),
('moses', '594aa0a9de0c75cd4d4037b6b65c683e', 'admin', 'active', ''),
('user1', '24c9e15e52afc47c225b757e7bee1f9d', 'user', 'active', ''),
('user2', '7e58d63b60197ceb55a1c487989a3720', 'user', 'active', '');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `ve_id` varchar(25) NOT NULL,
  `ve_no` varchar(25) NOT NULL,
  `ve_vt_id` varchar(25) NOT NULL,
  `ve_desc` varchar(200) NOT NULL,
  `ve_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`ve_id`, `ve_no`, `ve_vt_id`, `ve_desc`, `ve_status`) VALUES
('ve165832a43209ca3', 'PON-369', 'Vt165832a40528290', 'Mitsubishi Montero Sports', 'active'),
('ve165832a44739340', 'ONP-128', 'Vt165832a40e4743a', 'Mitsubishi Strada', 'active'),
('ve165832a454ce2ba', 'FRD-192', 'Vt165832a40528290', 'Ford Evereset', 'active'),
('ve165832a4639847f', 'TSL-111', 'Vt165832a40a349e5', 'Tesla SX 2', 'active'),
('ve165832a476bfdf6', '2TXS-12', 'Vt165832a4144edf3', 'Truck De Bale', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `vt_id` varchar(25) NOT NULL,
  `vt_name` varchar(100) NOT NULL,
  `vt_status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`vt_id`, `vt_name`, `vt_status`) VALUES
('vt165832a40528290', 'SUV', 'active'),
('vt165832a40a349e5', 'Sedan', 'active'),
('vt165832a40e4743a', 'Pick-up', 'active'),
('vt165832a4116452f', 'Jeep', 'active'),
('vt165832a4144edf3', 'Truck', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`doc_id`),
  ADD KEY `doc_ref` (`doc_ref`);

--
-- Indexes for table `document_operation`
--
ALTER TABLE `document_operation`
  ADD PRIMARY KEY (`do_id`);

--
-- Indexes for table `document_product`
--
ALTER TABLE `document_product`
  ADD PRIMARY KEY (`dp_id`);

--
-- Indexes for table `document_vehicle`
--
ALTER TABLE `document_vehicle`
  ADD PRIMARY KEY (`dv_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `group_div`
--
ALTER TABLE `group_div`
  ADD PRIMARY KEY (`gr_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`loc_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `product_version`
--
ALTER TABLE `product_version`
  ADD PRIMARY KEY (`pv_id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`po_id`),
  ADD KEY `doc_ref` (`po_ref`);

--
-- Indexes for table `purchase_product`
--
ALTER TABLE `purchase_product`
  ADD PRIMARY KEY (`pp_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `trail`
--
ALTER TABLE `trail`
  ADD PRIMARY KEY (`trail_id`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`u_name`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`ve_id`),
  ADD KEY `ve_no` (`ve_no`);

--
-- Indexes for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`vt_id`);
