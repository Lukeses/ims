<?php
   include('../../include/connect.php');

$idKey = $_POST['idKey'];

  $sql = "SELECT emp_name,emp_phone,emp_tel,pos_id,dep_id,emp_add
  FROM employee e, position p, department d 
  WHERE (e.emp_pos_id = p.pos_id) 
  AND (p.pos_dep_id = d.dep_id) 
  AND (e.emp_id = ?) ";

$q = $conn->prepare($sql);
$q -> execute(array($idKey));
$browse = $q -> fetchAll();
foreach($browse as $fetch)
{
  $output[] = array ($fetch['emp_name'],$fetch['emp_phone'],$fetch['emp_tel'],
  	$fetch['dep_id'],$fetch['pos_id'],$fetch['emp_add']);         
}                      

echo json_encode($output);
$conn = null;
?>