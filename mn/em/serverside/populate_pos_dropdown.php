<?php
    include('../../include/connect.php');

    $dep_id = $_POST['dep_id'];

  $sql = "SELECT pos_id,pos_name
  FROM position WHERE pos_status='active' 
  AND pos_dep_id = ?
  ORDER BY pos_name ASC";

  $q = $conn->prepare($sql);
  $q -> execute(array($dep_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['pos_id'],$fetch['pos_name']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    