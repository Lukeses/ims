<?php
    include('../../include/connect.php');


  $sql = "SELECT emp_id,emp_name,emp_phone,emp_tel,pos_name,dep_name 
  FROM employee e, position p, department d 
  WHERE (e.emp_pos_id = p.pos_id) 
  AND (p.pos_dep_id = d.dep_id) 
  AND (e.emp_status = 'active') 
  ORDER BY e.emp_name ASC ";

  $q = $conn->prepare($sql);
  $q -> execute();
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['emp_id'],$fetch['emp_name'],$fetch['emp_phone'],$fetch['emp_tel'],
      $fetch['pos_name'],$fetch['dep_name']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    