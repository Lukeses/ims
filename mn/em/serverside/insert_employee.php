
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../include/connect.php');  
    include('../../include/log.php');  

                                     //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('emp'.$year);   

	$emp_name = ucwords(trim($_POST['emp_name']));
	$emp_phone = $_POST['emp_phone'];
	$emp_tel = $_POST['emp_tel'];
	$emp_pos = $_POST['emp_pos'];
	$emp_add = ucwords(trim($_POST['emp_add']));

                                                                 // INSERT DATA TO DATABASE
$sql = "INSERT INTO employee VALUES(?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);
$q -> execute(array($id,$emp_name,$emp_phone,$emp_tel,$emp_add,$emp_pos,'active'));

$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Employee', 'CREATE', 'New: ID:'.$id. ', Name:'.$emp_name.', Phone:'.$emp_phone.', Tel:'.$emp_tel.', Position ID:'.$emp_pos.' Address:'.$emp_add , date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));


$conn = null;
echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
