
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "500",
    "hideDuration": "1000",
    "timeOut": "6000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }//.toastr config  
	});  



	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	   

//Employee

	$('#btn_em_add').click(function(){
		clear_em_form();
		$('#btn_em_save').val('insert');
		$('#btn_em_save').prop('disabled',false);
    populate_dep_dropdown();
    $('#emp_pos').empty();    
    $('#emp_pos').append('<option selected="selected"></option>');        
		$('#em_modal').modal('show');
	})

	//tables
	    var em_table = $('#em_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [6,7] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_em_table(){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_em_table.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        em_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 

          em_table.fnAddData
          ([
            s[i][0],s[i][1],s[i][2],s[i][3],s[i][4],s[i][5],
            '<button data-toggle="tooltip" onclick="em_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="em_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ],false); 
          em_table.fnDraw();

        }       
      }  
    }); 
    $('#loading').modal('hide');
    
    //ajax end  
  } //.load em_table

  load_em_table();
	
  function clear_em_form(){
  	$('#emp_name').val('');
  	$('#emp_phone').val('');
  	$('#emp_tel').val('');
  	$('#emp_dep').val('');
  	$('#emp_pos').val('');
  	$('#emp_add').val(''); 
  	$('#emp_nameDiv').removeClass('has-error'); 	
  	$('#emp_phoneDiv').removeClass('has-error'); 	
  	$('#emp_telDiv').removeClass('has-error'); 	
  	$('#emp_depDiv').removeClass('has-error'); 	
  	$('#emp_posDiv').removeClass('has-error'); 	
  	$('#emp_addDiv').removeClass('has-error'); 	  	
  }

  function validate_em_form(){
    var err = true;

    if($('#emp_name').val()==''){
      err = false ;
      $('#emp_nameDiv').addClass('has-error');
    }
    else
      $('#emp_nameDiv').removeClass('has-error');   

    if($('#emp_dep').val()==''){
      err = false ;
      $('#emp_depDiv').addClass('has-error');
    }
    else
      $('#emp_depDiv').removeClass('has-error'); 


    if($('#emp_pos').val()==''){
      err = false ;
      $('#emp_posDiv').addClass('has-error');
    }
    else
      $('#emp_posDiv').removeClass('has-error'); 

    return err; 
  }  	

  function populate_dep_dropdown(){
    //ajax now populate dropdown departments
    $.ajax ({
      type: "POST",
      url: "serverside/populate_dep_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#emp_dep').empty();
        $('#emp_dep').append('<option selected="selected"></option>');              
        for(var i = 0; i < s.length; i++) { 
          $('#emp_dep').append('<option id="optdep'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end      
  }

  function populate_pos_dropdown(dep_id,get_selected){
    //ajax now populate dropdown departments
      $('#emp_pos').empty();    
      $('#emp_pos').append('<option selected="selected"></option>');        
    if(dep_id==''){}
    else{    
      $.ajax ({
        type: "POST",
        url: "serverside/populate_pos_dropdown.php",
        dataType: 'json', 
        data: 'dep_id='+dep_id,   
        cache: false,
        success: function(s)
        {
          $('#emp_pos').empty();              
          for(var i = 0; i < s.length; i++) { 
            $('#emp_pos').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
          }     
          $('#opt'+get_selected).prop('selected',true);
        }  
      }); 
      //ajax end           
    }    
  }

  function em_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  function em_row_update(get_id){
    clear_em_form();
    populate_dep_dropdown();    
    $('#btn_em_save').prop('disabled',false);    
    $('#btn_em_save').val(get_id);
      $.ajax({
        type: 'POST',
        url: 'serverside/fetch_employee.php',
        dataType: 'json',
        data: 'idKey='+get_id,
        success: function(s) {
        for(var i = 0; i < s.length; i++) {
          $('#emp_name').val(s[i][0]);
          $('#emp_phone').val(s[i][1]);
          $('#emp_tel').val(s[i][2]);
          $('#optdep'+s[i][3]).prop('selected',true);
          populate_pos_dropdown(s[i][3],s[i][4]);
          $('#emp_add').val(s[i][5]);
          $('#em_modal').modal('show');
        }
      }
    });
  }


  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/delete_employee.php',
      data: 'idKey='+$(this).val(),
      success: function(s) {          
        if(s==0){
          load_em_table();
          $('#delModal').modal('hide');     
          toastr.error('Record Deleted');
        }
        else if(s==1){
          $('#delModal').modal('hide');     
          toastr.warning('Error: No Connection');          
        }               
      }
    });  
  });

  $('#btn_em_save').click(function(){
  	if(validate_em_form() == false){}
  	else{
    $(this).prop('disabled',true);      
  		var emp_name = $('#emp_name').val();
  		var emp_phone = $('#emp_phone').val();
  		var emp_tel = $('#emp_tel').val();
  		var emp_pos = $('#emp_pos').val();
  		var emp_add = $('#emp_add').val();
  		var dataString = 'emp_name='+emp_name+'&emp_phone='+emp_phone+'&emp_tel='+emp_tel+'&emp_pos='+emp_pos+'&emp_add='+emp_add;


      if($(this).val()=='insert'){ //will insert
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_employee.php",
          data: dataString, 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#em_modal').modal('hide');
              clear_em_form();
              load_em_table();
              toastr.success(emp_name,'Employee Added:');              
            }//.if
            else{
              $('#em_modal').modal('hide');
              clear_em_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }

      else{ //will update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_employee.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#em_modal').modal('hide');
              clear_em_form();
              load_em_table();
              toastr.info(emp_name,'Employee Updated:');              
            }//.if
            else{
              $('#em_modal').modal('hide');
              clear_em_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }


  	}//.else
  })//.btn_em_save


//. employee	   