
//doc
$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();

  toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "500",
  "hideDuration": "1000",
  "timeOut": "6000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
  }//.toastr config

}); 

function logout()
{
  var choice = confirm("Are you sure you want to Log-out?");
  if(choice==true)
  {
    return true;
  }
  else
    return false;
} //.logout()

//tables
    var dep_table = $('#dep_table').dataTable({
        columnDefs: [
       { type: 'formatted-num', targets: 0 }
       ],       
      "aoColumnDefs": [ { "bSortable": false, "aTargets": [1,2] } ],
      "aaSorting": []
    });  //Initialize the datatable department

    var pos_table = $('#pos_table').dataTable({
        columnDefs: [
       { type: 'formatted-num', targets: 0 }
       ],       
      "aoColumnDefs": [ { "bSortable": false, "aTargets": [4,5] } ],
      "aaSorting": []
    });  //Initialize the datatable position
//.tables    


// DEPARTMENT
  function clear_dep_form(){
      $('#depname').val('');
      $('#depnameDiv').removeClass('has-error');
  }

  function populate_dep_table(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_dep_table.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        dep_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 

          dep_table.fnAddData
          ([
            s[i][1],
            '<button data-toggle="tooltip" onclick="dep_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="dep_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ]); 

        }       
      }  
    }); 
    //ajax end  
  } // 

  $('#btn_add_dep').click(function(){    
  	clear_dep_form();
    $('#btn_dep_save').prop('disabled',false);
    $('#btn_dep_save').val('insert');
  	$('#dep_modal').modal('show');
  })

  function dep_row_update(get_id){
    clear_dep_form();
    $('#btn_dep_save').prop('disabled',false);    
    $('#btn_dep_save').val(get_id);

    $.ajax({
      type: 'POST',
      url: 'serverside/fetch_dep_table.php',
      dataType: 'json',
      data: 'idKey='+get_id,
      success: function(s) {
      for(var i = 0; i < s.length; i++) {
        $('#depname').val(s[i][0]);
        $('#dep_modal').modal('show');
      }

      }
    });
  }

  function dep_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  $('#btnDel').click(function(){
    if(($(this).val().slice(0, 3)) == 'dep'  ){
      $.ajax({
        type: 'POST',
        url: 'serverside/delete_dep_table.php',
        data: 'idKey='+$(this).val(),
        success: function(s) {          
          if(s==0){
            populate_dep_table();
            $('#delModal').modal('hide');     
            toastr.error('Record Deleted');
          }
          else if(s==1){
            $('#delModal').modal('hide');     
            toastr.warning('Error: No Connection');          
          }        
          else if(s==2){
            $('#delModal').modal('hide');     
            toastr.warning('Warning: Department is in use by Positions');          
          }        

        }
      });  
    }
    else{
      $.ajax({
        type: 'POST',
        url: 'serverside/delete_pos_table.php',
        data: 'idKey='+$(this).val(),
        success: function(s) {          
          if(s==0){
            populate_pos_table();
            $('#delModal').modal('hide');     
            toastr.error('Record Deleted');
          }
          else if(s==1){
            $('#delModal').modal('hide');     
            toastr.warning('Error: No Connection');          
          }            
          else if(s==2){
            $('#delModal').modal('hide');     
            toastr.warning('Warning: Position is in use by Employee');          
          }            

        }
      });        
    } 
  });

  $('#btn_dep_save').click(function(){
    if(validate_dep_form()==false){}
    else if(validate_dep_form()==true){
        //var for sending
      $(this).prop('disabled',true);        
      var dep_name = $('#depname').val();

      if($(this).val()=='insert'){ // will Add
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_dep_table.php",
          data: 'dep_name='+dep_name, 
          cache: false,
          success: function(s){
            if(s==0){
              $('#dep_modal').modal('hide');
              clear_dep_form();
              populate_dep_table();
              toastr.success(dep_name,'Department Added:');              
            }//.if
            else{
              $('#dep_modal').modal('hide');
              clear_dep_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end          
      }
      else{ // will Update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_dep_table.php",
          data: 'dep_name='+dep_name+'&idKey='+$(this).val(), 
          cache: false,
          success: function(s){
            if(s==0){
              $('#dep_modal').modal('hide');
              clear_dep_form();
              populate_dep_table();
              populate_pos_table();
              toastr.info(dep_name,'Department Updated:');
            }
            else if(s==1){
              $('#dep_modal').modal('hide');
              clear_dep_form();
              toastr.warning('Error: No Connection');         
            }
  
          }  
        }); 
        //ajax end    
      }
    }
  })

  
  function validate_dep_form(){
    var err = true;

    if($('#depname').val()==''){
      err = false ;
      $('#depnameDiv').addClass('has-error');
    }
    else
      $('#depnameDiv').removeClass('has-error');   
        
    return err; 
  }  

  populate_dep_table(); //load table
// .end Department


//POSITION
  function clear_pos_form(){
      $('#posname').val('');
      $('#pos_optype').val('');
      $('#pos_desc').val('');
      $('#posnameDiv').removeClass('has-error');
      $('#posdepDiv').removeClass('has-error');
      $('#pos_optypeDiv').removeClass('has-error');      
  }

  $('#btn_add_pos').click(function(){    
    clear_pos_form();
    $('#btn_pos_save').prop('disabled',false);
    $('#btn_pos_save').val('insert');
    populate_pos_dropdown();
    $('#pos_modal').modal('show');
  })

  function populate_pos_dropdown(){
    //ajax now populate dropdown departments
    $.ajax ({
      type: "POST",
      url: "serverside/populate_dep_table.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#posdep').empty();
        for(var i = 0; i < s.length; i++) { 
          $('#posdep').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end      
  }

  function validate_pos_form(){
    var err = true;

    if($('#posname').val()==''){
      err = false ;
      $('#posnameDiv').addClass('has-error');
    }
    else
      $('#posnameDiv').removeClass('has-error');   

    if($('#posdep').val()==''){
      err = false ;
      $('#posdepDiv').addClass('has-error');
    }
    else
      $('#posdepDiv').removeClass('has-error');   

    if($('#pos_optype').val()==''){
      err = false ;
      $('#pos_optypeDiv').addClass('has-error');
    }
    else
      $('#pos_optypeDiv').removeClass('has-error');      

    return err; 
  }  

  function populate_pos_table(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_pos_table.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        pos_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 
          pos_table.fnAddData
          ([
            s[i][1],s[i][2],s[i][3],s[i][4],
            '<button data-toggle="tooltip" onclick="pos_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="pos_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ]); 

        }       
      }  
    }); 
    //ajax end  
  } // 

  populate_pos_table(); //populate position Table


  function pos_row_update(get_id){
    clear_pos_form();
    populate_pos_dropdown();
    $('#btn_pos_save').prop('disabled',false);    
    $('#btn_pos_save').val(get_id);

    $.ajax({
      type: 'POST',
      url: 'serverside/fetch_pos_table.php',
      dataType: 'json',
      data: 'idKey='+get_id,
      success: function(s) {
        for(var i = 0; i < s.length; i++) {        
          $('#posname').val(s[i][1]);
          $('#opt'+s[i][0]).prop('selected',true);
          $('#pos_optype').val(s[i][3]);
          $('#pos_desc').val(s[i][4]);
          $('#pos_modal').modal('show');
        }

      }
    });
  }

  $('#btn_pos_save').click(function(){
    if(validate_pos_form()==false){}
    else if(validate_pos_form()==true){
        //var for sending
      $(this).prop('disabled',true);        
      var posname = $('#posname').val();
      var posdep = $('#posdep').val();
      var pos_optype = $('#pos_optype').val();
      var pos_desc = $('#pos_desc').val();
      var dataString = 'pos_name='+posname+'&pos_dep='+posdep+ '&pos_optype='+pos_optype+'&pos_desc='+pos_desc;
      if($(this).val()=='insert'){ // will Add
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_pos_table.php",
          data: dataString, 
          cache: false,
          success: function(s){
            if(s==0){
              $('#pos_modal').modal('hide');
              clear_pos_form();
              populate_pos_table();
              toastr.success(posname,'Position Added:');              
            }//.if
            else{
              $('#pos_modal').modal('hide');
              clear_pos_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end          
      } 
      else{ // will Update
        //ajax now        
        $.ajax ({
          type: "POST",
          url: "serverside/update_pos_table.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false,
          success: function(s){
            if(s==0){
              populate_pos_table();              
              $('#pos_modal').modal('hide');
              clear_pos_form();
              toastr.info(posname,'Position Updated:');
            }
            else if(s==1){
              $('pos_modal').modal('hide');
              clear_pos_form();
              toastr.warning('Error: No Connection');         
            }
  
          }  
        }); 
        //ajax end                      
      } //.else
    }
  })

  function pos_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }



//.end Position