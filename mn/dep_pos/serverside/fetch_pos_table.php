<?php
   include('../../include/connect.php');

$idKey = $_POST['idKey'];

$sql = "SELECT pos_dep_id,pos_name,dep_name,pos_optype,pos_desc
   FROM department d, position p
  WHERE p.pos_dep_id = d.dep_id
  AND (p.pos_id = ?)";

$q = $conn->prepare($sql);
$q -> execute(array($idKey));
$browse = $q -> fetchAll();
foreach($browse as $fetch)
{
  $output[] = array ($fetch['pos_dep_id'],$fetch['pos_name'],$fetch['dep_name'],
  	$fetch['pos_optype'],$fetch['pos_desc']);         
}                      

echo json_encode($output);
$conn = null;
?>