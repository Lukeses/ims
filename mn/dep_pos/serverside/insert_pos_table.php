
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../include/connect.php');  
    include('../../include/log.php');  

                                     //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('pos'.$year);   

    $pos_name = ucwords(trim($_POST['pos_name']));
    $pos_dep = $_POST['pos_dep'];
    $pos_optype = $_POST['pos_optype'];
    $pos_desc = ucwords(trim($_POST['pos_desc']));


                                                                 // INSERT DATA TO DATABASE
$sql = "INSERT INTO position VALUES(?,?,?,?,?,?)";
$q = $conn -> prepare($sql);
$q -> execute(array($id,$pos_dep,$pos_name,$pos_optype,$pos_desc,'active'));

$year = date('Y');
$year = substr($year,2,3);
$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Position', 'CREATE', 'New: ID:'.$id. ', Name:'.$pos_name.', Department ID:'.$pos_desc.', Operation Type:'.$pos_optype.', Description:'.$pos_desc, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$conn = null;
echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
