<?php
    include('../../include/connect.php');


  $sql = "SELECT pos_id,pos_name,dep_name, pos_optype, pos_desc
   FROM department d, position p
  WHERE p.pos_dep_id = d.dep_id
  AND (p.pos_status = 'active')
  ORDER BY pos_name ASC";

  $q = $conn->prepare($sql);
  $q -> execute();
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['pos_id'],$fetch['pos_name'],$fetch['dep_name'],
      ucwords($fetch['pos_optype']),$fetch['pos_desc']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    