
    <?php           // ESTABLISH CONNECTION TO MYSQL
try{
    include('../../include/connect.php');                             //FETCH ALL VARIABLES
	 include('../../include/log.php'); 

    $idKey = $_POST['idKey'];
    $depcount = 0;

$sql = "SELECT COUNT(*) as count FROM position 
WHERE pos_dep_id = ? 
AND (pos_status = 'active')";
$q = $conn -> prepare($sql);
$q -> execute(array($idKey));
$browse = $q -> fetchAll();
foreach($browse as $fetch)
{
  $depcount = $fetch['count'];         
}   

if($depcount == 0){
		// UPDATE DATA TO DATABASE
	$sql = "UPDATE department SET dep_status = ? WHERE dep_id = ?";
	$q = $conn -> prepare($sql);
	$q -> execute(array('inactive',$idKey));

	$year = date('Y');
	$year = substr($year,2,3);
	$trail_id =uniqid('at'.$year);  
	$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
	$q = $conn -> prepare($sql);    
	$q -> execute(array($trail_id,'Maintenance','Department', 'REMOVE', 'Deleted: ID:'.$idKey, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

	$conn = null;
	echo json_encode(0); 	
}
else if($depcount > 0){
	echo json_encode(2); 		
}
	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}



?>  
