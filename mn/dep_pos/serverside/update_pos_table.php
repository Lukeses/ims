

    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
   include('../../include/connect.php');
   include('../../include/log.php');

                                     //FETCH ALL VARIABLES
  
    $idKey = $_POST['idKey'];
    $pos_name = $_POST['pos_name'];
    $pos_dep = $_POST['pos_dep'];
    $pos_optype = $_POST['pos_optype'];
    $pos_desc = ucwords(trim($_POST['pos_desc']));
                                                                 // INSERT DATA TO DATABASE
$sql = "UPDATE position SET pos_name=?, pos_dep_id = ?, pos_optype=?, pos_desc= ? 
WHERE pos_id=?";
$q = $conn -> prepare($sql);
$q -> execute(array(ucwords(trim($pos_name)),$pos_dep,$pos_optype,$pos_desc,$idKey));

$year = date('Y');
$year = substr($year,2,3);
$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Position', 'EDIT', 'Updated: ID:'.$idKey. ', Name:'.$pos_name.', Department ID:'.$pos_desc.', Operation Type:'.$pos_optype.', Description:'.$pos_desc, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));


$conn = null;
echo json_encode(0); 	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}




?>
