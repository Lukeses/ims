
//doc
$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();

  toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "500",
  "hideDuration": "1000",
  "timeOut": "6000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
  }//.toastr config

}); 

function logout()
{
  var choice = confirm("Are you sure you want to Log-out?");
  if(choice==true)
  {
    return true;
  }
  else
    return false;
} //.logout()

//tables
    var gr_table = $('#gr_table').dataTable({
        columnDefs: [
       { type: 'formatted-num', targets: 0 }
       ],       
      "aoColumnDefs": [ { "bSortable": false, "aTargets": [1,2] } ],
      "aaSorting": []
    });  //Initialize the datatable department

    var co_table = $('#co_table').dataTable({
        columnDefs: [
       { type: 'formatted-num', targets: 0 }
       ],       
      "aoColumnDefs": [ { "bSortable": false, "aTargets": [3,4] } ],
      "aaSorting": []
    });  //Initialize the datatable position
//.tables    


// GROUP_DIV
  function clear_gr_form(){
      $('#gr_name').val('');
      $('#gr_nameDiv').removeClass('has-error');
  }

  function load_gr_data(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/load_gr_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        gr_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 

          gr_table.fnAddData
          ([
            s[i][1].toUpperCase(),
            '<button data-toggle="tooltip" onclick="gr_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="gr_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ]); 

        }       
      }  
    }); 
    //ajax end  
  } // 

  $('#btn_add_gr').click(function(){    
  	clear_gr_form();
    $('#btn_gr_save').prop('disabled',false);
    $('#btn_gr_save').val('insert');
  	$('#gr_modal').modal('show');
  })

  function gr_row_update(get_id){
    clear_gr_form();
    $('#btn_gr_save').prop('disabled',false);    
    $('#btn_gr_save').val(get_id);

    $.ajax({
      type: 'POST',
      url: 'serverside/fetch_gr_data.php',
      dataType: 'json',
      data: 'idKey='+get_id,
      success: function(s) {
      for(var i = 0; i < s.length; i++) {
        $('#gr_name').val(s[i][0]);
        $('#gr_modal').modal('show');
      }

      }
    });
  }

  function gr_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  $('#btnDel').click(function(){
    if(($(this).val().slice(0, 2)) == 'gr'  ){
      $.ajax({
        type: 'POST',
        url: 'serverside/delete_gr_data.php',
        data: 'idKey='+$(this).val(),
        success: function(s) {          
          if(s==0){
            load_gr_data();
            $('#delModal').modal('hide');     
            toastr.error('Record Deleted');
          }
          else if(s==1){
            $('#delModal').modal('hide');     
            toastr.warning('Error: No Connection');          
          }        
          else if(s==2){
            $('#delModal').modal('hide');     
            toastr.warning('Warning: Group is in use by Company');          
          }        

        }
      });  
    }
    else{
      $.ajax({
        type: 'POST',
        url: 'serverside/delete_co_data.php',
        data: 'idKey='+$(this).val(),
        success: function(s) {          
          if(s==0){
            load_co_data();
            $('#delModal').modal('hide');     
            toastr.error('Record Deleted');
          }
          else if(s==1){
            $('#delModal').modal('hide');     
            toastr.warning('Error: No Connection');          
          }            
          else if(s==2){
            $('#delModal').modal('hide');     
            toastr.warning('Warning: Company is in use by Purchase Order');          
          }            

        }
      });        
    } 
  });

  $('#btn_gr_save').click(function(){
    if(validate_gr_form()==false){}
    else if(validate_gr_form()==true){
        //var for sending
      $(this).prop('disabled',true);        
      var gr_name = $('#gr_name').val();

      if($(this).val()=='insert'){ // will Add
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_gr_data.php",
          data: 'gr_name='+gr_name, 
          cache: false,
          success: function(s){
            if(s==0){
              $('#gr_modal').modal('hide');
              clear_gr_form();
              load_gr_data();
              toastr.success(gr_name.toUpperCase(),'Group Added:');              
            }//.if
            else if(s==2){
              $('#gr_modal').modal('hide');
              clear_gr_form();
              toastr.warning('DUPLICATE: '+gr_name.toUpperCase()+' Already Exist');   
            }
            else if (s==1){
              $('#gr_modal').modal('hide');
              clear_gr_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end          
      }
      else{ // will Update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_gr_data.php",
          data: 'gr_name='+gr_name+'&idKey='+$(this).val(), 
          cache: false,
          success: function(s){
            if(s==0){
              $('#gr_modal').modal('hide');
              clear_gr_form();
              load_gr_data();
              load_co_data();
              toastr.info(gr_name.toUpperCase(),'Group Updated:');
            }
            else if(s==2){
              $('#gr_modal').modal('hide');
              clear_gr_form();
              toastr.warning('SAME: '+gr_name.toUpperCase()+' Nothing to Commit');   
            }            
            else if(s==1){
              $('#gr_modal').modal('hide');
              clear_gr_form();
              toastr.warning('Error: No Connection');         
            }
  
          }  
        }); 
        //ajax end    
      }
    }
  })

  
  function validate_gr_form(){
    var err = true;

    if($('#gr_name').val()==''){
      err = false ;
      $('#gr_nameDiv').addClass('has-error');
    }
    else
      $('#gr_nameDiv').removeClass('has-error');   
        
    return err; 
  }  

  load_gr_data(); //load table
// .end GROUP_DIV


//COMPANY

  function clear_co_form(){
      $('#co_name').val('');
      $('#co_desc').val('');
      $('#co_nameDiv').removeClass('has-error');
      $('#co_descDiv').removeClass('has-error');
      $('#co_groupDiv').removeClass('has-error');      
  }

  $('#btn_add_co').click(function(){    
    clear_co_form();
    $('#btn_co_save').prop('disabled',false);
    $('#btn_co_save').val('insert');
    populate_co_dropdown();
    $('#co_group').prop('disabled',false);
    $('#co_modal').modal('show');
  })

  function populate_co_dropdown(){
    //ajax now populate dropdown departments
    $.ajax ({
      type: "POST",
      url: "serverside/load_gr_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#co_group').empty();
        for(var i = 0; i < s.length; i++) { 
          $('#co_group').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end      
  }

  function validate_co_form(){
    var err = true;

    if($('#co_name').val()==''){
      err = false ;
      $('#co_nameDiv').addClass('has-error');
    }
    else
      $('#co_nameDiv').removeClass('has-error');   

    if($('#co_group').val()==''){
      err = false ;
      $('#co_groupDiv').addClass('has-error');
    }
    else
      $('#co_groupDiv').removeClass('has-error');   


    return err; 
  }  

  function load_co_data(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/load_co_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        co_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 
          co_table.fnAddData
          ([
            s[i][1].toUpperCase(),s[i][2].toUpperCase(),s[i][3],
            '<button onclick="co_row_update(this.value)" value='+s[i][0]+' data-toggle="tooltip" class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button onclick="co_row_del(this.value)" value='+s[i][0]+' data-toggle="tooltip" data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ]); 

        }       
      }  
    }); 
    //ajax end  
  } // 

  load_co_data(); //populate position Table


  function co_row_update(get_id){
    clear_co_form();
    populate_co_dropdown();
    $('#btn_co_save').prop('disabled',false);    
    $('#btn_co_save').val(get_id);

    $.ajax({
      type: 'POST',
      url: 'serverside/fetch_co_data.php',
      dataType: 'json',
      data: 'idKey='+get_id,
      success: function(s) {
        for(var i = 0; i < s.length; i++) {        
          $('#co_name').val(s[i][1]);
          $('#co_desc').val(s[i][3]);
          $('#opt'+s[i][0]).prop('selected',true);
          $('#co_group').prop('disabled',true);
          $('#co_modal').modal('show');
        }

      }
    });
  }


  $('#btn_co_save').click(function(){
    if(validate_co_form()==false){}
    else if(validate_co_form()==true){
        //var for sending
      $(this).prop('disabled',true);        
      var co_name = $('#co_name').val();
      var co_desc = $('#co_desc').val();      
      var co_group = $('#co_group').val();
      var dataString = 'co_name='+co_name+'&co_group='+co_group+'&co_desc=' +co_desc;
      
      if($(this).val()=='insert'){ // will Add
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_co_data.php",
          data: dataString, 
          cache: false,
          success: function(s){
            if(s==0){
              $('#co_modal').modal('hide');
              clear_co_form();
              load_co_data();
              toastr.success(co_name.toUpperCase(),'Company Added:');              
            }//.if
            if(s==2){
              $('#co_modal').modal('hide');
              clear_co_form();
              load_co_data();
              toastr.warning('DUPLICATE: '+co_name.toUpperCase()+' Already Exist');              
            }//.if            
            else if(s==1){
              $('#co_modal').modal('hide');
              clear_co_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end          
      } 
      else{ // will Update
        //ajax now        
        $.ajax ({
          type: "POST",
          url: "serverside/update_co_data.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false,
          success: function(s){
            if(s==0){
              load_co_data();              
              $('#co_modal').modal('hide');
              clear_co_form();
              toastr.info(co_name,'Company Updated:');
            }
            if(s==2){
              $('#co_modal').modal('hide');
              clear_co_form();
              load_co_data();
              toastr.warning('SAME: '+co_name.toUpperCase()+' Nothing to Commit');              
            }//.if                
            else if(s==1){
              $('co_modal').modal('hide');
              clear_co_form();
              toastr.warning('Error: No Connection');         
            }
  
          }  
        }); 
        //ajax end                      
      } //.else
    }
  })

  function co_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }



//.end Position