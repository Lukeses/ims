
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../include/connect.php');  
 	include('../../include/log.php');  

                                     //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('co'.$year);   

    $co_name = $_POST['co_name'];
    $co_group = $_POST['co_group'];
    $co_desc = $_POST['co_desc'];    
    $count = 0;

$sql = "SELECT COUNT(*) as count FROM company
WHERE co_name LIKE ? AND(co_gr_id=?) AND(co_status='active')";
$q = $conn->prepare($sql);
$q -> execute(array($co_name,$co_group));
$browse = $q -> fetchAll();
foreach($browse as $fetch){
  $count = $fetch['count'];         
} 




if($count>0){
	echo json_encode(2); 
}
else if($count==0){
	// INSERT DATA TO DATABASE
	$sql = "INSERT INTO company	 VALUES(?,?,?,?,?)";
	$q = $conn -> prepare($sql);
	$q -> execute(array($id,$co_group,strtoupper(trim($co_name)),$co_desc,'active'));

	$trail_id =uniqid('at'.$year);  
	$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
	$q = $conn -> prepare($sql);    
	$q -> execute(array($trail_id,'Maintenance','Company', 'CREATE', 'New: ID:'.$id. ', Name:'.$co_name.', Group ID:'.$co_group.', Description:'.$co_desc, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));
	echo json_encode(0); 	
}

$conn = null;

}

catch(PDOException $x) {
echo json_encode(1); 		
$conn = null;
}



?>
