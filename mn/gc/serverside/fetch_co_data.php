<?php
   include('../../include/connect.php');

$idKey = $_POST['idKey'];

$sql = "SELECT co_gr_id,co_name,gr_name,co_desc
   FROM group_div g, company c
  WHERE c.co_gr_id = g.gr_id
  AND (c.co_id = ?)";

$q = $conn->prepare($sql);
$q -> execute(array($idKey));
$browse = $q -> fetchAll();
foreach($browse as $fetch)
{
  $output[] = array ($fetch['co_gr_id'],$fetch['co_name'],$fetch['gr_name'],$fetch['co_desc']);         
}                      

echo json_encode($output);
$conn = null;
?>