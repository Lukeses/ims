<?php
require('../include/log.php')
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Groups | Company </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />  
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatablesX/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />        
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!--toastr css -->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />      
    <!-- select 2 -->
    <link rel="stylesheet" href="../../resources/plugins/select2/select2.min.css">


    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/slive.js" type="text/javascript"></script>   
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>

  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Groups & Company
            <small>Maintenance</small>
          </h1>

        </section>

        <!-- Main content -->
        <section class="content">

        <?php include('../include/loading.html'); ?>

        <div class="row" style="margin-top:25px">

          <div class="col-sm-5 col-xs-12"> <!--left-->
            <div class="box box-solid">

                <div class="box-header">
                  <button id="btn_add_gr" class="pull-right btn bg-blue" data-toggle='tooltip' title="Add New Group" data-placement='top'> <i class='fa fa-plus'></i> Add New </button>                  
                  <h3 class="box-title"><i class="fa fa-folder-o"></i> Group List</h3>                  
                </div><!-- /.box-header -->

                <div class="box-body">

                  <table id="gr_table" class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Group Name</th> 
                        <th style="width:10px"></th> 
                        <th style="width:10px"></th>                                                                                                                                             
                      </tr>
                      <tbody></tbody>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>

          <div class="col-sm-7 col-xs-12"> <!--right-->
            <div class="box box-solid">
                <div class="box-header">
                <button id='btn_add_co' class="pull-right btn bg-blue" data-toggle='tooltip' title="Add New Company" data-placement='top'> <i class='fa fa-plus'></i> Add New </button>                                    
                  <h3 class="box-title"><i class="fa fa-building-o"></i> Company List</h3>                  
                </div><!-- /.box-header -->

                <div class="box-body">

                  <table id="co_table" class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Company Name</th> 
                        <th>Group</th>                         
                        <th>Description</th>                         
                        <th style="width:10px"></th> 
                        <th style="width:10px"></th>                                                                                                                                             
                      </tr>
                      <tbody></tbody>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->            
          </div>          
        </div>

        <?php include('modals/co_modal.html'); ?>          
        <?php include('modals/gr_modal.html'); ?>          

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <?php include('../include/footer.php'); ?>
    </div><!-- ./wrapper -->

    <!--MAIN CONTROLLER -->    
    <script src="controller.js"></script>   

  </body>
</html>
