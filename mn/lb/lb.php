<?php
require('../include/log.php')
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Backup</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- toastr-->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />       

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/slive.js" type="text/javascript"></script>   
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>
    <!--bootsrap validator-->
    <script src="../../resources/bootstrap/js/bootstrapValidator.js"></script>

  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue  sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Local Backup
            <small> Utilities</small>
          </h1>

        </section>

        <!-- Main content -->
        <section class="content">

          <?php include('../include/loading.html') ?>

          <div class="row" style="margin-top:100px">
            <div class="col-md-3"></div><!-- left margin-->
            <div class="col-md-6" style="text-align:center">
              <button role="button" id="btn_lb" data-toggle='tooltip' title="Create Local Backup" data-placement='bottom' class="btn bg-green btn-lg "
             style=" border-radius:100px; width:200px; height:200px; margin-bottom:5px; outline:none;
             text-align: center; font-size:108px; color:white "> <i class="fa fa-database"></i> 
              </button>              
            </div>
            <div class="col-md-3"></div><!-- right margin-->            
          </div>

          <div class="row" style="margin-top:50px; text-align:center">
            <div class="col-md-3"></div><!-- left margin-->
            <div class="col-md-6">
              <div class="alert alert-xs alert-dismissable" style="border: solid #ffc000 1px; background-color:white; display:none" id="msg">
                <i class=" text-success icon fa fa-check"></i> Local Backup Success at 'www' directory of the WebServer <br> Directory Name: 'db-backup.sql'
               <label id="msgContent"></label>
              </div>                           
            </div>
            <div class="col-md-3"></div><!-- right margin-->                 
          </div>



        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <?php include('../include/footer.php'); ?>
    </div><!-- ./wrapper -->


    <!--MAIN CONTROLLER -->
    <script>

      $('#btn_lb').click(function(){
        $(this).prop('disabled',true);
            $('#loading').modal({backdrop: 'static'})                
          $('#loading').modal('show');  
        setTimeout(start_backup, 3000);                    
      })

      function start_backup(){
            //ajax now
          $.ajax ({
            url: "lbX.php",
            cache: false,
            success: function(s)
            {
              $('#msg').css('display','block');
              $('#loading').modal('hide');    
            }  
          }); 
          //ajax end  
      }

    </script>
  </body>
</html>
