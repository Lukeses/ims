
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "500",
    "hideDuration": "1000",
    "timeOut": "6000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }//.toastr config  
	});  



	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	   

//Employee

	$('#btn_ve_add').click(function(){
		clear_ve_form();
    populate_vt_dropdown();
		$('#btn_ve_save').val('insert');
		$('#btn_ve_save').prop('disabled',false);
		$('#ve_modal').modal('show');
	})

	//tables
	    var ve_table = $('#ve_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [4,5] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_ve_table(){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        ve_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 

          ve_table.fnAddData
          ([
            s[i][0],s[i][1],s[i][2],s[i][3],
            '<button data-toggle="tooltip" onclick="ve_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="ve_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ],false); 
          ve_table.fnDraw();

        }       
      }  
    }); 
    $('#loading').modal('hide');    
    //ajax end  
  } //.load ve_table

  load_ve_table();
	
  function clear_ve_form(){
    populate_vt_dropdown();
  	$('#ve_no').val('');
  	$('#ve_desc').val('');
  	$('#ve_typeDiv').removeClass('has-error'); 	
  	$('#ve_noDiv').removeClass('has-error'); 	
  	$('#ve_descDiv').removeClass('has-error'); 	  	
  }

  function populate_vt_dropdown(){
    //ajax now populate dropdown departments
    $.ajax ({
      type: "POST",
      url: "serverside/populate_vt_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#ve_type').empty();
        $('#ve_type').append('<option></option>');
        for(var i = 0; i < s.length; i++) { 
          $('#ve_type').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end      
  }


  function validate_ve_form(){
    var err = true;

    if($('#ve_type').val()==''){
      err = false ;
      $('#ve_typeDiv').addClass('has-error');
    }
    else
      $('#ve_typeDiv').removeClass('has-error');   

    if($('#ve_no').val()==''){
      err = false ;
      $('#ve_noDiv').addClass('has-error');
    }
    else
      $('#ve_noDiv').removeClass('has-error');   


    if($('#ve_desc').val()==''){
      err = false ;
      $('#ve_descDiv').addClass('has-error');
    }
    else
      $('#ve_descDiv').removeClass('has-error');   

    return err; 
  }  	


  function ve_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  function ve_row_update(get_id){
    clear_ve_form();
    $('#btn_ve_save').prop('disabled',false);    
    $('#btn_ve_save').val(get_id);
      $.ajax({
        type: 'POST',
        url: 'serverside/fetch_data.php',
        dataType: 'json',
        data: 'idKey='+get_id,
        success: function(s) {
        for(var i = 0; i < s.length; i++) {
          $('#opt'+s[i][0]).prop('selected',true);
          $('#ve_no').val(s[i][1]);
          $('#ve_desc').val(s[i][2]);
          $('#ve_modal').modal('show');
        }
      }
    });
  }


  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/delete_data.php',
      data: 'idKey='+$(this).val(),
      success: function(s) {          
        if(s==0){
          load_ve_table();
          $('#delModal').modal('hide');     
          toastr.error('Record Deleted');
        }
        else if(s==1){
          $('#delModal').modal('hide');     
          toastr.warning('Error: No Connection');          
        }               
      }
    });  
  });

  $('#btn_ve_save').click(function(){
  	if(validate_ve_form() == false){}
  	else{
    $(this).prop('disabled',true);      
  		var ve_type = $('#ve_type').val();
  		var ve_no = $('#ve_no').val();
  		var ve_desc = $('#ve_desc').val();
  		var dataString = 've_type=' +ve_type+ '&ve_no=' +ve_no+ '&ve_desc=' +ve_desc;

      if($(this).val()=='insert'){ //will insert
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_data.php",
          data: dataString, 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#ve_modal').modal('hide');
              clear_ve_form();
              load_ve_table();
              toastr.success(ve_type+' '+ve_no,'Vehicle Added:');              
            }//.if
            else{
              $('#ve_modal').modal('hide');
              clear_ve_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }

      else{ //will update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_data.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#ve_modal').modal('hide');
              clear_ve_form();
              load_ve_table();
              toastr.info(ve_type+' '+ve_no,'Vehicle Updated:');              
            }//.if
            else{
              $('#ve_modal').modal('hide');
              clear_ve_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }


  	}//.else
  })//.btn_em_save


//. employee	   