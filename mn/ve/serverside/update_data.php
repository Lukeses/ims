

    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
   include('../../include/connect.php');
   include('../../include/log.php');

                                     //FETCH ALL VARIABLES
  
    $idKey = $_POST['idKey'];
	$ve_type = trim($_POST['ve_type']);
	$ve_no = $_POST['ve_no'];
	$ve_desc = ucwords(trim($_POST['ve_desc']));

	        // INSERT DATA TO DATABASE
$sql = "UPDATE vehicle SET ve_vt_id=?, ve_no=?, ve_desc=? WHERE ve_id=?";
$q = $conn -> prepare($sql);
$q -> execute(array($ve_type,$ve_no,$ve_desc,$idKey));

$year = date('Y');
$year = substr($year,2,3);
$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Vehicle', 'EDIT', 'Updated: ID:'.$idKey.', Type ID:'.$ve_type.', Plt/No:'.$ve_no.', Description:'.$ve_desc, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$conn = null;
echo json_encode(0); 	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}




?>
