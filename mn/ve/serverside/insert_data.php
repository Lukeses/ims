
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../include/connect.php');  
 	include('../../include/log.php');  

                                     //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('ve'.$year);   

	$ve_type = ucwords(trim($_POST['ve_type']));
	$ve_no = $_POST['ve_no'];
	$ve_desc = ucwords(trim($_POST['ve_desc']));

                                                                 // INSERT DATA TO DATABASE
$sql = "INSERT INTO vehicle VALUES(?,?,?,?,?)";
$q = $conn -> prepare($sql);
$q -> execute(array($id,$ve_no,$ve_type,$ve_desc,'active'));

$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Vehicle', 'CREATE', 'Updated: ID:'.$id. ', Type ID:'.$ve_type.', Plt/No:'.$ve_no.', Description:'.$ve_desc, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$conn = null;
echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
