<?php
    include('../../include/connect.php');


  $sql = "SELECT ve_id, ve_no, vt_name, ve_desc 
  FROM vehicle v, vehicle_type vt
  WHERE (ve_status='active') 
  AND (v.ve_vt_id = vt.vt_id)
  ORDER BY ve_id ASC ";

  $q = $conn->prepare($sql);
  $q -> execute();
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['ve_id'],$fetch['ve_no'],$fetch['vt_name'],$fetch['ve_desc']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    