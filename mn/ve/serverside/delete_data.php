
    <?php           // ESTABLISH CONNECTION TO MYSQL
try{
    include('../../include/connect.php');                             //FETCH ALL VARIABLES
	include('../../include/log.php');    
    $idKey = $_POST['idKey'];


		// UPDATE DATA TO DATABASE
	$sql = "UPDATE vehicle SET ve_status = ? WHERE ve_id = ?";
	$q = $conn -> prepare($sql);
	$q -> execute(array('inactive',$idKey));

	$year = date('Y');
	$year = substr($year,2,3);
	$trail_id =uniqid('at'.$year);  
	$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
	$q = $conn -> prepare($sql);    
	$q -> execute(array($trail_id,'Maintenance','Vehicle', 'REMOVE', 'Deleted: ID:'.$idKey, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

	$conn = null;
	echo json_encode(0); 	
	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}



?>  
