<?php
    include('../../include/connect.php');

    $gd_name = $_POST['gd_name'];

  $sql = "SELECT prod_id, prod_name, cat_name, prod_var, prod_price, sup_name, co_name, prod_treshold,prod_sku
   FROM product p, category cat, supplier s, company c, group_div g
   WHERE (c.co_id=p.prod_co_id)
   AND (c.co_gr_id=g.gr_id)
   AND (g.gr_id = ? )
   AND (p.prod_cat_id = cat.cat_id)
   AND (p.prod_sup_id = s.sup_id)
   AND (p.prod_status = 'active')
   GROUP BY prod_id
   ORDER BY prod_name ASC ";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_name));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['prod_id'],$fetch['prod_name'],
      $fetch['cat_name'],$fetch['prod_var'],$fetch['prod_price'],$fetch['sup_name'],
      $fetch['co_name'],$fetch['prod_treshold'],$fetch['prod_sku']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    