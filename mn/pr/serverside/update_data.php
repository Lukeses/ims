

    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
   include('../../include/connect.php');
   include('../../include/log.php');

                                     //FETCH ALL VARIABLES
  
    $idKey = $_POST['idKey'];
	$pr_name = ucwords(trim($_POST['pr_name']));
	$pr_sup = $_POST['pr_sup'];
    $pr_co = $_POST['pr_co'];
    $pr_cat = $_POST['pr_cat'];    
	$pr_var = ucwords(trim($_POST['pr_var']));
    $pr_price = ucwords(trim($_POST['pr_price']));
    $pr_treshold = trim($_POST['pr_treshold']);
    $pr_sku = trim($_POST['pr_sku']);

	        // INSERT DATA TO DATABASE
$sql = "UPDATE product SET prod_name=?, prod_sku=?, prod_cat_id=?, prod_var=?, prod_price=?, 
prod_sup_id=?, prod_co_id=?, prod_treshold=? WHERE prod_id = ? ";

$q = $conn -> prepare($sql);
$q -> execute(array($pr_name,$pr_sku,$pr_cat,$pr_var,$pr_price,$pr_sup,$pr_co,$pr_treshold,$idKey));

$year = date('Y');
$year = substr($year,2,3);
$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Product', 'EDIT', 'Updated: ID:'.$idKey. ', Name:'.$pr_name.', SKU:'.$pr_sku.', Variant:'.$pr_var.', Treshold:'.$pr_treshold.', Price:'.$pr_price, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$conn = null;
echo json_encode(0); 	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}




?>
