
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../include/connect.php');  
    include('../../include/log.php');     
                                     //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('pr'.$year);   
    $pv_id =uniqid('pv'.$year);   

	$pr_name = ucwords(trim($_POST['pr_name']));
	$pr_sup = $_POST['pr_sup'];
    $pr_co = $_POST['pr_co'];
    $pr_cat = $_POST['pr_cat'];    
	$pr_var = ucwords(trim($_POST['pr_var']));
    $pr_price = ucwords(trim($_POST['pr_price']));
    $pr_treshold = trim($_POST['pr_treshold']);
    $pr_sku = trim($_POST['pr_sku']);

                                                                 // INSERT DATA TO DATABASE
$sql = "INSERT INTO product VALUES(?,?,?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);
$q -> execute(array($id,$pr_sku,$pr_name,$pr_cat,$pr_var,$pr_price,$pr_sup,$pr_co,$pr_treshold,'active'));


$sql = "INSERT INTO product_version VALUES(?,?,?,NOW(),?)";
$q = $conn -> prepare($sql);
$q -> execute(array($pv_id,$id,$pr_price,'active'));   


$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Product', 'CREATE', 'New: ID:'.$id. ', Name:'.$pr_name.', SKU:'.$pr_sku.', Variant:'.$pr_var.', Treshold:'.$pr_treshold.', Price:'.$pr_price, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Product Version', 'CREATE', 'New: ID:'.$pv_id.', Product ID:'.$id.', Version Price:'.$pr_price, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));
$conn = null;
echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
