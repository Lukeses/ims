

	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-left",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "500",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }//.toastr config  

    //Initialize Select2 Elements
    $("#gd_name").select2();   
    $('#pr_co').select2();
    $('#pr_cat').select2();    
    $('#pr_sup').select2();    

    $('#pr_name').typeahead({
        name: 'pr_name',
        remote:'serverside/search_product.php?key=%QUERY',
        limit : 10
    });     
	});  

	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	  
 function comma(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }

  function select_groupdiv(){
    populate_gr_dropdown();    
    $('#gd_name').select2().select2('val','none');                  
    $('#gd_nameDiv').removeClass('has-error');    
    $('#gd_modal').modal('show');    
  }     

//Product

  $('#pr_sku').focus(function(){
    $(this).val($('#pr_name').val());
  })

  $('#btn_gd_submit').click(function(){
    if($('#gd_name').val()=='none'){
      $('#gd_nameDiv').addClass('has-error');
    }
    else{
      $('#btn_select_groupdiv').removeClass('bg-red');
      $('#btn_select_groupdiv').addClass('bg-green');         
      $('#gd_nameDiv').removeClass('has-error');
      $('#gd_selected').val($('#gd_name').val());
      load_pr_table($('#gd_selected').val());
      $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );
      $('#gd_modal').modal('hide');    
      change_session_group();
    }
  })

  function change_session_group(){
    var gr_name = $( "#gd_name option:selected" ).text();  
    var gr_id = $('#gd_name').val();
    var dataString = 'gr_id='+gr_id+'&gr_name='+gr_name;
    //ajax now
    $.ajax ({
      type: "POST",
      url: "../include/change_session_group.php",
      data: dataString,
      success: function(s){         
      }  
    }); 
    //ajax end      
  }

  function populate_gr_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_gd_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#gd_name').empty();
        $('#gd_name').append('<option value="none">--Select Group--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
  } // 

  function populate_company_dropdown(gd_name){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_company_dropdown.php",
      dataType: 'json',      
      data: 'gd_name='+gd_name,
      cache: false,
      success: function(s)
      {
        $('#pr_co').empty();
        $('#pr_co').html('<option selected="selected" value="none">--Search Company--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#pr_co').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
    $('#pr_co').select2().select2('val','none');                      
  } // 

  function populate_category_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_category_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#pr_cat').empty();
        $('#pr_cat').html('<option selected="selected" value="none">--Search Category--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#pr_cat').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
    $('#pr_cat').select2().select2('val','none');                      
  } // 

  function populate_supplier_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_supplier_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#pr_sup').empty();
        $('#pr_sup').html('<option selected="selected" value="none">--Search Supplier--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#pr_sup').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
    $('#pr_sup').select2().select2('val','none');                      
  } // 

	$('#btn_pr_add').click(function(){
    if($('#gd_selected').val()==''){
      $('#btn_select_groupdiv').removeClass('bg-green');
      $('#btn_select_groupdiv').addClass('bg-red');      
    }
    else{   
      clear_pr_form();
      $('#btn_pr_save').val('insert');
      $('#btn_pr_save').prop('disabled',false);
      $('#pr_modal').modal('show');
    }
	})

	//tables
	    var pr_table = $('#pr_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [8] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_pr_table(gd_name){ 
    //ajax now
    $('#loading').modal('show');
    pr_table.fnClearTable();            
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      data: 'gd_name='+gd_name,
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        for(var i = 0; i < s.length; i++) 
        { 

          pr_table.fnAddData
          ([
            s[i][8],s[i][1],s[i][2],s[i][3],comma(s[i][4]),s[i][5],s[i][6],comma(s[i][7]),
            '<button data-toggle="tooltip" onclick="pr_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            //'<button onclick="pr_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ],false); 
          pr_table.fnDraw();

        }       
      }  
    }); 
    //ajax end  
  } //.load pr_table


  function clear_pr_form(){     
  	$('#pr_name').val('');
  	$('#pr_co').val('');
  	$('#pr_sup').val('');
    $('#pr_cat').val('');
    $('#pr_var').val('');
    $('#pr_price').val('');
    $('#pr_treshold').val('');
    $('#pr_sku').val('');
  	$('#pr_nameDiv').removeClass('has-error'); 	
  	$('#pr_coDiv').removeClass('has-error'); 	
  	$('#pr_supDiv').removeClass('has-error'); 	  
    $('#pr_catDiv').removeClass('has-error');       
    $('#pr_varDiv').removeClass('has-error');       
    $('#pr_priceDiv').removeClass('has-error');       
    $('#pr_tresholdDiv').removeClass('has-error');
    $('#pr_skuDiv').removeClass('has-error');      

    populate_company_dropdown($('#gd_selected').val());
    populate_category_dropdown();
    populate_supplier_dropdown();            	
  }

  function validate_pr_form(){
    var err = true;

    if($('#pr_name').val()==''){
      err = false ;
      $('#pr_nameDiv').addClass('has-error');
    }
    else
      $('#pr_nameDiv').removeClass('has-error');   

    if($('#pr_sku').val()==''){
      err = false ;
      $('#pr_skuDiv').addClass('has-error');
    }
    else
      $('#pr_skuDiv').removeClass('has-error');   


    if($('#pr_var').val()==''){
      err = false ;
      $('#pr_varDiv').addClass('has-error');
    }
    else
      $('#pr_varDiv').removeClass('has-error');             

    if($('#pr_co').val()=='none'){
      err = false ;
      $('#pr_coDiv').addClass('has-error');
    }
    else
      $('#pr_coDiv').removeClass('has-error');   

    if($('#pr_sup').val()=='none'){
      err = false ;
      $('#pr_supDiv').addClass('has-error');
    }
    else
      $('#pr_supDiv').removeClass('has-error');   

    if($('#pr_cat').val()=='none'){
      err = false ;
      $('#pr_catDiv').addClass('has-error');
    }
    else
      $('#pr_catDiv').removeClass('has-error');   


    if($('#pr_price').val()==''){
      err = false ;
      $('#pr_priceDiv').addClass('has-error');
    }
    else if( $('#pr_price').val() < 0){
      err = false ;
      $('#pr_priceDiv').addClass('has-error');      
    }
    else
      $('#pr_priceDiv').removeClass('has-error');   

    if($('#pr_treshold').val()==''){
      err = false ;
      $('#pr_tresholdDiv').addClass('has-error');
    }
    else if( $('#pr_treshold').val() < 0){
      err = false ;
      $('#pr_tresholdDiv').addClass('has-error');      
    }
    else
      $('#pr_tresholdDiv').removeClass('has-error');   

    return err; 
  }  	

  function pr_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  function pr_row_update(get_id){
    clear_pr_form();
    $('#btn_pr_save').prop('disabled',false);    
    $('#btn_pr_save').val(get_id);
      $.ajax({
        type: 'POST',
        url: 'serverside/fetch_data.php',
        dataType: 'json',
        data: 'idKey='+get_id,
        success: function(s) {
        for(var i = 0; i < s.length; i++) {
          $('#pr_name').val(s[i][0]);
          $('#pr_sku').val(s[i][1]);
          $('#pr_sup').select2().select2('val',s[i][2]);  
          $('#pr_co').select2().select2('val',s[i][3]);          
          $('#pr_cat').select2().select2('val',s[i][4]);
          $('#pr_var').val(s[i][5]);
          $('#pr_price').val(s[i][6]);
          $('#pr_treshold').val(s[i][7]);                    
        }
          $('#pr_modal').modal('show');        
      }
    });
  }


  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/delete_data.php',
      data: 'idKey='+$(this).val(),
      success: function(s) {          
        if(s==0){
          load_pr_table($('#gd_selected').val());
          $('#delModal').modal('hide');     
          toastr.error('Record Deleted');
        }
        else if(s==1){
          $('#delModal').modal('hide');     
          toastr.warning('Error: No Connection');          
        }               
      }
    });  
  });

  $('#btn_pr_save').click(function(){
  	if(validate_pr_form() == false){}
  	else{
    $(this).prop('disabled',true);     
      var pr_name = $('#pr_name').val(); 
  		var pr_sup = $('#pr_sup').val();
      var pr_co = $('#pr_co').val();
  		var pr_cat = $('#pr_cat').val();
      var pr_var = $('#pr_var').val();
      var pr_price = $('#pr_price').val();
      var pr_treshold = $('#pr_treshold').val();
      var pr_sku = $('#pr_sku').val();

  		var dataString = 'pr_name=' +pr_name+ '&pr_sup=' +pr_sup+ '&pr_co=' +pr_co+ '&pr_cat='+pr_cat+ '&pr_var='+pr_var+'&pr_price='+pr_price+'&pr_treshold='+pr_treshold+ '&pr_sku='+pr_sku;
      if($(this).val()=='insert'){ //will insert
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_data.php",
          data: dataString, 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#pr_modal').modal('hide');
              clear_pr_form();
              load_pr_table($('#gd_selected').val());
              toastr.success(pr_name,'Product Added:');              
            }//.if
            else{
              $('#pr_modal').modal('hide');
              clear_pr_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }

      else{ //will update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_data.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#pr_modal').modal('hide');
              clear_pr_form();
              load_pr_table($('#gd_selected').val());
              toastr.info(pr_name,'Product Updated:');              
            }//.if
            else{
              $('#pr_modal').modal('hide');
              clear_pr_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }


  	}//.else
  })//.btn_em_save

  function initialize(){
    if($('#gd_selected').val()==''){
      select_groupdiv();
    }
    else{
      load_pr_table($('#gd_selected').val());
    }
  }

  initialize();

//. employee	   
