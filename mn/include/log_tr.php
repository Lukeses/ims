<?php 

  if(!isset($_SESSION)) 
  { session_start(); } 

  if( !isset($_SESSION["u_name"]) || !isset($_SESSION["u_type"]) )
  { 

    if(isset($_SESSION)) 
    {
      // remove all session variables
      session_unset(); 

      // destroy the session 
      session_destroy(); 
    } ?>

    <script type="text/javascript">   
      window.location="../../mn/user/login.php";
    </script>

  <?php
  }
  else {  
    $u_name = $_SESSION["u_name"];
    $u_type = $_SESSION["u_type"];

      if(isset($_SESSION['session_group']) ){
        $session_group = $_SESSION['session_group'];
        $session_group_name = $_SESSION['session_group_name'];        
      }
      else{
        $session_group = '';
        $session_group_name = '';        
      }
    
    ?>


 <?php } ?>