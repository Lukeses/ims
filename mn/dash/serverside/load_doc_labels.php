<?php
    include('../../include/connect.php');

$gr_id = $_POST['gr_id'];
$pending_grn=0; $pending_dn=0; 
$shipped_grn=0; $shipped_dn=0; 
$todate = date('Y-m-d');


  $sql = "SELECT 
  COALESCE((SUM((case when d.doc_desti = l.loc_id then (pv.pv_price * dp.dp_qty) else 0 end)) ),0) as grand
   FROM location l, company c, group_div g, document d, document_product dp, product_version pv
   WHERE (c.co_id=l.loc_co_id)
   AND (c.co_gr_id=g.gr_id) 
   AND (g.gr_id = ? )
   AND (d.doc_id = dp.dp_doc_id)
   AND (l.loc_type = 'warehouse')
   AND (d.doc_type = 'GRN')
   AND (dp.dp_prod_id = pv.pv_id)
   AND (d.doc_status = 'pending')";
  $q = $conn->prepare($sql);
  $q -> execute(array($gr_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch){
    $pending_grn = $fetch['grand'];
  }   

  $sql = "SELECT 
  COALESCE((SUM((case when d.doc_desti = l.loc_id then (pv.pv_price * dp.dp_qty) else 0 end)) ),0) as grand
   FROM location l, company c, group_div g, document d, document_product dp, product_version pv
   WHERE (c.co_id=l.loc_co_id)
   AND (c.co_gr_id=g.gr_id) 
   AND (g.gr_id = ? )
   AND (d.doc_id = dp.dp_doc_id)
   AND (l.loc_type = 'warehouse')
   AND (d.doc_type = 'GRN')
   AND (d.doc_date = CURDATE())
   AND (dp.dp_prod_id = pv.pv_id)
   AND (d.doc_status = 'shipped')";
  $q = $conn->prepare($sql);
  $q -> execute(array($gr_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch){
    $shipped_grn = $fetch['grand'];
  }   

  $sql = "SELECT 
  COALESCE((SUM((case when d.doc_desti = l.loc_id then (pv.pv_price * dp.dp_qty) else 0 end)) ),0) as grand
   FROM location l, company c, group_div g, document d, document_product dp, product_version pv
   WHERE (c.co_id=l.loc_co_id)
   AND (c.co_gr_id=g.gr_id) 
   AND (g.gr_id = ? )
   AND (d.doc_id = dp.dp_doc_id)
   AND (l.loc_type = 'branch')
   AND (d.doc_type = 'DN')
   AND (dp.dp_prod_id = pv.pv_id)
   AND (d.doc_status = 'pending')";
  $q = $conn->prepare($sql);
  $q -> execute(array($gr_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch){
    $pending_dn = $fetch['grand'];
  }   

  $sql = "SELECT 
  COALESCE((SUM((case when d.doc_desti = l.loc_id then (pv.pv_price * dp.dp_qty) else 0 end)) ),0) as grand
   FROM location l, company c, group_div g, document d, document_product dp, product_version pv
   WHERE (c.co_id=l.loc_co_id)
   AND (c.co_gr_id=g.gr_id) 
   AND (g.gr_id = ? )
   AND (d.doc_id = dp.dp_doc_id)
   AND (l.loc_type = 'branch')
   AND (d.doc_date = CURDATE())   
   AND (d.doc_type = 'DN')
   AND (dp.dp_prod_id = pv.pv_id)
   AND (d.doc_status = 'shipped')";
  $q = $conn->prepare($sql);
  $q -> execute(array($gr_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch){
    $shipped_dn = $fetch['grand'];
  }   

$conn = null;             
$output[] = array($pending_grn,$shipped_grn,$pending_dn,$shipped_dn);
echo json_encode($output);
?>    