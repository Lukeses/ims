<?php
    include('../../include/connect.php');

$gr_id = $_POST['gr_id'];

  $sql = "  SELECT pv.pv_id,l.loc_id, l.loc_name, pr.prod_name, pr.prod_sku,pr.prod_var,
(SELECT c.cat_name FROM category c WHERE c.cat_id = pr.prod_cat_id) as category, 
(SELECT s.sup_name FROM supplier s WHERE s.sup_id = pr.prod_sup_id) as supplier, 
co.co_name,pv.pv_price, pr.prod_treshold,
  ((SUM(case when d.doc_desti = l.loc_id then (dp.dp_qty) else 0 end))-(SUM(case when d.doc_source = l.loc_id then (dp.dp_qty) else 0 end))) as soh
  FROM product_version pv, product pr, document_product dp, document d, company co , location l
  WHERE (pv.pv_prod_id = pr.prod_id) 
  AND (pr.prod_co_id = co.co_id)
  AND (co.co_gr_id = ?)  
  AND (d.doc_id = dp.dp_doc_id)
  AND (dp.dp_prod_id = pv.pv_id)
  AND (d.doc_status='shipped')
  AND (d.doc_source = l.loc_id OR d.doc_desti = l.loc_id)
  AND (l.loc_type = 'warehouse')
  GROUP BY pv.pv_id
  ORDER BY pr.prod_treshold ASC, soh DESC";

  $q = $conn->prepare($sql);
  $q -> execute(array($gr_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['pv_id'],$fetch['loc_id'],$fetch['loc_name'],$fetch['prod_name'],
      $fetch['prod_sku'],$fetch['prod_var'],$fetch['category'],$fetch['supplier'],$fetch['co_name'],
      $fetch['pv_price'],$fetch['prod_treshold'],$fetch['soh']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    