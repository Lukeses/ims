
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "500",
    "hideDuration": "1000",
    "timeOut": "6000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }//.toastr config  
	});  



	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	   

//Employee

	$('#btn_ve_add').click(function(){
		clear_ve_form();
		$('#btn_ve_save').val('insert');
		$('#btn_ve_save').prop('disabled',false);
		$('#vt_modal').modal('show');
	})

	//tables
	    var ve_table = $('#ve_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [1,2] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_ve_table(){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        ve_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 

          ve_table.fnAddData
          ([
            s[i][1],
            '<button data-toggle="tooltip" onclick="ve_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="ve_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ],false); 
          ve_table.fnDraw();

        }       
      }  
    }); 
    $('#loading').modal('hide');    
    //ajax end  
  } //.load ve_table

  load_ve_table();
	
  function clear_ve_form(){
  	$('#vt_name').val('');
  	$('#vt_nameDiv').removeClass('has-error'); 	  	
  }

  function validate_ve_form(){
    var err = true;

    if($('#vt_name').val()==''){
      err = false ;
      $('#vt_nameDiv').addClass('has-error');
    }
    else
      $('#vt_nameDiv').removeClass('has-error');   


    return err; 
  }  	


  function ve_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  function ve_row_update(get_id){
    clear_ve_form();
    $('#btn_ve_save').prop('disabled',false);    
    $('#btn_ve_save').val(get_id);
      $.ajax({
        type: 'POST',
        url: 'serverside/fetch_data.php',
        dataType: 'json',
        data: 'idKey='+get_id,
        success: function(s) {
        for(var i = 0; i < s.length; i++) {
          $('#vt_name').val(s[i][0]);
          $('#vt_modal').modal('show');
        }
      }
    });
  }


  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/delete_data.php',
      data: 'idKey='+$(this).val(),
      success: function(s) {          
          if(s==0){
            load_ve_table();
            $('#delModal').modal('hide');     
            toastr.error('Record Deleted');
          }
          else if(s==1){
            $('#delModal').modal('hide');     
            toastr.warning('Error: No Connection');          
          }        
          else if(s==2){
            $('#delModal').modal('hide');     
            toastr.warning('Warning: Type is in use by Vehicles');          
          }              
      }
    });  
  });

  $('#btn_ve_save').click(function(){
  	if(validate_ve_form() == false){}
  	else{
    $(this).prop('disabled',true);    
      var vt_name = $('#vt_name').val();  
  		var dataString = 'vt_name=' +vt_name;

      if($(this).val()=='insert'){ //will insert
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_data.php",
          data: dataString, 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#vt_modal').modal('hide');
              clear_ve_form();
              load_ve_table();
              toastr.success(vt_name,'Vehicle Type Added:');              
            }//.if
            else{
              $('#vt_modal').modal('hide');
              clear_ve_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }

      else{ //will update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_data.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#vt_modal').modal('hide');
              clear_ve_form();
              load_ve_table();
              toastr.info(vt_name,'Vehicle Type Updated:');              
            }//.if
            else{
              $('#vt_modal').modal('hide');
              clear_ve_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }


  	}//.else
  })//.btn_em_save


//. employee	   