

    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
   include('../../include/connect.php');
   include('../../include/log.php');

                                     //FETCH ALL VARIABLES
  
    $idKey = $_POST['idKey'];
	$vt_name = ucwords(trim($_POST['vt_name']));

	        // INSERT DATA TO DATABASE
$sql = "UPDATE vehicle_type SET vt_name=? WHERE vt_id=?";

$q = $conn -> prepare($sql);
$q -> execute(array($vt_name,$idKey));

$year = date('Y');
$year = substr($year,2,3);
$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Vehicle Type', 'EDIT', 'Updated: ID:'.$idKey. ', Type Name:'.$vt_name, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$conn = null;
echo json_encode(0); 	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}




?>
