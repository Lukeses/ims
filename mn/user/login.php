
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Log in | Inventory Mangement System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">
  </head>
  <body class="login-page">

    <div class="login-box" >
      <div class="login-logo">
        <b>IMS</b> Cloudipark
      </div><!-- /.login-logo -->
      <div class="login-box-body" >
        <p class="login-box-msg">Sign in to start your session</p>
        <form>
          <div class="form-group has-feedback" id="unameDiv">
            <input id="uname" type="text" class="form-control input-lg" placeholder="Username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback" id="upassDiv">
            <input id="upass" type="password" class="form-control input-lg" placeholder="Password" >
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div style="text-align:center; display:none" id="alertmessage" class="form-group">
            <text class="lead" style="font-color:red"> <i class="fa fa-warning"></i> Invalid Credentials </label>
          </div>
          <button id="loginBtn" class="btn btn-primary btn-block btn-lg"><i class="fa fa-sign-in"> </i> Sign In</button>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

  </body>

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>

  <script type="text/javascript">

//****************** FUNCTION of update FETCHING **************//
function loginprocess()
{
  $('#loginBtn').prop('disabled',true);  
  var uname = $('#uname').val();
  var upass = $('#upass').val();

   $.ajax({
    type: 'POST',
    url: 'loginprocess.php',
    data: 'uname='+uname+ '&upass='+upass,
    dataType: 'json',
    success: function(s) {
      if(s == 'false'){
        clear();
        $('#alertmessage').css('display','block');
        $('#loginBtn').addClass('bg-red');
        $('#loginBtn').prop('disabled',false);          
      }
      else if(s == 'true'){
        setTimeout(' window.location="../../index.php"', 300);                
      }
    }
  });
}
//****************** end of FUNCTION of updateFetching **************//

    $('#loginBtn').click(function(){
        if(formValidation()=='false'){
          loginprocess();          
        }

    })

    function formValidation() 
    {
    
      var err='false';

      if($('#uname').val()=='')
      {
        $('#unameDiv').addClass('has-error');
        err = 'true';
      }
      else
        $('#unameDiv').removeClass('has-error');

      if($('#upass').val()=='')
      {
        $('#upassDiv').addClass('has-error');
        err = 'true';
      }
      else
        $('#upassDiv').removeClass('has-error');


      if(err == 'true')
        return 'true';
      else if(err=='false')
        return 'false';
    }

    function clear()
    {
      //$('#uname').val('');
      $('#upass').val('');
      $('#unameDiv').removeClass('has-error');
      $('#upassDiv').removeClass('has-error');
    }

  </script>
</html>
