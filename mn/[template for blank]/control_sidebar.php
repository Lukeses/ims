<?php ?>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-light">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-bell"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-wrench"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Item Stock Treshold</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="#">
                  <i class="menu-icon bg-red">12</i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Office Supplies</h4>
                    <p>Stock level Alert</p>
                  </div>
                </a>
              </li>

              <li>
                <a href="#">
                  <i class="menu-icon bg-red">12</i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Office Supplies</h4>
                    <p>Stock level Alert</p>
                  </div>
                </a>
              </li>

            </ul><!-- /.control-sidebar-menu -->
          </div><!-- /.tab-pane -->

          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>


      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->


      <div class="control-sidebar-bg"></div>

<?php ?>