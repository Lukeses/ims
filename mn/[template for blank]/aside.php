<?php ?>      

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <?php echo'<img src="../../resources/dist/img/'.$u_type.'.png"'.' class="user-image" alt="User Image">'; ?>
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo(ucwords($u_name)); ?></span>
                </a>
                  <ul class="dropdown-menu" style="width:10%;border-radius:5px">
                    <li style="text-align:center"> 
                      <small style="font-size:0.8em"><?php echo ucfirst($u_type); ?></small>
                    </li>
                      <li class="divider"></li>
                  <?php if($u_type=="admin"){ ?>                      
                    <li><a href="../uc/uc.php"><i class="fa fa-users"></i> User Accounts</a></li>
                  <?php } ?>
                    <li><a onclick="return logout()" href="mn/user/processlogout.php"> <i class="fa fa-sign-in"></i><span>Log-out</span>    
              </a></li><br>
                  </ul>
              </li>

              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-ellipsis-v" title="Notifications"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">MENU NAVIGATION</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="../../index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            
            <li class="active treeview">
              <a href="index.php"><i class="fa fa-gear"></i> <span>Maintenance</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="../pr/pr.php"><i class="fa fa-circle-o"></i> Product</a></li>
                <li class="active"><a href="../su/su.php"><i class="fa fa-circle"></i> Supplier</a></li>
                <li><a href="../lo/lo.php"><i class="fa fa-circle-o"></i> Location</a></li>
                <li><a href="../ve/ve.php"><i class="fa fa-circle-o"></i> Vehicle</a></li>                
                <li><a href="../em/em.php"><i class="fa fa-circle-o"></i> Employee</a></li>
                <li><a href="../og/og.php"><i class="fa fa-circle-o"></i> Owner Group</a></li>                                
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="fa fa-paste"></i> <span>Operation</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="mn/pr/pr.php"><i class="fa fa-circle-o"></i>Goods Received</a></li>
                <li><a href="mn/su/su.php"><i class="fa fa-circle-o"></i>Delivery</a></li>
                <li><a href="mn/ve/ve.php"><i class="fa fa-circle-o"></i>Stock Transfer</a></li>                
                <li ><a href="mn/em/em.php"><i class="fa fa-circle-o"></i>Lost/Damaged</a></li>
                <li><a href="mn/og/og.php"><i class="fa fa-circle-o"></i> Adjustment</a></li>                                
              </ul>
            </li>            
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
<?php ?>