
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "500",
    "hideDuration": "1000",
    "timeOut": "6000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }//.toastr config  
	});  



	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	   

//Audit Trail


	//tables
	    var tr_table = $('#tr_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_tr_table(){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        tr_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 
              var statcol;          
              if(s[i][3]=='CREATE')
                statcol = "label label-success";
              else if(s[i][3]=='EDIT')
                statcol = "label label-primary";       
              else if(s[i][3]=='REMOVE')
                statcol = "label label-danger"; 
              else
                statcol = "label label-default"


          tr_table.fnAddData
          ([
            s[i][0],s[i][1],s[i][2],'<span class="'+statcol+'">'+s[i][3]+'</span>',s[i][4],s[i][5],s[i][6],s[i][7],
          ],false); 
          tr_table.fnDraw();

        }       
      }  
    }); 
    //$('#loading').modal('hide');    
    //ajax end  
  } //.load cat_table

  load_tr_table();


//. employee	   