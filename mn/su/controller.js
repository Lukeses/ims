
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-left",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "500",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }//.toastr config  

    load_su_table();
	});  

	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	   

//Supplier

	$('#btn_su_add').click(function(){
		clear_su_form();
		$('#btn_su_save').val('insert');
		$('#btn_su_save').prop('disabled',false);   
		$('#su_modal').modal('show');
	})

	//tables
	    var su_table = $('#su_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [5,6] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_su_table(){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        su_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 

          su_table.fnAddData
          ([
            s[i][0],s[i][1],s[i][2],s[i][3],s[i][4],
            '<button data-toggle="tooltip" onclick="su_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="su_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ],false); 
          su_table.fnDraw();

        }       
      }  
    }); 
    $('#loading').modal('hide');    
    //ajax end  
  } //.load su_table

	
  function clear_su_form(){
  	$('#su_name').val('');
  	$('#su_phone').val('');
  	$('#su_tel').val('');
  	$('#su_desc').val('');
  	$('#su_nameDiv').removeClass('has-error'); 	
  	$('#su_phoneDiv').removeClass('has-error'); 	
  	$('#su_telDiv').removeClass('has-error'); 	
  	$('#su_descDiv').removeClass('has-error'); 	  	
  }

  function validate_su_form(){
    var err = true;

    if($('#su_name').val()==''){
      err = false ;
      $('#su_nameDiv').addClass('has-error');
    }
    else
      $('#su_nameDiv').removeClass('has-error');   

    return err; 
  }  	

  function su_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  function su_row_update(get_id){
    clear_su_form();
    $('#btn_su_save').prop('disabled',false);    
    $('#btn_su_save').val(get_id);
      $.ajax({
        type: 'POST',
        url: 'serverside/fetch_data.php',
        dataType: 'json',
        data: 'idKey='+get_id,
        success: function(s) {
        for(var i = 0; i < s.length; i++) {
          $('#su_name').val(s[i][0]);
          $('#su_phone').val(s[i][1]);
          $('#su_tel').val(s[i][2]);
          $('#su_desc').val(s[i][3]);
          $('#su_modal').modal('show');
        }
      }
    });
  }


  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/delete_data.php',
      data: 'idKey='+$(this).val(),
      success: function(s) {          
        if(s==0){
          load_su_table();
          $('#delModal').modal('hide');     
          toastr.error('Record Deleted');
        }
        else if(s==1){
          $('#delModal').modal('hide');     
          toastr.warning('Error: No Connection');          
        }       
        else if(s==2){
          $('#delModal').modal('hide');     
          toastr.warning('Warning: Supplier is in use by Product');          
        }                  
      }
    });  
  });

  $('#btn_su_save').click(function(){
  	if(validate_su_form() == false){}
  	else{
    $(this).prop('disabled',true);      
  		var su_name = $('#su_name').val();
  		var su_phone = $('#su_phone').val();
  		var su_tel = $('#su_tel').val();
  		var su_desc = $('#su_desc').val();
  		var dataString = 'su_name='+su_name+'&su_phone='+su_phone+'&su_tel='+su_tel+'&su_desc='+su_desc;


      if($(this).val()=='insert'){ //will insert
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_data.php",
          data: dataString, 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#su_modal').modal('hide');
              clear_su_form();
              load_su_table();
              toastr.success(su_name,'Supplier Added:');              
            }//.if
            else{
              $('#su_modal').modal('hide');
              clear_su_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }

      else{ //will update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_data.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#su_modal').modal('hide');
              clear_su_form();
              load_su_table();
              toastr.info(su_name,'Supplier Updated:');              
            }//.if
            else{
              $('#su_modal').modal('hide');
              clear_su_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }


  	}//.else
  })//.btn_su_save


//. employee	   