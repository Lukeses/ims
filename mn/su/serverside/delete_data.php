
    <?php           // ESTABLISH CONNECTION TO MYSQL
try{
    include('../../include/connect.php');                             //FETCH ALL VARIABLES
	include('../../include/log.php'); 

    $idKey = $_POST['idKey'];
    $constraint_count = 0;

$sql = "SELECT COUNT(*) as count FROM product 
WHERE prod_sup_id = ? AND (prod_status='active') ";
$q = $conn -> prepare($sql);
$q -> execute(array($idKey));
$browse = $q -> fetchAll();
foreach($browse as $fetch)
{
  $constraint_count = $fetch['count'];         
}   

if($constraint_count == 0){
		// UPDATE DATA TO DATABASE
	$sql = "UPDATE supplier SET sup_status = ? WHERE sup_id = ?";
	$q = $conn -> prepare($sql);
	$q -> execute(array('inactive',$idKey));

	$year = date('Y');
	$year = substr($year,2,3);
	$trail_id =uniqid('at'.$year);  
	$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
	$q = $conn -> prepare($sql);    
	$q -> execute(array($trail_id,'Maintenance','Supplier', 'REMOVE', 'Deleted: ID:'.$idKey, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

	$conn = null;
	echo json_encode(0); 	
}
else if($constraint_count > 0){
	echo json_encode(2); 		
}
	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}



?>  
