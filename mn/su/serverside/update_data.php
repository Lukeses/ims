

    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
   include('../../include/connect.php');
   include('../../include/log.php');

                                     //FETCH ALL VARIABLES
  
    $idKey = $_POST['idKey'];
	$su_name = ucwords(trim($_POST['su_name']));
	$su_phone = $_POST['su_phone'];
	$su_tel = $_POST['su_tel'];	
	$su_desc = ucwords(trim($_POST['su_desc']));


	        // INSERT DATA TO DATABASE
$sql = "UPDATE supplier SET sup_name=?, sup_desc=?, sup_phone=?, sup_tel=? WHERE sup_id=?";
$q = $conn -> prepare($sql);
$q -> execute(array($su_name,$su_desc,$su_phone,$su_tel,$idKey));

$year = date('Y');
$year = substr($year,2,3);
$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Supplier', 'EDIT', 'Updated: ID:'.$idKey. ', Name:'.$su_name.', Phone:'.$su_phone.', Tel:'.$su_tel.', Description:'.$su_desc, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$conn = null;
echo json_encode(0); 	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}




?>
