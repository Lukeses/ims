
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "500",
    "hideDuration": "1000",
    "timeOut": "6000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }//.toastr config  
	});  



	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	   

//Employee

	$('#btn_cat_add').click(function(){
		clear_cat_form();
		$('#btn_cat_save').val('insert');
		$('#btn_cat_save').prop('disabled',false);
		$('#cat_modal').modal('show');
	})

	//tables
	    var cat_table = $('#cat_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [1,2] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_cat_table(){ 
    //ajax now
    $('#loading').modal('show');
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        cat_table.fnClearTable();        
        for(var i = 0; i < s.length; i++) 
        { 

          cat_table.fnAddData
          ([
            s[i][1],
            '<button data-toggle="tooltip" onclick="cat_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="cat_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ],false); 
          cat_table.fnDraw();

        }       
      }  
    }); 
    $('#loading').modal('hide');    
    //ajax end  
  } //.load cat_table

  load_cat_table();
	
  function clear_cat_form(){
  	$('#cat_name').val('');
  	$('#cat_nameDiv').removeClass('has-error'); 	  	
  }

  function validate_cat_form(){
    var err = true;

    if($('#cat_name').val()==''){
      err = false ;
      $('#cat_nameDiv').addClass('has-error');
    }
    else
      $('#cat_nameDiv').removeClass('has-error');   


    return err; 
  }  	


  function cat_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  function cat_row_update(get_id){
    clear_cat_form();
    $('#btn_cat_save').prop('disabled',false);    
    $('#btn_cat_save').val(get_id);
      $.ajax({
        type: 'POST',
        url: 'serverside/fetch_data.php',
        dataType: 'json',
        data: 'idKey='+get_id,
        success: function(s) {
        for(var i = 0; i < s.length; i++) {
          $('#cat_name').val(s[i][0]);
          $('#cat_modal').modal('show');
        }
      }
    });
  }


  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/delete_data.php',
      data: 'idKey='+$(this).val(),
      success: function(s) {          
          if(s==0){
            load_cat_table();
            $('#delModal').modal('hide');     
            toastr.error('Record Deleted');
          }
          else if(s==1){
            $('#delModal').modal('hide');     
            toastr.warning('Error: No Connection');          
          }        
          else if(s==2){
            $('#delModal').modal('hide');     
            toastr.warning('Warning: Category is in use by Products');          
          }              
      }
    });  
  });

  $('#btn_cat_save').click(function(){
  	if(validate_cat_form() == false){}
  	else{
    $(this).prop('disabled',true);    
      var cat_name = $('#cat_name').val();  
  		var dataString = 'cat_name=' +cat_name;

      if($(this).val()=='insert'){ //will insert
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_data.php",
          data: dataString, 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#cat_modal').modal('hide');
              clear_cat_form();
              load_cat_table();
              toastr.success(cat_name,'Category Added:');              
            }//.if
            else{
              $('#cat_modal').modal('hide');
              clear_cat_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }

      else{ //will update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_data.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#cat_modal').modal('hide');
              clear_cat_form();
              load_cat_table();
              toastr.info(cat_name,'Category Updated:');              
            }//.if
            else{
              $('#cat_modal').modal('hide');
              clear_cat_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }


  	}//.else
  })//.btn_em_save


//. employee	   