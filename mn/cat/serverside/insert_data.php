
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../include/connect.php');  
 	include('../../include/log.php');  

                                     //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('ct'.$year);   

	$cat_name = ucwords(trim($_POST['cat_name']));


                                                                 // INSERT DATA TO DATABASE
$sql = "INSERT INTO category VALUES(?,?,?)";
$q = $conn -> prepare($sql);
$q -> execute(array($id,$cat_name,'active'));


$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Category', 'CREATE', 'New: ID:'.$id. ', Name:'.$cat_name, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

$conn = null;
echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
