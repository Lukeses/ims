<?php ?>      

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <?php echo'<img src="../../resources/dist/img/'.$u_type.'.png"'.' class="user-image" alt="User Image">'; ?>
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo(ucwords($u_name)); ?></span>
                </a>
                  <ul class="dropdown-menu" style="width:10%;border-radius:5px">
                    <li style="text-align:center"> 
                      <small style="font-size:0.8em"><?php echo ucfirst($u_type); ?></small>
                    </li>
                      <li class="divider"></li>
                  <?php if($u_type=="admin"){ ?>                      
                    <li><a href="../uc/uc.php"><i class="fa fa-users"></i> User Accounts</a></li>
                  <?php } ?>
                    <li><a onclick="return logout()" href="../../mn/user/processlogout.php"> <i class="fa fa-sign-in"></i><span>Log-out</span>    
              </a></li><br>
                  </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">MENU NAVIGATION</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="../../index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            
            <li class="active treeview">
              <a href="index.php"><i class="fa fa-gear"></i> <span>Maintenance</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="../pr/pr.php"><i class="fa fa-circle-o"></i> Product</a></li>
                <li><a href="../su/su.php"><i class="fa fa-circle-o"></i> Supplier</a></li>
                <li><a href="../ve/ve.php"><i class="fa fa-circle-o"></i> Vehicle</a></li>                
                <li><a href="../em/em.php"><i class="fa fa-circle-o"></i> Employee</a></li>
                <li  class="active"><a href="../../mn/lo/lo.php"><i class="fa fa-circle"></i> Location</a></li>                
                <li><a href="../../mn/gc/gc.php"><i class="fa fa-circle-o"></i>Groups & Company</a></li>               </ul>
            </li>

            <li><a href="../../tr/po/po.php"><i class="fa fa-cart-arrow-down"></i> <span>Purchase Order</span></a></li>

            <li class="treeview">
              <a href="#"><i class="fa fa-bar-chart"></i> <span>P.O. Analysis</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="../../tr/poa/poa_sup.php"><i class="fa fa-circle-o"></i>By Supplier</a></li>
                <li><a href="../../tr/poa/poa_vessel.php"><i class="fa fa-circle-o"></i>By Vessel</a></li>
                <li><a href="../../tr/poa/poa_cat.php"><i class="fa fa-circle-o"></i>By Category</a></li>                                              
              </ul>
            </li>   
            
            <li class="treeview">
              <a href="#"><i class="fa fa-paste"></i> <span>Operation</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="../../tr/gr/gr.php"><i class="fa fa-circle-o"></i>Goods Received</a></li>
                <li><a href="../../tr/dn/dn.php"><i class="fa fa-circle-o"></i>Delivery</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Stock Transfer</a>
                  <ul class="treeview-menu">
                    <li><a href="../../tr/st-bb/st.php"><i class="fa fa-circle-o"></i>Branch</a></li>
                    <li><a href="../../tr/st-ww/st.php"><i class="fa fa-circle-o"></i>Warehouse</a></li> 
                    <li><a href="../../tr/st-bw/st.php"><i class="fa fa-circle-o"></i>Pull-Out</a></li>                                                                                                                       
                  </ul>                  
                </li>                                              
              </ul>
            </li>            

            <li class="treeview">
              <a href="#"><i class="fa fa-exchange"></i> 
                <span>Stock Movement</span><i class="fa fa-angle-left pull-right"></i></a>    
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Warehouse </a>
                  <ul class="treeview-menu">
                    <li><a href="../../sm/wh-i/sm.php"><i class="fa fa-circle-o"></i>Inbound</a></li>
                    <li><a href="../../sm/wh-o/sm.php"><i class="fa fa-circle-o"></i>Outbound</a></li>    
                    <li><a href="../../sm/pr/sm.php"><i class="fa fa-circle-o"></i>Procurement</a></li>                                                                                                                    
                  </ul>
                </li>

                <li><a href="#"><i class="fa fa-circle-o"></i> Branch </a>
                  <ul class="treeview-menu">
                    <li><a href="../../sm/br-i/sm.php"><i class="fa fa-circle-o"></i>Inbound</a></li>
                    <li><a href="../../sm/br-o/sm.php"><i class="fa fa-circle-o"></i>Outbound</a></li>                                                                                                                                     
                  </ul>
                </li>                            
              </ul> 

            <li class="treeview">
              <a href="#"><i class="fa fa-sitemap"></i> 
                <span>Inventory Level</span><i class="fa fa-angle-left pull-right"></i></a>    
              </a>
              <ul class="treeview-menu">
                <li><a href="../../il/wh/il.php"><i class="fa fa-circle-o"></i> Warehouse </a></li>
                <li><a href="../../il/br/il.php"><i class="fa fa-circle-o"></i> Branch </a></li>
                <li><a href="../../il/tot/il.php"><i class="fa fa-circle-o"></i> Total </a></li>
                <li><a href="../../il/pr/il.php"><i class="fa fa-circle-o"></i> Product </a></li>                
              </ul> 
            </li>

            <li><a href="../../tr/ld/ld.php"><i class="fa fa-ban"></i> <span>Lost/Damaged/Return</span></a></li>

            <li class="treeview">
              <a href="#"><i class="fa fa-wrench"></i> <span>Utilities</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="../../mn/at/at.php"><i class="fa fa-circle-o"></i>Audit Trail</a></li>   
                <li><a href="../../mn/lb/lb.php"><i class="fa fa-circle-o"></i>Local Backup</a></li>                                                                           
              </ul>
            </li>    
                                 
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
<?php ?>