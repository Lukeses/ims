
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-left",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "500",
      "hideDuration": "1000",
      "timeOut": "6000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }//.toastr config  


    //Initialize Select2 Elements
    $("#gd_name").select2();   
    $('#lo_co').select2();
	});  

	function logout(){

	  var choice = confirm("Are you sure you want to Log-out?");
	  if(choice==true)
	  {
	    return true;
	  }
	  else
	    return false;
	}
	   
  function select_groupdiv(){
    populate_gr_dropdown();    
    $('#gd_name').select2().select2('val','none');              
    $('#gd_nameDiv').removeClass('has-error');
    $('#gd_modal').modal('show');    
  }     

//Location

  $('#btn_gd_submit').click(function(){
    if($('#gd_name').val()=='none'){
      $('#gd_nameDiv').addClass('has-error');
    }
    else{
      $('#btn_select_groupdiv').removeClass('bg-red');
      $('#btn_select_groupdiv').addClass('bg-green');         
      $('#gd_nameDiv').removeClass('has-error');
      $('#gd_selected').val($('#gd_name').val());
      $('#gd_selected_name').text( $( "#gd_name option:selected" ).text()  );      
      load_lo_table($('#gd_selected').val());
      $('#gd_modal').modal('hide');    
      change_session_group();
    }
  })

  function change_session_group(){
    var gr_name = $( "#gd_name option:selected" ).text();  
    var gr_id = $('#gd_name').val();
    var dataString = 'gr_id='+gr_id+'&gr_name='+gr_name;
    //ajax now
    $.ajax ({
      type: "POST",
      url: "../include/change_session_group.php",
      data: dataString,
      success: function(s){         
      }  
    }); 
    //ajax end      
  }

  function populate_gr_dropdown(){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_gd_dropdown.php",
      dataType: 'json',      
      cache: false,
      success: function(s)
      {
        $('#gd_name').empty();
        $('#gd_name').append('<option value="none">--Select Group--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#gd_name').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
  } // 

  function populate_company_dropdown(gd_name){ 
    //ajax now
    $.ajax ({
      type: "POST",
      url: "serverside/populate_company_dropdown.php",
      dataType: 'json',      
      data: 'gd_name='+gd_name,
      cache: false,
      success: function(s)
      {
        $('#lo_co').empty();
        $('#lo_co').html('<option selected="selected" value="none">--Search Company--</option>');
        for(var i = 0; i < s.length; i++) { 
          $('#lo_co').append('<option id="opt'+s[i][0]+'" value="'+s[i][0]+'">'+s[i][1]+'</option>');
        }       
      }  
    }); 
    //ajax end  
    $('#lo_co').select2().select2('val','none');                      
  } // 

	$('#btn_lo_add').click(function(){
    if($('#gd_selected').val()==''){
      $('#btn_select_groupdiv').removeClass('bg-green');
      $('#btn_select_groupdiv').addClass('bg-red');      
    }
    else{   
      clear_lo_form();
      populate_company_dropdown($('#gd_selected').val());
      $('#btn_lo_save').val('insert');
      $('#btn_lo_save').prop('disabled',false);
      $('#lo_modal').modal('show');
    }
	})

	//tables
	    var lo_table = $('#lo_table').dataTable({
	        columnDefs: [
	       { type: 'formatted-num', targets: 0 }
	       ],       
	      "aoColumnDefs": [ { "bSortable": false, "aTargets": [5,6] } ],
	      "aaSorting": []
	    });  //Initialize the datatable department
	//.tables    

  function load_lo_table(gd_name){ 
    //ajax now
    $('#loading').modal({backdrop: 'static'})                     
    $('#loading').modal('show');
        lo_table.fnClearTable();            
    $.ajax ({
      type: "POST",
      url: "serverside/load_data.php",
      dataType: 'json',      
      data: 'gd_name='+gd_name,
      cache: false,
      success: function(s)
      {
      	$('#loading').modal('hide');
        for(var i = 0; i < s.length; i++) 
        { 

          lo_table.fnAddData
          ([
            s[i][0],s[i][1],s[i][2],s[i][3],s[i][4],
            '<button data-toggle="tooltip" onclick="lo_row_update(this.value)" value='+s[i][0]+'  class="btn btn-xs btn-default" title="View / Update"> <i class="fa fa-edit"></i> </button>',
            '<button data-toggle="tooltip" onclick="lo_row_del(this.value)" value='+s[i][0]+' data-toggle="modal" class="btn btn-xs  btn-default" title="Delete"> <i class="fa fa-trash"></i> </button>',      
          ],false); 
          lo_table.fnDraw();

        }       
      }  
    }); 
    //ajax end  
    $('#loading').modal('hide');          
  } //.load lo_table


  function clear_lo_form(){
  	$('#lo_name').val('');
  	$('#lo_co').val('');
  	$('#lo_add').val('');
    $('#optnone').prop('selected',true);    
  	$('#lo_nameDiv').removeClass('has-error'); 	
  	$('#lo_coDiv').removeClass('has-error'); 	
  	$('#lo_typeDiv').removeClass('has-error'); 	  	
  }

  function validate_lo_form(){
    var err = true;

    if($('#lo_name').val()==''){
      err = false ;
      $('#lo_nameDiv').addClass('has-error');
    }
    else
      $('#lo_nameDiv').removeClass('has-error');   

    if($('#lo_co').val()=='none'){
      err = false ;
      $('#lo_coDiv').addClass('has-error');
    }
    else
      $('#lo_coDiv').removeClass('has-error');   

    if($('#lo_type').val()=='none'){
      err = false ;
      $('#lo_typeDiv').addClass('has-error');
    }
    else
      $('#lo_typeDiv').removeClass('has-error');   

    return err; 
  }  	


  function lo_row_del(get_id){
    $('#delModal').modal('show');
    $('#btnDel').val(get_id);
  }

  function lo_row_update(get_id){
    clear_lo_form();
    populate_company_dropdown($('#gd_selected').val());    
    $('#btn_lo_save').prop('disabled',false);    
    $('#btn_lo_save').val(get_id);
      $.ajax({
        type: 'POST',
        url: 'serverside/fetch_data.php',
        dataType: 'json',
        data: 'idKey='+get_id,
        success: function(s) {
        for(var i = 0; i < s.length; i++) {
          $('#lo_name').val(s[i][0]);
          $('#lo_co').select2().select2('val',s[i][1]);          
          $('#lo_add').val(s[i][2]);
          $('#lo_type').val(s[i][3]);
        }
          $('#lo_modal').modal('show');        
      }
    });
  }


  $('#btnDel').click(function(){
    $.ajax({
      type: 'POST',
      url: 'serverside/delete_data.php',
      data: 'idKey='+$(this).val(),
      success: function(s) {          
        if(s==0){
          load_lo_table($('#gd_selected').val());
          $('#delModal').modal('hide');     
          toastr.error('Record Deleted');
        }
        else if(s==1){
          $('#delModal').modal('hide');     
          toastr.warning('Error: No Connection');          
        }         
        else if(s==2){
          $('#delModal').modal('hide');     
          toastr.warning('Warning: Location is in use by Documents');          
        }          

      }
    });  
  });

  $('#btn_lo_save').click(function(){
  	if(validate_lo_form() == false){}
  	else{
    $(this).prop('disabled',true);     
      var lo_name = $('#lo_name').val(); 
  		var lo_type = $('#lo_type').val();
      var lo_co = $('#lo_co').val();
  		var lo_add = $('#lo_add').val();
  		var dataString = 'lo_name=' +lo_name+ '&lo_type=' +lo_type+ '&lo_co=' +lo_co+ '&lo_add='+lo_add;
      if($(this).val()=='insert'){ //will insert
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/insert_data.php",
          data: dataString, 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#lo_modal').modal('hide');
              clear_lo_form();
              load_lo_table($('#gd_selected').val());
              toastr.success(lo_name,'Location Added:');              
            }//.if
            else{
              $('#lo_modal').modal('hide');
              clear_lo_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }

      else{ //will update
        //ajax now
        $.ajax ({
          type: "POST",
          url: "serverside/update_data.php",
          data: dataString+'&idKey='+$(this).val(), 
          cache: false, 
          success: function(s){
            if(s==0){
              $('#lo_modal').modal('hide');
              clear_lo_form();
              load_lo_table($('#gd_selected').val());
              toastr.info(lo_name,'Location Updated:');              
            }//.if
            else{
              $('#lo_modal').modal('hide');
              clear_lo_form();
              toastr.warning('Error: No Connection');         
            }//.else
          }  
        }); 
        //ajax end    
      }


  	}//.else
  })//.btn_em_save

  function initialize(){
    if($('#gd_selected').val()==''){
      select_groupdiv();
    }
    else{
      load_lo_table($('#gd_selected').val());
    }
  }

  initialize();

//. employee	   