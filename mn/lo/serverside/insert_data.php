
    <?php           // ESTABLISH CONNECTION TO MYSQL

try{
 	include('../../include/connect.php');  
    include('../../include/log.php');  

                                     //FETCH ALL VARIABLES
    $year = date('Y');
    $year = substr($year,2,3);
    $id =uniqid('lo'.$year);   

	$lo_name = ucwords(trim($_POST['lo_name']));
	$lo_co = $_POST['lo_co'];
	$lo_add = ucwords(trim($_POST['lo_add']));
	$lo_type = $_POST['lo_type'];

                                                                 // INSERT DATA TO DATABASE
$sql = "INSERT INTO location VALUES(?,?,?,?,?,?)";
$q = $conn -> prepare($sql);
$q -> execute(array($id,$lo_co,$lo_name,$lo_add,$lo_type,'active'));

$trail_id =uniqid('at'.$year);  
$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
$q = $conn -> prepare($sql);    
$q -> execute(array($trail_id,'Maintenance','Location', 'CREATE', 'New: ID:'.$id. ', Name:'.$lo_name.', Company:'.$lo_co.', Type:'.$lo_type.', Address:'.$lo_add, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));


$conn = null;
echo json_encode(0); 	
}

catch(PDOException $x) {
echo json_encode(1); 		
}



?>
