<?php
    include('../../include/connect.php');

    $gd_name = $_POST['gd_name'];

  $sql = "SELECT loc_id, loc_name, loc_add, loc_type, co_name
   FROM location l, company c, group_div g
   WHERE (c.co_id=l.loc_co_id)
   AND (c.co_gr_id=g.gr_id)
   AND (g.gr_id = ? )
   AND (l.loc_status = 'active')
   GROUP BY loc_id
   ORDER BY loc_id ASC ";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_name));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['loc_id'],$fetch['loc_name'],
      $fetch['loc_add'],$fetch['co_name'],ucwords($fetch['loc_type']));				 	
  }         
$conn = null;             

echo json_encode($output);
?>    