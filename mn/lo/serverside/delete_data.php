
    <?php           // ESTABLISH CONNECTION TO MYSQL
try{
    include('../../include/connect.php');                             //FETCH ALL VARIABLES
	include('../../include/log.php');  

    $idKey = $_POST['idKey'];
    $constraint_count = 0;

$sql = "SELECT COUNT(*) as count FROM document 
WHERE (doc_source = ? OR doc_desti = ?)
AND (doc_status != 'deleted')";
$q = $conn -> prepare($sql);
$q -> execute(array($idKey,$idKey));
$browse = $q -> fetchAll();
foreach($browse as $fetch)
{
  $constraint_count = $fetch['count'];         
}   

if($constraint_count == 0){
		// UPDATE DATA TO DATABASE
	$sql = "UPDATE location SET loc_status = ? WHERE loc_id = ?";
	$q = $conn -> prepare($sql);
	$q -> execute(array('inactive',$idKey));

	$year = date('Y');
	$year = substr($year,2,3);
	$trail_id =uniqid('at'.$year);  
	$sql = "INSERT INTO trail VALUES(?,?,?,?,?,?,?,?)";
	$q = $conn -> prepare($sql);    
	$q -> execute(array($trail_id,'Maintenance','Location', 'REMOVE', 'Deleted: ID:'.$idKey, date('Y/m/d H:i:s'),  $_SESSION["u_name"],$_SESSION['u_type']   ));

	$conn = null;
	echo json_encode(0); 	
}
else if($constraint_count > 0){
	echo json_encode(2); 		
}
	
}// end try
catch(PDOException $x){
echo json_encode(1); 	
}



?>  
