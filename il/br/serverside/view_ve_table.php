<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT v.ve_id as id, v.ve_no as pltno, v.ve_desc as description, 
(SELECT vt.vt_name FROM vehicle_type vt WHERE vt.vt_id = v.ve_vt_id) as type
FROM vehicle v, document_vehicle dv, document d 
WHERE (d.doc_id = ?) 
AND (d.doc_id = dv.dv_doc_id)
AND (v.ve_id = dv.dv_ve_id)
GROUP BY dv.dv_id";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['id'],$fetch['pltno'],$fetch['type'],$fetch['description']);          
  }         
$conn = null;             

echo json_encode($output);
?>    