<?php
    include('../../../mn/include/connect.php');

  $lo_id = $_POST['lo_id'];


  $sql = "SELECT pv.pv_id, pr.prod_name, pr.prod_sku,pr.prod_var,
(SELECT c.cat_name FROM category c WHERE c.cat_id = pr.prod_cat_id) as category, 
(SELECT s.sup_name FROM supplier s WHERE s.sup_id = pr.prod_sup_id) as supplier, 
(SELECT co.co_name FROM company co WHERE co.co_id = pr.prod_co_id) as company,
pv.pv_price, pv.pv_price as price,
  SUM(case when d.doc_desti = ? then (dp.dp_qty) else 0 end) as inbound,
  SUM(case when d.doc_source = ? then (dp.dp_qty) else 0 end) as outbound 
  FROM product_version pv, product pr, document_product dp, document d 
  WHERE (pv.pv_prod_id = pr.prod_id) 
  AND (d.doc_id = dp.dp_doc_id)
  AND (dp.dp_prod_id = pv.pv_id)
  AND (d.doc_status='shipped')
  AND (d.doc_source = ? OR d.doc_desti = ?)
  GROUP BY pv.pv_id
  ORDER BY pr.prod_name ASC, pv.pv_date DESC";

  $q = $conn->prepare($sql);
  $q -> execute(array($lo_id,$lo_id,$lo_id,$lo_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['pv_id'],$fetch['prod_name'],$fetch['prod_sku'],$fetch['prod_var'],
      $fetch['category'],$fetch['supplier'],$fetch['company'],$fetch['pv_price'],($fetch['inbound']-$fetch['outbound']),
      ($fetch['price']*($fetch['inbound']-$fetch['outbound'])));          
  }         
$conn = null;             

echo json_encode($output);
?>    