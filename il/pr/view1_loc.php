<?php
require('../../mn/include/log.php')
?>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inventory Level | Product </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="../../resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Ionicons -->
    <link href="../../resources/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />      
    <!-- Theme style -->
    <link rel="stylesheet" href="../../resources/dist/css/AdminLTE.min.css">
    <!-- DATA TABLES -->
    <link href="../../resources/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- toastr-->
    <link href="../../resources/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />    


    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../../resources/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../../resources/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../resources/dist/js/app.min.js"></script>
    <!-- DATA TABLE SCRIPT--> 
    <script src="../../resources/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../resources/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>   
    <!-- toastr js--> 
    <script src="../../resources/plugins/toastr/toastr.min.js" type="text/javascript"></script>   
    <!--live search-->
    <script src="../../resources/plugins/slive/slive.js" type="text/javascript"></script>   
    <!-- select 2 -->
    <script src="../../resources/plugins/select2/select2.full.min.js"></script>
    <!--bootsrap validator-->
    <script src="../../resources/bootstrap/js/bootstrapValidator.js"></script>

  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="../../index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>IM</b>S</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IMS </b>Cloudipark</span>
        </a>

        <?php include('aside.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Inventory Level
            <small> Product By Location
              <?php 
                $pv_id = $_GET['pv_id'];
                echo'<input type="hidden" id="pv_id" value='.$pv_id.'>';                
              ?>
            </small>            
          </h1>          
          <?php echo'<text class="lead" id="gd_selected_name">'.$session_group_name.'</text>';  ?>                    
          <?php echo'<input type="hidden" id="gd_selected" value="'.$session_group.'">';  ?>  

        </section>

        <!-- Main content -->
        <section class="content">

          <?php include('../../mn/include/loading.html') ?>
          <?php include('../../mn/include/group_div.html') ?>


          <div class="row" style="margin-top:10px">                     <!-- TABLES -->
            <div class="col-lg-12 col-sm-12 col-xs-12">
              <div class="box box-default">
                <div class="box-header">

                <div class="row">
                  <div class="col-sm-1 col-xs-6">
                    <div class="description-block border-right">
                      <h5 class="description-header" id="lbl_pr_sku"><?php echo($_GET['pr_sku']); ?></h5>
                      <span class="description-text">SKU</span>
                    </div><!-- /.description-block -->
                  </div><!-- /.col -->                       
                  <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                      <h5 class="description-header" id="lbl_pr_cat"><?php echo($_GET['pr_cat']); ?></h5>
                      <span class="description-text">CATEGORY</span>
                    </div><!-- /.description-block -->
                  </div><!-- /.col -->                             
                  <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                      <h5 class="description-header" id="lbl_pr_var"><?php echo($_GET['pr_var']); ?></h5>
                      <span class="description-text">UOM</span>
                    </div><!-- /.description-block -->
                  </div><!-- /.col -->
                  <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                      <h5 class="description-header" id="lbl_pr_name"><?php echo($_GET['pr_name']); ?></h5>
                      <span class="description-text">PRODUCT</span>
                    </div><!-- /.description-block -->
                  </div><!-- /.col -->         
                  <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                      <h5 class="description-header" id="lbl_pr_sup"><?php echo($_GET['pr_sup']); ?></h5>
                      <span class="description-text">SUPPLIER</span>
                    </div><!-- /.description-block -->
                  </div><!-- /.col -->
                  <div class="col-sm-2 col-xs-6">
                    <div class="description-block border-right">
                      <h5 class="description-header" id="lbl_pr_co"><?php echo($_GET['pr_co']); ?></h5>
                      <span class="description-text">OWNER</span>
                    </div><!-- /.description-block -->
                  </div><!-- /.col -->                              
                  <div class="col-sm-1 col-xs-6">
                    <div class="description-block border-right">
                      <h5 class="description-header" id="lbl_pr_price">₱<?php echo($_GET['pr_price']); ?></h5>
                      <span class="description-text">PRICE</span>
                    </div><!-- /.description-block -->
                  </div><!-- /.col -->                                                                      
                </div><!-- /.row -->
                <hr>

                <div class="row">
                  <div class="col-sm-3"></div><!--left margin-->
                  <div class="col-sm-3 col-xs-12">
                    <div class="description-block">
                      <h5 class="description-header text-info" id="lbl_tot_soh" style="font-size:23px"></h5>
                      <span class="description-text">TOTAL STOCKS ON HAND</span>
                    </div>                         
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="description-block">
                      <h5 class="description-header text-success" id="lbl_grand_total" style="font-size:23px"></h5>
                      <span class="description-text">TOTAL VALUE</span>
                    </div>                         
                  </div>
                  <div class="col-sm-3"></div><!--right margin-->     
                </div><!--/.row-->


                <div class="box-body">
                  <table id="loc_table" class="table  table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Location</th>                    
                        <th>Address</th>
                        <th>Company</th>
                        <th>Type</th>
                        <th>Stocks On Hand</th>
                        <th>Value</th>
                        <th style="width:10px"></th>                  
                      </tr>
                      <tbody></tbody>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>  <!-- /.row -->

        </section><!-- /.content -->


      </div><!-- /.content-wrapper -->

      <?php include('../../mn/include/footer.php'); ?>
    </div><!-- ./wrapper -->


    <!--MAIN CONTROLLER -->
    <script src="controller.js"></script>

    <script>
     load_loc_table($('#pv_id').val());
    </script>
  
  </body>
</html>
