<?php
    include('../../../mn/include/connect.php');

    $gd_name = $_POST['gd_name'];

  $sql = "SELECT pv.pv_id, pr.prod_sku, pr.prod_name,pr.prod_var,
  (SELECT cat_name FROM category c WHERE c.cat_id = pr.prod_cat_id) as category,
  (SELECT sup_name FROM supplier s WHERE s.sup_id = pr.prod_sup_id) as supplier,
  co.co_name,pv.pv_price
  FROM product pr, product_version pv, company co
  WHERE (pv.pv_prod_id = pr.prod_id) 
  AND (pr.prod_co_id = co.co_id)
  AND (co.co_gr_id = ?)
  GROUP BY pv.pv_id 
  ORDER BY pr.prod_name ASC, pr.prod_var ASC, pv.pv_date DESC";

  $q = $conn->prepare($sql);
  $q -> execute(array($gd_name));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['pv_id'],$fetch['prod_sku'],$fetch['prod_name'],
      $fetch['category'],$fetch['prod_var'],$fetch['supplier'],$fetch['co_name'],$fetch['pv_price']);				 	
  }         
$conn = null;             

echo json_encode($output);
?>    