<?php
    include('../../../mn/include/connect.php');

  $doc_no = $_POST['doc_no'];


  $sql = "SELECT e.emp_id as id, e.emp_name as name, e.emp_phone as phone, do.do_desc as role, 
  (SELECT d.dep_name FROM department d, position p WHERE e.emp_pos_id = p.pos_id AND p.pos_dep_id = d.dep_id) as department
FROM employee e, document_operation do, document d 
WHERE (d.doc_id = ?) 
AND (d.doc_id = do.do_doc_id)
AND (do.do_emp_id = e.emp_id) 
GROUP BY do.do_id";

  $q = $conn->prepare($sql);
  $q -> execute(array($doc_no));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['id'],$fetch['name'],$fetch['phone'],$fetch['department'],$fetch['role']);          
  }         
$conn = null;             

echo json_encode($output);
?>    