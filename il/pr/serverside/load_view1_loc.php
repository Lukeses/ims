<?php
    include('../../../mn/include/connect.php');

    $pv_id = $_POST['pv_id'];

  $sql = "SELECT loc_id, loc_name, loc_add, loc_type,co_name,pv_price,
  SUM(case when d.doc_desti = l.loc_id then (dp.dp_qty) else 0 end) as inbound,
  SUM(case when d.doc_source = l.loc_id then (dp.dp_qty) else 0 end) as outbound
  FROM location l, company co, document d, document_product dp, product_version pv
  WHERE (l.loc_status = 'active')
  AND (l.loc_co_id = co.co_id) 
  AND (d.doc_desti = l.loc_id OR d.doc_source = l.loc_id)
  AND (d.doc_id = dp.dp_doc_id)
  AND (dp.dp_prod_id = pv.pv_id)
  AND (d.doc_status = 'shipped')
  AND (pv.pv_id = ?)
  GROUP BY l.loc_id";

  $q = $conn->prepare($sql);
  $q -> execute(array($pv_id));
  $browse = $q -> fetchAll();
  foreach($browse as $fetch)
  {
    $output[] = array ($fetch['loc_id'],$fetch['loc_name'],$fetch['loc_add'],$fetch['co_name'],ucwords($fetch['loc_type']),
    ($fetch['inbound']-$fetch['outbound']),$fetch['pv_price']*($fetch['inbound']-$fetch['outbound']) );				 	
  }         
$conn = null;             

echo json_encode($output);
?>    